#pragma once

#include <string_view>

namespace bwn
{
	template<typename CharT>
	class BasicStringViewDivider
	{
		//
		// Construction and destruction.
		//
	public:
		explicit BasicStringViewDivider(const std::basic_string_view<CharT>& _string);

		//
		// Public interface.
		//
	public:
		// Returns string from the current position to the divider.
		std::basic_string_view<CharT> next(const std::basic_string_view<CharT>& _divider);
		// Returns true string is finished (next, will always return empty strings from this point).
		bool finished() const;

		//
		// Private members.
		//
	private:
		std::basic_string_view<CharT> m_string;
	};

	using StringViewDivider = BasicStringViewDivider<char32_t>;

} // namespace bwn

#include "bwn_core/algorithm/stringViewDivider.inl"