#pragma once

#include "bwn_core/algorithm/stringViewDivider.hpp"

template<typename CharT>
bwn::BasicStringViewDivider<CharT>::BasicStringViewDivider(const std::basic_string_view<CharT>& _string)
	: m_string( _string )
{}

template<typename CharT>
std::basic_string_view<CharT> bwn::BasicStringViewDivider<CharT>::next(const std::basic_string_view<CharT>& _divider)
{
	const size_t endIndex = m_string.find(_divider);
	
	const std::basic_string_view<CharT> result = m_string.substr(0, endIndex);

    const size_t startIndex = endIndex != std::basic_string_view<CharT>::npos
        ? endIndex + _divider.length()
        : m_string.length();

	m_string = m_string.substr(startIndex);

	return result;
}

template<typename CharT>
bool bwn::BasicStringViewDivider<CharT>::finished() const
{
	return m_string.empty();
}