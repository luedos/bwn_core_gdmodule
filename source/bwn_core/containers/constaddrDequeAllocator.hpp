#pragma once

#include "bwn_core/containers/staticFreelist.hpp"

#include <forward_list>
#include <type_traits>

namespace bwn
{
	template<typename TypeT, size_t k_stackCount = 16>
	class constaddr_deque_allocator;
} // namespace bwn

template<typename TypeT, size_t k_leftStackCount, size_t k_rightStackCount>
bool operator==(const bwn::constaddr_deque_allocator<TypeT, k_leftStackCount>& _left, const bwn::constaddr_deque_allocator<TypeT, k_rightStackCount>& _right);
template<typename TypeT, size_t k_leftStackCount, size_t k_rightStackCount>
bool operator!=(const bwn::constaddr_deque_allocator<TypeT, k_leftStackCount>& _left, const bwn::constaddr_deque_allocator<TypeT, k_rightStackCount>& _right);

namespace bwn
{
	template<typename TypeT, size_t k_stackCount>
	class constaddr_deque_allocator
	{
		template<typename OtherTypeT, size_t k_leftStackCount, size_t k_rightStackCount>
		friend bool ::operator==(const constaddr_deque_allocator<OtherTypeT, k_leftStackCount>& _left, const constaddr_deque_allocator<OtherTypeT, k_rightStackCount>& _right);
		template<typename OtherTypeT, size_t k_leftStackCount, size_t k_rightStackCount>
		friend bool ::operator!=(const constaddr_deque_allocator<OtherTypeT, k_leftStackCount>& _left, const constaddr_deque_allocator<OtherTypeT, k_rightStackCount>& _right);

		//
		// Private defines.
		//
	private:
		using Stack = static_freelist<TypeT, k_stackCount>;

		//
		// Public defines.
		//
	public:
		using value_type = TypeT;
		using size_type = size_t;
		using difference_type = std::ptrdiff_t;
		using propagate_on_container_move_assignment = std::true_type;
		using propagate_on_container_copy_assignment = std::false_type;
		using propagate_on_container_swap = std::false_type;
		using is_always_equal = std::true_type;
		template<typename OtherTypeT> 
		struct rebind
		{
			using other = constaddr_deque_allocator<OtherTypeT, k_stackCount>;
		};


		//
		// Construction and destruction.
		//
	public:
		constaddr_deque_allocator();
		~constaddr_deque_allocator();

		constaddr_deque_allocator(const constaddr_deque_allocator&) noexcept = delete;
		constaddr_deque_allocator(constaddr_deque_allocator&& _other) noexcept;

		constaddr_deque_allocator& operator=(const constaddr_deque_allocator&) noexcept = delete;
		constaddr_deque_allocator& operator=(constaddr_deque_allocator&& _other) noexcept;

		//
		// Public interface.
		//
	public:
		// Allocates the memory for the type (_count could be equal only to 1).
		TypeT* allocate(size_type _count);
		// Deallocates the memory from the type (_count could be equal only to 1).
		void deallocate(TypeT* _object, size_type _count);
		// Simply returns 1.
		constexpr size_type max_size() const;
		// Mostly does nothing, used for type_traits.
		constaddr_deque_allocator select_on_container_copy_construction() const;
		// Returns true if allocator has no allocated objects.
		bool empty() const;

		//
		// Private members.
		//
	private:
		// The list with the stacks of objects.
		std::forward_list<Stack> m_list;
	};
} // namespace bwn

#include "bwn_core/containers/constaddrDequeAllocator.inl"