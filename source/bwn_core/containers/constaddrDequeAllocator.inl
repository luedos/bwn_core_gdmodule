#pragma once

#include "bwn_core/containers/constaddrDequeAllocator.hpp"

#include "bwn_core/debug/debug.hpp"

template<typename TypeT, size_t k_stackCount>
bwn::constaddr_deque_allocator<TypeT, k_stackCount>::constaddr_deque_allocator() = default;

template<typename TypeT, size_t k_stackCount>
bwn::constaddr_deque_allocator<TypeT, k_stackCount>::~constaddr_deque_allocator()
{
	BWN_ASSERT_ERROR_COND(empty(), "The constaddr_deque_allocator is destroyed when some allocated object still weren't free. Allocated objects will be corrapted.");
}

template<typename TypeT, size_t k_stackCount>
bwn::constaddr_deque_allocator<TypeT, k_stackCount>::constaddr_deque_allocator(constaddr_deque_allocator&& _other) noexcept
    : m_list( std::move(_other.m_list) )
{}

template<typename TypeT, size_t k_stackCount>
bwn::constaddr_deque_allocator<TypeT, k_stackCount>& bwn::constaddr_deque_allocator<TypeT, k_stackCount>::operator=(constaddr_deque_allocator&& _other) noexcept
{
    m_list = std::move(_other.m_list);
    return *this;
}

template<typename TypeT, size_t k_stackCount>
TypeT* bwn::constaddr_deque_allocator<TypeT, k_stackCount>::allocate(size_type _count)
{

    for(Stack& stack : m_list)
    {
        TypeT*const allocated = stack.allocate();
        if (allocated != nullptr)
        {
            return allocated;
        }
    }

    m_list.emplace_front();
    return m_list.front().allocate();
}

template<typename TypeT, size_t k_stackCount>
void bwn::constaddr_deque_allocator<TypeT, k_stackCount>::deallocate(TypeT* _object, size_type _count)
{
    for(Stack& stack : m_list)
    {
        if (stack.owns(_object))
        {
            stack.deallocate(_object);
            return;
        }
    }
}

template<typename TypeT, size_t k_stackCount>
constexpr typename bwn::constaddr_deque_allocator<TypeT, k_stackCount>::size_type bwn::constaddr_deque_allocator<TypeT, k_stackCount>::max_size() const
{
    // Basically this allocator allow to allocate only 1 object at a time.
    return 1;
}

template<typename TypeT, size_t k_stackCount>
bool bwn::constaddr_deque_allocator<TypeT, k_stackCount>::empty() const
{
	bool empty = true;
	for (const Stack& stack : m_list)
	{
		if (!stack.empty())
		{
			empty = false;
			break;
		}
	}

	return empty;
}

template<typename TypeT, size_t k_leftStackCount, size_t k_rightStackCount>
bool operator==(const bwn::constaddr_deque_allocator<TypeT, k_leftStackCount>& _left, const bwn::constaddr_deque_allocator<TypeT, k_rightStackCount>& _right)
{
	// The allocator can't manage allocated object by itself, so allocators could be considered equal only if they are empty 
	// (if they were not empty, we have no way of telling if they are actually equal).
    return _left.empty() && _right.empty();
}

template<typename TypeT, size_t k_leftStackCount, size_t k_rightStackCount>
bool operator!=(const bwn::constaddr_deque_allocator<TypeT, k_leftStackCount>& _left, const bwn::constaddr_deque_allocator<TypeT, k_rightStackCount>& _right)
{
    return !_left.empty() || !_right.empty();
}