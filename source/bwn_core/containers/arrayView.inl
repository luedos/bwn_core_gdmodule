#pragma once

#include "bwn_core/containers/arrayView.hpp"
#include "bwn_core/debug/debug.hpp"

#include <fmt/core.h>

template<typename TypeT, typename SizeT>
bwn::basic_array_view<TypeT, SizeT>::basic_array_view()
	: m_ptr( nullptr )
	, m_size( 0 )
{}

template<typename TypeT, typename SizeT>
template<size_t k_count>
bwn::basic_array_view<TypeT, SizeT>::basic_array_view(TypeT (&_array)[k_count])
	: m_ptr( _array )
	, m_size( k_count )
{}

template<typename TypeT, typename SizeT>
bwn::basic_array_view<TypeT, SizeT>::basic_array_view(TypeT* _arrayPtr, SizeT _arraySize)
	: m_ptr( _arrayPtr )
	, m_size( _arraySize )
{}

template<typename TypeT, typename SizeT>
bwn::basic_array_view<TypeT, SizeT>::basic_array_view(iterator _begin, iterator _end)
{
	BWN_TRAP_COND(_end >= _begin, "Created array view with incorrect iterators (begin: {}, end: {}). Possible memory corruption.", fmt::ptr(_begin), fmt::ptr(_end));
	m_size = _end - _begin;
	m_ptr = _begin;
}

template<typename TypeT, typename SizeT>
template<typename DataHolderT, typename>
bwn::basic_array_view<TypeT, SizeT>::basic_array_view(DataHolderT& _holder)
	: m_ptr( _holder.data() )
	, m_size( _holder.size() )
{}

template<typename TypeT, typename SizeT>
bwn::basic_array_view<TypeT, SizeT>::basic_array_view(const basic_array_view<typename std::remove_const<TypeT>::type, SizeT> _nonConstOther)
	: m_ptr( _nonConstOther.m_ptr )
	, m_size( _nonConstOther.m_size )
{}

template<typename TypeT, typename SizeT>
const bwn::basic_array_view<TypeT, SizeT>& bwn::basic_array_view<TypeT, SizeT>::operator=(const basic_array_view<typename std::remove_const<TypeT>::type, SizeT> _nonConstOther)
{
	m_ptr = _nonConstOther.m_ptr;
	m_size = _nonConstOther.m_size;

	return *this;
}

template<typename TypeT, typename SizeT>
typename bwn::basic_array_view<TypeT, SizeT>::iterator bwn::basic_array_view<TypeT, SizeT>::begin() const
{
	return m_ptr;
}

template<typename TypeT, typename SizeT>
typename bwn::basic_array_view<TypeT, SizeT>::iterator bwn::basic_array_view<TypeT, SizeT>::end() const
{
	return m_ptr + m_size;
}

template<typename TypeT, typename SizeT>
TypeT& bwn::basic_array_view<TypeT, SizeT>::front() const
{
	BWN_TRAP_COND(!empty(), "Can't return front element from ampty array.");
	return *m_ptr;
}

template<typename TypeT, typename SizeT>
TypeT& bwn::basic_array_view<TypeT, SizeT>::back() const
{
	BWN_TRAP_COND(!empty(), "Can't return back element from ampty array.");
	return *(m_ptr + m_size - 1);
}

template<typename TypeT, typename SizeT>
TypeT* bwn::basic_array_view<TypeT, SizeT>::data() const
{
	return m_ptr;
}

template<typename TypeT, typename SizeT>
typename bwn::basic_array_view<TypeT, SizeT>::size_type bwn::basic_array_view<TypeT, SizeT>::size() const
{
	return m_size;
}

template<typename TypeT, typename SizeT>
TypeT& bwn::basic_array_view<TypeT, SizeT>::operator[] (size_type _index) const
{
	BWN_TRAP_COND(_index < m_size, "Incorrect index passed to the array view ({}), which size is {}. Possible memory corruption.", _index, m_size);
	return m_ptr[_index];
}

template<typename TypeT, typename SizeT>
bwn::basic_array_view<TypeT, SizeT> bwn::basic_array_view<TypeT, SizeT>::sub(size_type _startIndex, size_type _endIndex) const
{
	BWN_ASSERT_WARNING_COND(_startIndex <= m_size, "Incorrect start index ({}) passed to the \'sub\' of array view, with size {}. Empty array view will be returned.", _startIndex, m_size);
	if (_startIndex >= m_size){
		return basic_array_view();
	}

	return basic_array_view(m_ptr + _startIndex, std::min(m_size, _endIndex) - _startIndex);
}

template<typename TypeT, typename SizeT>
bwn::basic_array_view<TypeT, SizeT> bwn::basic_array_view<TypeT, SizeT>::sub_begin(const iterator _newEndIt) const
{
	BWN_TRAP_COND(_newEndIt >= begin() && _newEndIt <= end(), "Invalid new end iterator, this could lead to buffer overflow");
	return basic_array_view(m_ptr, _newEndIt - m_ptr);
}

template<typename TypeT, typename SizeT>
bwn::basic_array_view<TypeT, SizeT> bwn::basic_array_view<TypeT, SizeT>::sub_end(const iterator _newBeginIt) const
{
	BWN_TRAP_COND(_newBeginIt >= begin() && _newBeginIt <= end(), "Invalid new begin iterator, this could lead to buffer overflow");
	return basic_array_view(_newBeginIt, end() - _newBeginIt);
}

template<typename TypeT, typename SizeT>
bool bwn::basic_array_view<TypeT, SizeT>::empty() const
{
	return m_size == 0;
}

template<typename TypeT, typename SizeT, typename CharT> 
struct fmt::formatter<bwn::basic_array_view<TypeT, SizeT>, CharT> 
{
	constexpr auto parse (format_parse_context& _ctx) 
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const bwn::basic_array_view<TypeT, SizeT>& _view, FormatContextT& _ctx)
	{
		auto outIt = _ctx.out();

		auto write = [&outIt](const fmt::basic_string_view<CharT> _str)
		{
			outIt = fmt::detail::write(outIt, _str);
		};
		
		write(bwn::detail::ArrayOpeningStrLiteral<CharT>::k_value);

		const TypeT* it = _view.ptr();
		const TypeT*const end = it + _view.size();
		bool first = true;

		for(; it != end; ++it)
		{
			if (!first) {
				write(bwn::detail::ArrayDividerStrLiteral<CharT>::k_value);
			}
			first = false;

			outIt = fmt::format_to(outIt, bwn::detail::FormatTagStrLiteral<CharT>::k_value, *it);			
		}
		
		write(bwn::detail::ArrayClosingStrLiteral<CharT>::k_value);

		return outIt;
	}
};