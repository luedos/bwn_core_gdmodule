#pragma once

#include <initializer_list>
#include <type_traits>

namespace bwn
{
	namespace detail
	{

		template<typename NodeT, bool k_forward>
		class simple_list_iterator;

	} // namespace detail

	template<typename NodeT, bool k_forward>
	bool operator==(const bwn::detail::simple_list_iterator<NodeT, k_forward>& _left, const bwn::detail::simple_list_iterator<NodeT, k_forward>& _right);
	template<typename NodeT, bool k_forward>
	bool operator!=(const bwn::detail::simple_list_iterator<NodeT, k_forward>& _left, const bwn::detail::simple_list_iterator<NodeT, k_forward>& _right);

} // namespace bwn

namespace bwn
{
	namespace detail
	{
		template<typename NodeT, bool k_forward>
		class simple_list_iterator
		{
			template<typename OtherNodeT, bool k_otherForward>
			friend bool ::bwn::operator==(const simple_list_iterator<OtherNodeT, k_otherForward>& _left, const simple_list_iterator<OtherNodeT, k_otherForward>& _right);
			template<typename OtherNodeT, bool k_otherForward>
			friend bool ::bwn::operator!=(const simple_list_iterator<OtherNodeT, k_otherForward>& _left, const simple_list_iterator<OtherNodeT, k_otherForward>& _right);

			//
			// Construction and destruction.
			//
		public:
			simple_list_iterator();
			simple_list_iterator(NodeT* _node);
			~simple_list_iterator();

			template<typename OtherNodeT, bool k_otherForward, typename=typename std::enable_if<std::is_convertible<OtherNodeT*, NodeT*>::value>::type>
			simple_list_iterator(const simple_list_iterator<OtherNodeT, k_otherForward>& _other) noexcept;

			template<typename OtherNodeT, bool k_otherForward, typename=typename std::enable_if<std::is_convertible<OtherNodeT*, NodeT*>::value>::type>
			simple_list_iterator& operator=(const simple_list_iterator<OtherNodeT, k_otherForward>& _other) noexcept;

			//
			// Public interface.
			//
		public:
			simple_list_iterator& operator++();
			simple_list_iterator operator++(int);

			simple_list_iterator& operator--();
			simple_list_iterator operator--(int);

			NodeT& operator*() const;
			NodeT* operator->() const;

			//
			// Private members.
			//
		private:
			NodeT* m_node = nullptr;
		};

		template<typename NodeT, typename AllocatorT, bool k_propagateAllocator>
		struct SimpleListMover;

	} // namespace detail

	template<typename NodeT, typename AllocatorT = std::allocator<NodeT>>
	class simple_list
	{
		//
		// Private classes.
		//
	private:
		using AllocatorTraits = std::allocator_traits<AllocatorT>;
		template<typename OtherNodeT, typename OtherAllocatorT, bool k_propagateAllocator>
		friend struct ::bwn::detail::SimpleListMover;

		//
		// Public typedefs.
		//
	public:
		using iterator = bwn::detail::simple_list_iterator<NodeT, true>;
		using const_iterator = bwn::detail::simple_list_iterator<const typename std::remove_const<NodeT>::type, true>;

		using reverse_iterator = bwn::detail::simple_list_iterator<NodeT, false>;
		using const_reverse_iterator = bwn::detail::simple_list_iterator<const typename std::remove_const<NodeT>::type, false>;

		//
		// Construction and destruction.
		//
	public:
		simple_list();
		~simple_list();

		simple_list(const simple_list& _other);
		simple_list(simple_list&& _other) noexcept;

		simple_list& operator=(const simple_list& _other);
		simple_list& operator=(simple_list&& _other) noexcept;

		template<typename IteratorT>
		simple_list(IteratorT _begin, IteratorT _end);
		
		simple_list(const std::initializer_list<NodeT>& _initialzierList);

		//
		// Public interface.
		//
	public:
		iterator begin();
		iterator end();

		const_iterator begin() const;
		const_iterator end() const;

		const_iterator cbegin() const;
		const_iterator cend() const;

		reverse_iterator rbegin();
		reverse_iterator rend();

		const_reverse_iterator rbegin() const;
		const_reverse_iterator rend() const;

		const_reverse_iterator crbegin() const;
		const_reverse_iterator crend() const;
		
		NodeT& front();
		const NodeT& front() const;
		NodeT& back();
		const NodeT& back() const;

		NodeT* push_front(const NodeT& _node);
		NodeT* push_front(NodeT&& _node);
		template<typename...ArgTs>
		NodeT* emplace_front(ArgTs&&..._args);

		NodeT* push_back(const NodeT& _node);
		NodeT* push_back(NodeT&& _node);
		template<typename...ArgTs>
		NodeT* emplace_back(ArgTs&&..._args);

		template<bool k_forward>
		bwn::detail::simple_list_iterator<NodeT, k_forward> erase(bwn::detail::simple_list_iterator<NodeT, k_forward> _iterator);
		void erase(NodeT& _node);

		bool empty() const;

		void clear();

		//
		// Private interface.
		//
	private:
		template<typename...ArgTs>
		NodeT* new_node(ArgTs&&..._args);
		void delete_node(NodeT* _node);

		void set_first(NodeT& _node);
		void set_last(NodeT& _node);

		bool check_exist(const NodeT& _node) const;

		//
		// Private members.
		//
	private:
		NodeT* m_first = nullptr;
		NodeT* m_last = nullptr;
		AllocatorT m_allocator;
	};

} // namespace bwn

#include "bwn_core/containers/simpleList.inl"