#pragma once

#include "bwn_core/containers/smallVector.hpp"
#include "bwn_core/utility/typeParsingUtility.hpp"
#include "bwn_core/debug/debug.hpp"

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
constexpr bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::small_vector_allocator(TypeT*const _buffer, const BaseAllocatorT& _baseAllocator) noexcept
	: m_bufferPtr( _buffer )
	, m_baseAllocator( _baseAllocator )
{}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::small_vector_allocator(const small_vector_allocator& _other) noexcept
	: m_bufferPtr( _other.m_bufferPtr )
	, m_baseAllocator( _other.m_baseAllocator )
{}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::small_vector_allocator(small_vector_allocator&& _other) noexcept
	: m_bufferPtr( _other.m_bufferPtr )
	, m_baseAllocator( std::move(_other.m_baseAllocator) )
{}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>& bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::operator=(const small_vector_allocator& _other) noexcept
{
	if (this != &_other)
	{
		m_baseAllocator = _other.m_baseAllocator;
		m_bufferPtr = _other.m_bufferPtr;
	}
	return *this;
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>& bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::operator=(small_vector_allocator&& _other) noexcept
{
	if (this != &_other)
	{
		m_baseAllocator = std::move(_other.m_baseAllocator);
		m_bufferPtr = _other.m_bufferPtr;
	}
	return *this;
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
TypeT* bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::allocate(const size_t _count)
{
	if (_count <= k_stackCount)
	{
		return m_bufferPtr;
	}

	//otherwise use the default allocator
	return m_baseAllocator.allocate(_count);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
void bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::deallocate(TypeT*const _pointer, const size_t _count)
{
	if (m_bufferPtr != _pointer)
	{
		m_baseAllocator.deallocate(_pointer, _count);
	}
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector() noexcept
	: SuperType( create_allocator() )
{
	SuperType::reserve(k_stackCount);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const BaseAllocatorT& _baseAllocator) noexcept
	: SuperType( create_allocator(_baseAllocator) )
{
	SuperType::reserve(k_stackCount);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const small_vector& _other)
	: small_vector()
{
	SuperType::assign(_other.begin(), _other.end());
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const small_vector& _other, const BaseAllocatorT& _baseAllocator)
	: small_vector( _baseAllocator )
{
	SuperType::assign(_other.begin(), _other.end());
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(small_vector&& _other) noexcept(std::is_nothrow_move_constructible<TypeT>::value)
	: small_vector()
{
	SuperType::operator=(std::move(_other));
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>& bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::operator=(const small_vector& _other)
{
	if (this == &_other)
	{
		return *this;
	}

	if (_other.size() <= k_stackCount)
	{
		SuperType::clear();
		SuperType::shrink_to_fit();
		SuperType::reserve(k_stackCount);
		SuperType::assign(_other.begin(), _other.end());
	}
	else
	{
		SuperType::operator=(_other);
	}

	return *this;
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>& bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::operator=(small_vector&& _other) noexcept(std::is_nothrow_move_constructible<TypeT>::value)
{
	SuperType::operator=(std::move(_other));
	return *this;
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const size_t _count, const BaseAllocatorT& _baseAllocator)
	: small_vector( _baseAllocator )
{
	SuperType::resize(_count);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const size_t _count, const TypeT& _value, const BaseAllocatorT& _baseAllocator)
	: small_vector( _baseAllocator )
{
	SuperType::assign(_count, _value);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
template<class InputItT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(InputItT _begin, InputItT _end, const BaseAllocatorT& _baseAllocator)
	: small_vector( _baseAllocator )
{
	SuperType::assign(_begin, _end);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const std::initializer_list<TypeT> _list)
	: small_vector()
{
	SuperType::assign(_list.begin(), _list.end());
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const std::initializer_list<TypeT> _list, const BaseAllocatorT& _baseAllocator)
	: small_vector( _baseAllocator )
{
	SuperType::assign(_list.begin(), _list.end());
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
void bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::shrink_to_fit()
{
	const bool currentlySmallBuffer = is_small_buffer();
	const bool desiredSmallBuffer = SuperType::size() <= k_stackCount;

	if (currentlySmallBuffer && desiredSmallBuffer)
	{
		return;
	}

	if (!currentlySmallBuffer && !desiredSmallBuffer)
	{
		SuperType::shrink_to_fit();
		return;
	}

	SuperType temp(SuperType::get_allocator());
	temp.reserve(k_stackCount);
	temp.assign(std::make_move_iterator(SuperType::begin()), std::make_move_iterator(SuperType::end()));

	SuperType::clear();
	SuperType::shrink_to_fit();

	SuperType::operator=(std::move(temp));
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bool bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::is_small_buffer() const
{
	return SuperType::data() == reinterpret_cast<const TypeT*>(m_buffer);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
typename bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::SmallAllocatorType bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::create_allocator()
{
	return SmallAllocatorType(reinterpret_cast<TypeT*>(m_buffer));
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
typename bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::SmallAllocatorType bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::create_allocator(const BaseAllocatorT& _baseAllocator)
{
	return SmallAllocatorType(reinterpret_cast<TypeT*>(m_buffer), _baseAllocator);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::operator array_view<TypeT>()
{
	return array_view<TypeT>(SuperType::data(), SuperType::size());
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::operator array_view<const TypeT>() const
{
	return array_view<const TypeT>(SuperType::data(), SuperType::size());
}

namespace bwn
{

template<typename ValueT, size_t k_stackSize, typename AllocT>
struct VariantConverter<bwn::small_vector<ValueT, k_stackSize, AllocT>>
{
	static constexpr Variant::Type k_variantType = detail::VariantVectorConverter<bwn::small_vector<ValueT, k_stackSize, AllocT>, ValueT>::k_variantType;
	
	static Variant toVariant(const bwn::small_vector<ValueT, k_stackSize>& _vector)
	{
		return detail::VariantVectorConverter<bwn::small_vector<ValueT, k_stackSize, AllocT>, ValueT>::toVariant(_vector);
	}

	static bool fromVariantTry(const Variant& _variant, bwn::small_vector<ValueT, k_stackSize>& o_vector)
	{
		return detail::VariantVectorConverter<bwn::small_vector<ValueT, k_stackSize, AllocT>, ValueT>::fromVariantTry(_variant, o_vector);
	}

	static bool fromVariant(const Variant& _variant, bwn::small_vector<ValueT, k_stackSize>& o_vector)
	{
		return detail::VariantVectorConverter<bwn::small_vector<ValueT, k_stackSize, AllocT>, ValueT>::fromVariant(_variant, o_vector);
	}
};

} // namespace bwn