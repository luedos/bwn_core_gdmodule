#pragma once

#include "bwn_core/containers/staticFreelist.hpp"
#include "bwn_core/debug/debug.hpp"

template<typename TypeT, size_t k_count>
bwn::static_freelist<TypeT, k_count>::static_freelist()
{
	for (Index id = 0; id < k_count; ++id)
	{
		// Last index is always equal to k_invalidIndex (aka count).
		m_stack[id].next = id + 1;
	}

	m_firstEmpty = 0;
	m_totalAllocated = 0;
}

template<typename TypeT, size_t k_count>
bwn::static_freelist<TypeT, k_count>::~static_freelist() = default;

template<typename TypeT, size_t k_count>
TypeT* bwn::static_freelist<TypeT, k_count>::allocate()
{
	if (m_firstEmpty == k_invalidIndex)
	{
		return nullptr;
	}

	Item& freeItem = m_stack[m_firstEmpty];
	m_firstEmpty = freeItem.next;
	++m_totalAllocated;

	return reinterpret_cast<TypeT*>(&freeItem.object); 
}

template<typename TypeT, size_t k_count>
void bwn::static_freelist<TypeT, k_count>::deallocate(TypeT* _object)
{
	BWN_ASSERT_WARNING_COND(_object == nullptr || owns(_object), "Trying do deallocate object by address {} from freelist this memmory doesn't belongs to. Memory will not be deallocated.", (void*)_object);
	if (!owns(_object))
	{
		return;
	}

	Item*const beg = m_stack;
	const Index itemIndex = reinterpret_cast<Item*>(_object) - beg;
	m_stack[itemIndex].next = m_firstEmpty;
	m_firstEmpty = itemIndex;
	--m_totalAllocated;
}

template<typename TypeT, size_t k_count>
bool bwn::static_freelist<TypeT, k_count>::owns(TypeT* _object) const
{
	const Item*const beg = m_stack;
	const size_t diff = size_t(_object) - size_t(beg);

	return diff < (k_count * sizeof(Item)) && (diff % sizeof(Item)) == 0;
}

template<typename TypeT, size_t k_count>
bool bwn::static_freelist<TypeT, k_count>::filled() const
{
	return m_totalAllocated == k_count;
}

template<typename TypeT, size_t k_count>
bool bwn::static_freelist<TypeT, k_count>::empty() const
{
	return m_totalAllocated == 0;
}