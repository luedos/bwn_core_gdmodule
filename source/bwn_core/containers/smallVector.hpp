#pragma once

#include "bwn_core/containers/arrayView.hpp"

#include <vector>
#include <type_traits>

namespace bwn
{
	template<typename TypeT, size_t k_stackCount = 8, typename BaseAllocatorT = std::allocator<TypeT>>
	class small_vector_allocator
	{
		//
		// Public typedefs.
		//
	public:
		template<typename OtherTypeT>
		using BaseAllocatorRebindType = typename std::allocator_traits<BaseAllocatorT>::template rebind_alloc<OtherTypeT>;
		template<typename OtherTypeT>
		using RebindType = small_vector_allocator<OtherTypeT, k_stackCount, BaseAllocatorRebindType<OtherTypeT>>;

		using value_type = TypeT;
		// Because of a small buffer we can't allow small_vector_allocator to be movable/copyable by default.
		using propagate_on_container_move_assignment = std::false_type;
		using propagate_on_container_copy_assignment = std::false_type;
		using propagate_on_container_swap = std::false_type;
		// The small_buffer_allocators are not equal by default (they are not equal if their buffers are different)
		using is_always_equal = std::false_type;

		template <class OtherTypeT>
		struct rebind
		{
			using other = RebindType<OtherTypeT>;
		};

		//
		// Construction and destruction.
		//
	public:
		// The allocator should be always initialized with a buffer, because buffer here is an external thing.
		constexpr explicit small_vector_allocator(TypeT*const _buffer, const BaseAllocatorT& _baseAllocator = BaseAllocatorT()) noexcept;

		// This doesn't really makes sense because we need to initialize allocator with some buffer, and we can't use buffer of a different type.
		// template<class OtherTypeT>
		// small_vector_allocator(const RebindType<OtherTypeT>& _other) noexcept;

		small_vector_allocator(const small_vector_allocator& _other) noexcept;
		small_vector_allocator(small_vector_allocator&& _other) noexcept;
		
		small_vector_allocator& operator=(const small_vector_allocator& _other) noexcept;
		small_vector_allocator& operator=(small_vector_allocator&& _other) noexcept;

		//
		// Public interface.
		//
	public:
		TypeT* allocate(const size_t _count);
		void deallocate(TypeT*const _pointer, const size_t _count);

		// According to the C++ standard when propagate_on_container_move_assignment is set to false, the comparison operators are used
		// to check if two allocators are equal. When they are not, an element wise move is done instead of just taking over the memory.
		// For our implementation this means the comparison has to return false, if two allocators has different small buffers.
		friend constexpr bool operator==(const small_vector_allocator& _left, const small_vector_allocator& _right)
		{
			return _left.m_bufferPtr == _right.m_bufferPtr && _left.m_baseAllocator == _right.m_baseAllocator;
		}
		friend constexpr bool operator!=(const small_vector_allocator& _left, const small_vector_allocator& _right)
		{
			return !(_left == _right);
		}

		//
		// Private members.
		//
	private:
		// We are storing buffer externally (in a small_vector) so it would be possible to create copies of vector
		// internally in small_vector and then deallocate buffer with both of those vectors (helps with internal swaps basically).
		TypeT* m_bufferPtr;
		BaseAllocatorT m_baseAllocator;
	};

	template<typename TypeT, size_t k_stackCount = 8, typename BaseAllocatorT = std::allocator<TypeT>>
	class small_vector : public std::vector<TypeT, small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>>
	{
		//
		// Private typedefs.
		//
	private:
		using SmallAllocatorType = small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>;
		using SuperType = std::vector<TypeT, SmallAllocatorType>;

		//
		// Construction and destruction.
		//
	public:
		small_vector() noexcept;
		explicit small_vector(const BaseAllocatorT& _baseAllocator) noexcept;
		small_vector(const small_vector& _other);
		small_vector(const small_vector& _other, const BaseAllocatorT& _baseAllocator);
		small_vector(small_vector&& _other) noexcept(std::is_nothrow_move_constructible<TypeT>::value);
		
		small_vector& operator=(const small_vector& _other);
		small_vector& operator=(small_vector&& _other) noexcept(std::is_nothrow_move_constructible<TypeT>::value);
		
		explicit small_vector(const size_t _count, const BaseAllocatorT& _baseAllocator = BaseAllocatorT());
		small_vector(const size_t _count, const TypeT& _value, const BaseAllocatorT& _baseAllocator = BaseAllocatorT());
		template<class InputItT>
		small_vector(const InputItT _begin, const InputItT _end, const BaseAllocatorT& _baseAllocator = BaseAllocatorT());
		small_vector(const std::initializer_list<TypeT> _list);
		small_vector(const std::initializer_list<TypeT> _list, const BaseAllocatorT& _baseAllocator);

		//
		// Public interface.
		//
	public:
		void shrink_to_fit();
		bool is_small_buffer() const;

		operator array_view<TypeT>();
		operator array_view<const TypeT>() const; 

		//
		// Private methods.
		//
	private:
		SmallAllocatorType create_allocator();
		SmallAllocatorType create_allocator(const BaseAllocatorT& _baseAllocator);

		//
		// Private members.
		//
	private:
		alignas(alignof(TypeT)) std::byte m_buffer[k_stackCount * sizeof(TypeT)];
	};

} // namespace bwn

#include "bwn_core/containers/smallVector.inl"