#pragma once

#include "bwn_core/containers/simpleList.hpp"

#include "bwn_core/debug/debug.hpp"

template<typename NodeT, bool k_forward>
bool bwn::operator==(const bwn::detail::simple_list_iterator<NodeT, k_forward>& _left, const bwn::detail::simple_list_iterator<NodeT, k_forward>& _right)
{
	return _left.m_node == _right.m_node;
}
template<typename NodeT, bool k_forward>
bool bwn::operator!=(const bwn::detail::simple_list_iterator<NodeT, k_forward>& _left, const bwn::detail::simple_list_iterator<NodeT, k_forward>& _right)
{
	return _left.m_node != _right.m_node;
}

namespace bwn
{
	namespace detail
	{

		template<typename NodeT, bool k_forward>
		struct Advance
		{
			static NodeT* next(NodeT* _node)
			{
				return _node->next();
			}
		};

		template<typename NodeT>
		struct Advance<NodeT, false>
		{
			static NodeT* next(NodeT* _node)
			{
				return _node->previous();
			}
		};

	} // namespace detail
} // namespace bwn

template<typename NodeT, bool k_forward>
bwn::detail::simple_list_iterator<NodeT, k_forward>::simple_list_iterator()
	: m_node( nullptr )
{}

template<typename NodeT, bool k_forward>
bwn::detail::simple_list_iterator<NodeT, k_forward>::simple_list_iterator(NodeT* _node)
	: m_node( _node )
{}
template<typename NodeT, bool k_forward>
bwn::detail::simple_list_iterator<NodeT, k_forward>::~simple_list_iterator() = default;

template<typename NodeT, bool k_forward>
template<typename OtherNodeT, bool k_otherForward, typename>
bwn::detail::simple_list_iterator<NodeT, k_forward>::simple_list_iterator(const simple_list_iterator<OtherNodeT, k_otherForward>& _other) noexcept
	: m_node( _other.m_node )
{}

template<typename NodeT, bool k_forward>
template<typename OtherNodeT, bool k_otherForward, typename>
bwn::detail::simple_list_iterator<NodeT, k_forward>& bwn::detail::simple_list_iterator<NodeT, k_forward>::operator=(const simple_list_iterator<OtherNodeT, k_otherForward>& _other) noexcept
{
	m_node = _other.m_node;
	return *this;
}

template<typename NodeT, bool k_forward>
bwn::detail::simple_list_iterator<NodeT, k_forward>& bwn::detail::simple_list_iterator<NodeT, k_forward>::operator++()
{
	BWN_TRAP_COND(m_node != nullptr, "Can't advance node forward when node is the end one.");
	m_node = bwn::detail::Advance<NodeT, k_forward>::next(m_node);
	return *this;
}
template<typename NodeT, bool k_forward>
bwn::detail::simple_list_iterator<NodeT, k_forward> bwn::detail::simple_list_iterator<NodeT, k_forward>::operator++(int)
{
	simple_list_iterator ret(*this);
	++(*this);
	return ret;
}

template<typename NodeT, bool k_forward>
bwn::detail::simple_list_iterator<NodeT, k_forward>& bwn::detail::simple_list_iterator<NodeT, k_forward>::operator--()
{
	BWN_TRAP_COND(m_node != nullptr, "Can't advance node backward when node is the end one.");
	m_node = bwn::detail::Advance<NodeT, !k_forward>::next(m_node);
	return *this;
}
template<typename NodeT, bool k_forward>
bwn::detail::simple_list_iterator<NodeT, k_forward> bwn::detail::simple_list_iterator<NodeT, k_forward>::operator--(int)
{
	simple_list_iterator ret(*this);
	--(*this);
	return ret;
}

template<typename NodeT, bool k_forward>
NodeT& bwn::detail::simple_list_iterator<NodeT, k_forward>::operator*() const
{
	BWN_TRAP_COND(m_node != nullptr, "Dereferencing end iterator.");
	return *m_node;
}
template<typename NodeT, bool k_forward>
NodeT* bwn::detail::simple_list_iterator<NodeT, k_forward>::operator->() const
{
	BWN_TRAP_COND(m_node != nullptr, "Dereferencing end iterator.");
	return m_node;
}

template<typename NodeT, typename AllocatorT>
struct bwn::detail::SimpleListMover<NodeT, AllocatorT, false>
{
	static void copy(bwn::simple_list<NodeT, AllocatorT>& _this, const bwn::simple_list<NodeT, AllocatorT>& _other)
	{
		for (const NodeT& node : _other)
		{
			_this.pushBack(node);
		}
	}

	static void move(bwn::simple_list<NodeT, AllocatorT>& _this, bwn::simple_list<NodeT, AllocatorT>& _other)
	{
		for (NodeT& node : _other)
		{
			_this.pushBack(std::move(node));
		}
		_other.clear();
	}

	static void swap(bwn::simple_list<NodeT, AllocatorT>& _this, bwn::simple_list<NodeT, AllocatorT>& _other)
	{
		bwn::simple_list<NodeT, AllocatorT> temp = std::move(_this);
		_this = std::move(_other);
		_other = std::move(temp);
	}
};

template<typename NodeT, typename AllocatorT>
struct bwn::detail::SimpleListMover<NodeT, AllocatorT, true>
{
	static void copy(bwn::simple_list<NodeT, AllocatorT>& _this, const bwn::simple_list<NodeT, AllocatorT>& _other)
	{
		SimpleListMover<NodeT, AllocatorT, false>::copy(_this, _other);
	}

	static void move(bwn::simple_list<NodeT, AllocatorT>& _this, bwn::simple_list<NodeT, AllocatorT>& _other)
	{
		_this.m_allocator = std::move(_other.m_allocator);
		_this.m_first = _other.m_first;
		_this.m_last = _other.m_last;
		_other.m_first = nullptr;
		_other.m_last = nullptr;
	}

	static void swap(bwn::simple_list<NodeT, AllocatorT>& _this, bwn::simple_list<NodeT, AllocatorT>& _other)
	{
		std::swap(_this.m_allocator, _other.m_allocator);
		std::swap(_this.m_first, _other.m_first);
		std::swap(_this.m_last, _other.m_last);
	}
};


template<typename NodeT, typename AllocatorT>
bwn::simple_list<NodeT, AllocatorT>::simple_list()
	: m_first( nullptr )
	, m_last( nullptr )
	, m_allocator()
{}

template<typename NodeT, typename AllocatorT>
bwn::simple_list<NodeT, AllocatorT>::~simple_list()
{
	clear();
}

template<typename NodeT, typename AllocatorT>
bwn::simple_list<NodeT, AllocatorT>::simple_list(const simple_list& _other)
	: simple_list()
{
	detail::SimpleListMover<NodeT, AllocatorT, AllocatorTraits::propagate_on_container_copy_assignment::value>::copy(*this, _other);
}
template<typename NodeT, typename AllocatorT>
bwn::simple_list<NodeT, AllocatorT>::simple_list(simple_list&& _other) noexcept
	: simple_list()
{
	detail::SimpleListMover<NodeT, AllocatorT, AllocatorTraits::propagate_on_container_move_assignment::value>::move(*this, _other);
}

template<typename NodeT, typename AllocatorT>
bwn::simple_list<NodeT, AllocatorT>& bwn::simple_list<NodeT, AllocatorT>::operator=(const simple_list& _other)
{
	if (this != &_other)
	{
		clear();
		detail::SimpleListMover<NodeT, AllocatorT, AllocatorTraits::propagate_on_container_copy_assignment::value>::copy(*this, _other);
	}

	return *this;
}
template<typename NodeT, typename AllocatorT>
bwn::simple_list<NodeT, AllocatorT>& bwn::simple_list<NodeT, AllocatorT>::operator=(simple_list&& _other) noexcept
{
	if (this != &_other)
	{
		clear();
		detail::SimpleListMover<NodeT, AllocatorT, AllocatorTraits::propagate_on_container_move_assignment::value>::move(*this, _other);
	}

	return *this;
}

template<typename NodeT, typename AllocatorT>
template<typename IteratorT>
bwn::simple_list<NodeT, AllocatorT>::simple_list(IteratorT _begin, IteratorT _end)
	: simple_list()
{
	for(; _begin != _end; ++_begin)
	{
		pushBack(*_begin);
	}
}

template<typename NodeT, typename AllocatorT>
bwn::simple_list<NodeT, AllocatorT>::simple_list(const std::initializer_list<NodeT>& _initialzierList)
	: simple_list()
{
	for(const NodeT& node : _initialzierList)
	{
		pushBack(node);
	}
}

template<typename NodeT, typename AllocatorT>
typename bwn::simple_list<NodeT, AllocatorT>::iterator bwn::simple_list<NodeT, AllocatorT>::begin()
{
	return iterator( m_first );
}
template<typename NodeT, typename AllocatorT>
typename bwn::simple_list<NodeT, AllocatorT>::iterator bwn::simple_list<NodeT, AllocatorT>::end()
{
	return iterator();
}

template<typename NodeT, typename AllocatorT>
typename bwn::simple_list<NodeT, AllocatorT>::const_iterator bwn::simple_list<NodeT, AllocatorT>::begin() const
{
	return const_iterator( m_first );
}
template<typename NodeT, typename AllocatorT>
typename bwn::simple_list<NodeT, AllocatorT>::const_iterator bwn::simple_list<NodeT, AllocatorT>::end() const
{
	return const_iterator();
}

template<typename NodeT, typename AllocatorT>
typename bwn::simple_list<NodeT, AllocatorT>::const_iterator bwn::simple_list<NodeT, AllocatorT>::cbegin() const
{
	return const_iterator( m_first );
}
template<typename NodeT, typename AllocatorT>
typename bwn::simple_list<NodeT, AllocatorT>::const_iterator bwn::simple_list<NodeT, AllocatorT>::cend() const
{
	return const_iterator();
}

template<typename NodeT, typename AllocatorT>
typename bwn::simple_list<NodeT, AllocatorT>::reverse_iterator bwn::simple_list<NodeT, AllocatorT>::rbegin()
{
	return reverse_iterator( m_last );
}
template<typename NodeT, typename AllocatorT>
typename bwn::simple_list<NodeT, AllocatorT>::reverse_iterator bwn::simple_list<NodeT, AllocatorT>::rend()
{
	return reverse_iterator();
}

template<typename NodeT, typename AllocatorT>
typename bwn::simple_list<NodeT, AllocatorT>::const_reverse_iterator bwn::simple_list<NodeT, AllocatorT>::rbegin() const
{
	return const_reverse_iterator( m_last );
}
template<typename NodeT, typename AllocatorT>
typename bwn::simple_list<NodeT, AllocatorT>::const_reverse_iterator bwn::simple_list<NodeT, AllocatorT>::rend() const
{
	return const_reverse_iterator();
}

template<typename NodeT, typename AllocatorT>
typename bwn::simple_list<NodeT, AllocatorT>::const_reverse_iterator bwn::simple_list<NodeT, AllocatorT>::crbegin() const
{
	return const_reverse_iterator( m_last );
}
template<typename NodeT, typename AllocatorT>
typename bwn::simple_list<NodeT, AllocatorT>::const_reverse_iterator bwn::simple_list<NodeT, AllocatorT>::crend() const
{
	return const_reverse_iterator();
}

template<typename NodeT, typename AllocatorT>
NodeT& bwn::simple_list<NodeT, AllocatorT>::front()
{
	BWN_TRAP_COND(!empty(), "Returning front node from list when list is empty.");
	return *m_first;
}
template<typename NodeT, typename AllocatorT>
const NodeT& bwn::simple_list<NodeT, AllocatorT>::front() const
{
	BWN_TRAP_COND(!empty(), "Returning front node from list when list is empty.");
	return *m_first;
}
template<typename NodeT, typename AllocatorT>
NodeT& bwn::simple_list<NodeT, AllocatorT>::back()
{
	BWN_TRAP_COND(!empty(), "Returning back node from list when list is empty.");
	return *m_last;
}
template<typename NodeT, typename AllocatorT>
const NodeT& bwn::simple_list<NodeT, AllocatorT>::back() const
{
	BWN_TRAP_COND(!empty(), "Returning back node from list when list is empty.");
	return *m_last;
}

template<typename NodeT, typename AllocatorT>
NodeT* bwn::simple_list<NodeT, AllocatorT>::push_front(const NodeT& _node)
{
	NodeT*const ptr = new_node(_node);
	set_first(*ptr);
	return ptr;
}
template<typename NodeT, typename AllocatorT>
NodeT* bwn::simple_list<NodeT, AllocatorT>::push_front(NodeT&& _node)
{
	NodeT*const ptr = new_node(std::move(_node));
	set_first(*ptr);
	return ptr;
}
template<typename NodeT, typename AllocatorT>
template<typename...ArgTs>
NodeT* bwn::simple_list<NodeT, AllocatorT>::emplace_front(ArgTs&&..._args)
{
	NodeT*const ptr = new_node(std::forward<ArgTs>(_args)...);
	set_first(*ptr);
	return ptr;
}

template<typename NodeT, typename AllocatorT>
NodeT* bwn::simple_list<NodeT, AllocatorT>::push_back(const NodeT& _node)
{
	NodeT*const ptr = new_node(_node);
	set_last(*ptr);
	return ptr;
}
template<typename NodeT, typename AllocatorT>
NodeT* bwn::simple_list<NodeT, AllocatorT>::push_back(NodeT&& _node)
{
	NodeT*const ptr = new_node(std::move(_node));
	set_last(*ptr);
	return ptr;
}
template<typename NodeT, typename AllocatorT>
template<typename...ArgTs>
NodeT* bwn::simple_list<NodeT, AllocatorT>::emplace_back(ArgTs&&..._args)
{
	NodeT*const ptr = new_node(std::forward<ArgTs>(_args)...);
	set_last(*ptr);
	return ptr;
}

template<typename NodeT, typename AllocatorT>
template<bool k_forward>
bwn::detail::simple_list_iterator<NodeT, k_forward> bwn::simple_list<NodeT, AllocatorT>::erase(bwn::detail::simple_list_iterator<NodeT, k_forward> _iterator)
{
#if defined(DEBUG_ENABLED)
	using LocalIterator = bwn::detail::simple_list_iterator<NodeT, k_forward>;
	BWN_TRAP_COND(_iterator != LocalIterator(), "Can't erase end iterator");
#endif

	bwn::detail::simple_list_iterator<NodeT, k_forward> erasingIterator = _iterator++;
	erase(*erasingIterator);
	return _iterator;
}

template<typename NodeT, typename AllocatorT>
void bwn::simple_list<NodeT, AllocatorT>::erase(NodeT& _node)
{
	BWN_TRAP_COND(check_exist(_node), "Can't delete node which is not in the tree.");
	
	NodeT*const leftNode = _node.previous();
	NodeT*const rightNode = _node.next();

	if (leftNode != nullptr)
	{
		leftNode->next(rightNode);
	}
	else
	{
		m_first = rightNode;
	}

	if (rightNode != nullptr)
	{
		rightNode->previous(leftNode);
	}
	else
	{
		m_last = leftNode;
	}

	delete_node(&_node);
}

template<typename NodeT, typename AllocatorT>
bool bwn::simple_list<NodeT, AllocatorT>::empty() const
{
	return m_first == nullptr;
}

template<typename NodeT, typename AllocatorT>
void bwn::simple_list<NodeT, AllocatorT>::clear()
{
	NodeT* currentNode = m_first;
	while (currentNode != nullptr)
	{
		NodeT*const nextNode = currentNode->next();
		delete_node(currentNode);
		currentNode = nextNode;
	}

	m_first = nullptr;
	m_last = nullptr;
}

template<typename NodeT, typename AllocatorT>
template<typename...ArgTs>
NodeT* bwn::simple_list<NodeT, AllocatorT>::new_node(ArgTs&&..._args)
{
	NodeT*const ptr = m_allocator.allocate(1);
	new(ptr)NodeT( std::forward<ArgTs>(_args)... );
	return ptr;
}
template<typename NodeT, typename AllocatorT>
void bwn::simple_list<NodeT, AllocatorT>::delete_node(NodeT* _node)
{
	_node->~NodeT();
	m_allocator.deallocate(_node, 1);
}

template<typename NodeT, typename AllocatorT>
void bwn::simple_list<NodeT, AllocatorT>::set_first(NodeT& _node)
{
	if (m_first != nullptr)
	{
		_node.next(m_first);
		m_first->previous(&_node);
	}
	else
	{
		m_last = &_node;
	}
	m_first = &_node;
}
template<typename NodeT, typename AllocatorT>
void bwn::simple_list<NodeT, AllocatorT>::set_last(NodeT& _node)
{
	if (m_last != nullptr)
	{
		_node.previous(m_last);
		m_last->next(&_node);
	}
	else
	{
		m_first = &_node;
	}
	m_last = &_node;
}

template<typename NodeT, typename AllocatorT>
bool bwn::simple_list<NodeT, AllocatorT>::check_exist(const NodeT& _node) const
{
	const NodeT* checkedNode = m_first;
	while (checkedNode != nullptr)
	{
		if (checkedNode == &_node)
		{
			return true;
		}

		checkedNode = checkedNode->next();
	}

	return false;
}