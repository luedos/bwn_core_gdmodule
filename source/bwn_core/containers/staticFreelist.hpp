#pragma once

#include <type_traits>

namespace bwn
{
	template<typename TypeT, size_t k_count>
	class static_freelist
	{
		//
		// Private structs.
		//
	private:
		using Index =
			std::conditional_t<(k_count <= 0xFF), uint8_t,
			std::conditional_t<(k_count <= 0xFFFF), uint16_t,
			std::conditional_t<(k_count <= 0xFFFFFFFF), uint32_t,
			uint64_t>>>;

		union Item
		{
			Index next;
			typename std::aligned_storage<sizeof(TypeT), alignof(TypeT)>::type object;
		};

		static constexpr Index k_invalidIndex = k_count;
		static constexpr Index k_filledContainerTag = ~Index(0);

		//
		// Construction and destruction.
		//
	public:
		static_freelist();
		~static_freelist();

		static_freelist(const static_freelist&) = delete;
		static_freelist(static_freelist&&) noexcept = delete;

		static_freelist& operator=(const static_freelist&) = delete;
		static_freelist& operator=(static_freelist&&) noexcept = delete;

		//
		// Public interface.
		//
	public:
		TypeT* allocate();
		void deallocate(TypeT* _object);

		bool owns(TypeT* _object) const;
		bool filled() const;
		bool empty() const;

		//
		// Private members.
		//
	private:
		Item m_stack[k_count];
		Index m_firstEmpty = 0;
		Index m_totalAllocated = 0;
	};

	template<typename TypeT>
	class static_freelist<TypeT, 0>;

} // namespace bwn

#include "bwn_core/containers/staticFreelist.inl"