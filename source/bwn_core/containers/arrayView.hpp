#pragma once

#include <type_traits>

namespace bwn
{
	template<typename TypeT, typename SizeT>
	class basic_array_view;

	namespace detail
	{

		template<typename>    
		struct IsArrayView : public std::false_type
		{};

		template<typename TypeT, typename SizeT>
		struct IsArrayView<basic_array_view<TypeT, SizeT>> : public std::true_type
		{};

	} // namespace detail

	template<typename TypeT, typename SizeT>
	class basic_array_view
	{
		template<typename OtherTypeT, typename OtherSizeT>
		friend class basic_array_view;

		//
		// Public typedefs.
		//
	public:
		using size_type = SizeT;
		using iterator = TypeT*;
		static constexpr size_type npos = ~static_cast<size_type>(0);

		//
		// Construction and destruction.
		//
	public:
		basic_array_view();
		template<size_t k_count>
		basic_array_view(TypeT (&_array)[k_count]);
		basic_array_view(TypeT* _arrayPtr, SizeT _arraySize);
		basic_array_view(iterator _begin, iterator _end);

		template<typename DataHolderT, typename = 
			std::void_t< 
				typename std::enable_if< std::is_convertible<decltype(std::declval<DataHolderT>().data()), TypeT*>::value >::type,
				typename std::enable_if< std::is_convertible<decltype(std::declval<DataHolderT>().size()), SizeT>::value >::type,
				typename std::enable_if< !detail::IsArrayView<DataHolderT>::value  >
			> >
		basic_array_view(DataHolderT& _holder);

		basic_array_view(const basic_array_view<typename std::remove_const<TypeT>::type, SizeT> _nonConstOther);
		const basic_array_view& operator=(const basic_array_view<typename std::remove_const<TypeT>::type, SizeT> _nonConstOther);

		//
		// Public interface.
		//
	public:
		// Iterator to the first element.
		iterator begin() const;
		// Iterator to the last element.
		iterator end() const;
		// Iterator to the first element.
		TypeT& front() const;
		// Iterator to the last element.
		TypeT& back() const;
		// Returns pointer to the data.
		TypeT* data() const;
		// Size of the array.
		size_type size() const;
		// Returns element by it's index.
		TypeT& operator[] (size_type _index) const;
		// Creates sub array view from existing one.
		basic_array_view sub(size_type _startIndex, size_type _endIndex = npos) const;
		// Creates sub array view from current begin iterator and new end iterator.
		// New end iterator should be bigger or equal to current begin, and smaller or equal to current end iterator.
		basic_array_view sub_begin(const iterator _newEndIt) const;
		// Creates sub array view from current end iterator and new begin iterator.
		// New begin iterator should be bigger or equal to current begin, and smaller or equal to current end iterator.
		basic_array_view sub_end(const iterator _newBeginIt) const;
		// Returns true if array is empty.
		bool empty() const;

		//
		// Private members.
		//
	private:
		// Pointer to the array.
		TypeT* m_ptr = nullptr;
		// Size of the array.
		size_type m_size = 0;    
	};

	template<typename TypeT>
	using array_view = basic_array_view<TypeT, size_t>;

} // namespace bwn

#include "bwn_core/containers/arrayView.inl"