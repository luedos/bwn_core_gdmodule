#pragma once

#include "bwn_core/types/uniqueRef.hpp"

#include <core/object/ref_counted.h>

class PackedScene;

namespace bwn
{
	template<typename NodeT>
	class SceneResourceProperty
	{
		//
		// Construction and destruction.
		//
	public:
	#if defined(DEBUG_ENABLED)
		SceneResourceProperty(const char*const _variableHint);
	#else
		SceneResourceProperty();
	#endif

		//
		// Public interface.
		//
	public:
		// Sets resource. Returns true if resource is nullptr or correct type. Can return false if autoload = true but load was unsuccessfull.
		bool setResource(const Ref<PackedScene>& _resource, bool _autoLoad = true);
		const Ref<PackedScene>& getResource() const;
		
		// Getters.
		_FORCE_INLINE_ NodeT* getNode() const;
		_FORCE_INLINE_ NodeT* operator->() const;

		// Tries to load resource. Returns true if load successfull. If node already exist does nothing.
		bool softLoadResource();
		// Tries to load resource. returns true if load successful. If node already exist, removes it and loads again.
		bool forceLoadResource();
		// Fully resets resource and node.
		void reset();
		// Resets and returns current node ptr, but doesn't delete node itself.
		NodeT* releaseNode();

		// Returns true if scene resource was assign.
		bool isResourceValid() const;
		// Returns true if node was loaded.
		bool isNodeValid() const;

		//
		// Private methods.
		//
	private:
		// Tries to instance node from from current resource.
		UniqueRef<NodeT> instanceNode() const;

		//
		// Private members.
		//
	private:
		UniqueRef<NodeT> m_node;
		Ref<PackedScene> m_resource;
	#if defined(DEBUG_ENABLED)
		const char* m_variableHint = nullptr;
	#endif
};

} // namespace bwn

#include "bwn_core/types/sceneResourceProperty.inl"