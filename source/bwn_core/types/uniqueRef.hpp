#pragma once

#include "bwn_core/utility/objectCast.hpp"

#include <core/object/object.h>

#include <type_traits>

namespace bwn
{
	template<typename ObjectT>
	class UniqueRef
	{
		//
		// Public typedefs.
		//
	public:
		using MutableObjectType = typename std::remove_const<ObjectT>::type;

		//
		// Construction and destruction.
		//
	public:
		UniqueRef();
		explicit UniqueRef(MutableObjectType*const _object);
		explicit UniqueRef(MutableObjectType& _object);
		UniqueRef(std::nullptr_t);

		UniqueRef(const UniqueRef&) = delete;
		UniqueRef& operator=(const UniqueRef&) = delete;

		UniqueRef(UniqueRef&& _other) noexcept;
		UniqueRef& operator=(UniqueRef&& _other) noexcept;

		// A move constructor which is allowed only with down cast.
		template<typename OtherObjectT, typename=std::enable_if_t<std::is_base_of_v<OtherObjectT, ObjectT>>>
		explicit UniqueRef(UniqueRef<OtherObjectT>&& _other) noexcept;
		// A move operator which is allowed only with down cast.
		template<typename OtherObjectT, typename=std::enable_if_t<std::is_base_of_v<OtherObjectT, ObjectT>>>
		UniqueRef& operator=(UniqueRef<OtherObjectT>&& _other) noexcept;

		// A simple operator helper for ref resetting.
		UniqueRef& operator=(std::nullptr_t);

		~UniqueRef();

		//
		// Public interface.
		//
	public:
		// Returns object pointer (can return nullptr).
		ObjectT* get() const;
		// Simple access operator (requires inner object to be valid).
		ObjectT* operator->() const;
		// Simple access operator (requires inner object to be valid).
		ObjectT& operator*() const;
		// Returns true if reference is set and valid.
		bool valid() const;
		// Releases the pointer without deleting the object.
		ObjectT* release();
		// Resets the inner pointer. If it is still valid, deletes it.
		void reset();
		void reset(std::nullptr_t);
		void reset(ObjectT*const _object);

		//
		// Private members.
		//
	private:
		// Inits object in the sack (mostly just increments ref count in inner object is RefCounted).
		static void initObject(MutableObjectType& _object);
		static void deleteObject(MutableObjectType& _object);

		//
		// Private members.
		//
	private:
		MutableObjectType* m_objectPtr = nullptr;
	};

	template<typename TypeT, typename...ArgTs>
	UniqueRef<TypeT> makeUniqueRef(ArgTs&&..._args)
	{
		return UniqueRef<TypeT>(memnew(TypeT(std::forward<ArgTs>(_args)...)));
	}

	template<typename OtherObjectT>
	bool operator==(const UniqueRef<OtherObjectT>& _left, const std::nullptr_t);
	template<typename OtherObjectT>
	bool operator!=(const UniqueRef<OtherObjectT>& _left, const std::nullptr_t);

	template<typename OtherObjectT>
	bool operator==(const std::nullptr_t, const UniqueRef<OtherObjectT>& _right);
	template<typename OtherObjectT>
	bool operator!=(const std::nullptr_t, const UniqueRef<OtherObjectT>& _right);

	template<typename ToT, typename FromT>
	UniqueRef<ToT> objectCast(UniqueRef<FromT>&& _objectRef);
	template<typename ToT, typename FromT>
	UniqueRef<const ToT> objectCast(UniqueRef<const FromT>&& _objectRef);

} // namespace bwn

#include "bwn_core/types/uniqueRef.inl"