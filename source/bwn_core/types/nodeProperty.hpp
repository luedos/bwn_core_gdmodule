#pragma once

#include "bwn_core/containers/arrayView.hpp"
#include "bwn_core/utility/templateAlgorithms.hpp"

#include <core/string/node_path.h>
#include <scene/main/node.h>

#include <tuple>

namespace bwn
{
	// This struct used for the automatic binding of signals when node is reset.
	template<typename MethodT>
	struct NodeSignalBind
	{
	public:
		constexpr NodeSignalBind(const char*const _signalName, MethodT _methodPtr)
			: signalName( _signalName )
			, methodPtr( _methodPtr )
		{}

		const char* signalName = nullptr;
		MethodT methodPtr = nullptr;
	};

	template<typename...MethodTs>
	struct NodeSignalBindsTuple
	{
		//
		// Public typedefs.
		//
	public:
		// This is type of a class for which methods from NodeSignalBind are members.
		using ClassType = std::remove_cv_t<bwn::callable_class_type_t<variadic_template_first_t<MethodTs...>>>;

		//
		// Construction and destruction.
		//
	public:
		constexpr NodeSignalBindsTuple(const NodeSignalBind<MethodTs>&..._binds)
			: m_binds{ _binds... }
		{}

		//
		// Public interface.
		//
	public:
		// Connects all the signals to the _node.
		void connect(Node& _node, ClassType& _this) const
		{
			using Foreach = TupleForeach<std::make_index_sequence<sizeof...(MethodTs)>>;
			Foreach::call(
				[&_this, &_node](const auto& _methodBind)
				{
					_node.connect(_scs_create(_methodBind.signalName), callable_mp(&_this, _methodBind.methodPtr));
					return true;
				},
				m_binds);
		}
		// Disconnects all the signals from the _node.
		void disconnect(Node& _node, ClassType& _this) const
		{
			using Foreach = TupleForeach<std::make_index_sequence<sizeof...(MethodTs)>>;
			Foreach::call(
				[&_this, &_node](const auto& _methodBind)
				{
					_node.disconnect(_scs_create(_methodBind.signalName), callable_mp(&_this, _methodBind.methodPtr));
					return true;
				},
				m_binds);
		}

		//
		// Private methods.
		//
	private:
		std::tuple<NodeSignalBind<MethodTs>...> m_binds;
	};

	// The interface of this class is quite complicated so proper explanation is required.
	// This class is used as a simple wrapper for the all logic behind exporting node as a property of a class.
	// In godot it's impossible to simple require node as a property, we are forced to require node path, and then search for the node ourselves.
	// Another problem arise that properties are set before we enter the tree, and search for the node we can only after,
	// so we need to save the path for the node and then search for it only on ready or on enter the tree.
	//
	// This class hides most of the required logic and has 4 main function to call.
	// setNode(_this, NodePath) -> sets path and tries to reset the node ptr in the inner state, but only if parent (aka _this) currently in the tree, otherwise inner node is simply cleared.
	// getPath() -> returns setted node path. works independently on whenever node was actually retrieved or not, because path is stored separately.
	// onEnterTree(_this) -> should be called whenever main node enters the tree. this simply updates underlying node ptr by searching this node by the provided path.
	// getNode() -> directly returns the node ptr if one was set. doesn't do any checks.
	//
	// Another thing NodeProperty can do, is to automatically reconnect certain signals of a searched node. What signals are to connect and to what member methods are defined through SignalBindsT.
	// SignalBindsT can be of two things. If you don't want any signals to be automatically connected you can set it as 'void'.
	// Otherwise it should be a struct with a static (possibly constexpr) member k_binds of type NodeSignalBindsTuple.
	// NodeSignalBindsTuple itself is constructed with any number of NodeSignalBind, and each of those hold const char* string literal which represents name of a signal which should be connected,
	// and method member pointer of parent class to which this signal should be connected.
	// Using SignalBindsT NodeProperty will automatically connect and disconnect signals.
	template<typename NodeT, typename ParentClassT, typename SignalBindsT>
	class NodeProperty
	{
		//
		// Construction and destruction.
		//
	public:
	#if defined(DEBUG_ENABLED)
		explicit NodeProperty(const char*const _variableHint);
	#else
		explicit NodeProperty();
	#endif

		//
		// Public interface.
		//
	public:
		// Directly sets the node and it's path.
		// Also disconnects signals from old node, and  connects them to the new one.
		void setNode(ParentClassT& _this, NodeT*const _nodePtr);
		// Sets nodes path, and if _this node is currently in the tree resets node pointer as well (if not, it would be required additional call to onEnterTree).
		// Also disconnects signals from old node, and  connects them to the new one.
		void setNode(ParentClassT& _this, const NodePath& _path);

		// Reinitialize the node from the path which is saved internally.
		// Need to be called on tree entering, because we are not initializing node if we are self is not in the tree.
		void onEnterTree(ParentClassT& _this);

		// Returns the node pointer.
		NodeT* getNode() const;
		// Dereferences the node.
		NodeT* operator->() const;
		// Returns the path saved internally.
		const NodePath& getPath() const;

		// Returns true if property is initialized and node was found.
		bool isValid() const;

		// Resets the current ptr, but not the path.
		void clearPtr(ParentClassT& _this);

		//
		// Private methods.
		//
	private:
		// Updates node ptr from the path.
		void updateNode(ParentClassT& _this);
		// Rebinds signals from old node (if not nullptr) and sets them to the new node (also if not nullptr).
		void rebindNodePtr(ParentClassT& _this, NodeT*const _oldPtr, NodeT*const _newPtr) const;

		//
		// Private members.
		//
	private:
		NodeT* m_ptr = nullptr;
		NodePath m_path;
	#if defined(DEBUG_ENABLED)
		const char* m_variableHint = nullptr;
	#endif
	};

} // namespace bwn

#include "bwn_core/types/nodeProperty.inl"