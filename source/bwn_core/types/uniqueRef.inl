#pragma once

#include "bwn_core/types/uniqueRef.hpp"
#include "bwn_core/debug/debug.hpp"

#include <core/object/ref_counted.h> 
#include <scene/main/node.h>

template<typename ObjectT>
bwn::UniqueRef<ObjectT>::UniqueRef()
	: m_objectPtr( nullptr )
{}

template<typename ObjectT>
bwn::UniqueRef<ObjectT>::UniqueRef(MutableObjectType*const _object)
	: m_objectPtr( _object )
{
	if (m_objectPtr != nullptr)
	{
		initObject(*m_objectPtr);
	}
}

template<typename ObjectT>
bwn::UniqueRef<ObjectT>::UniqueRef(MutableObjectType& _object)
	: m_objectPtr( &_object )
{	
	initObject(*m_objectPtr);
}

template<typename ObjectT>
bwn::UniqueRef<ObjectT>::UniqueRef(std::nullptr_t)
	: m_objectPtr( nullptr )
{}

template<typename ObjectT>
bwn::UniqueRef<ObjectT>::UniqueRef(UniqueRef&& _other) noexcept
	: m_objectPtr( _other.m_objectPtr )
{
	_other.m_objectPtr = nullptr;
}

template<typename ObjectT>
bwn::UniqueRef<ObjectT>& bwn::UniqueRef<ObjectT>::operator=(UniqueRef&& _other) noexcept
{
	if (likely(this != &_other))
	{
		reset();
		m_objectPtr = _other.m_objectPtr;
		_other.m_objectPtr = nullptr;
	}

	return *this;
}

template<typename ObjectT>
template<typename OtherObjectT, typename>
bwn::UniqueRef<ObjectT>::UniqueRef(UniqueRef<OtherObjectT>&& _other) noexcept
	: m_objectPtr( _other.m_objectPtr )
{
	static constexpr bool k_isSelfConst = std::is_same_v<ObjectT, std::remove_const_t<ObjectT>>;
	static constexpr bool k_isOtherConst = std::is_same_v<OtherObjectT, std::remove_const_t<OtherObjectT>>;
	static_assert(k_isSelfConst == k_isOtherConst || k_isSelfConst, "Can't const cast UniqueRef.");

	_other.m_objectPtr = nullptr;
}

template<typename ObjectT>
template<typename OtherObjectT, typename>
bwn::UniqueRef<ObjectT>& bwn::UniqueRef<ObjectT>::operator=(UniqueRef<OtherObjectT>&& _other) noexcept
{
	static constexpr bool k_isSelfConst = std::is_same_v<ObjectT, std::remove_const_t<ObjectT>>;
	static constexpr bool k_isOtherConst = std::is_same_v<OtherObjectT, std::remove_const_t<OtherObjectT>>;
	static_assert(k_isSelfConst == k_isOtherConst || k_isSelfConst, "Can't const cast UniqueRef.");
	
	reset();
	m_objectPtr = _other.m_objectPtr;
	_other.m_objectPtr = nullptr;

	return *this;
}

template<typename ObjectT>
bwn::UniqueRef<ObjectT>& bwn::UniqueRef<ObjectT>::operator=(std::nullptr_t)
{
	reset();
	return *this;
}

template<typename ObjectT>
bwn::UniqueRef<ObjectT>::~UniqueRef()
{
	reset();
}

template<typename ObjectT>
ObjectT* bwn::UniqueRef<ObjectT>::get() const
{
	return m_objectPtr;
}

template<typename ObjectT>
ObjectT* bwn::UniqueRef<ObjectT>::operator->() const
{
	BWN_ASSERT_ERROR_COND(m_objectPtr != nullptr, "De-referencing nullptr UniqueRef.");
	return m_objectPtr;
}

template<typename ObjectT>
ObjectT& bwn::UniqueRef<ObjectT>::operator*() const
{
	BWN_ASSERT_ERROR_COND(m_objectPtr != nullptr, "De-referencing nullptr UniqueRef.");
	return *m_objectPtr;
}

template<typename ObjectT>
bool bwn::UniqueRef<ObjectT>::valid() const
{
	return m_objectPtr != nullptr;
}

template<typename ObjectT>
ObjectT* bwn::UniqueRef<ObjectT>::release()
{
	ObjectT*const releasedPtr = m_objectPtr;
	m_objectPtr = nullptr;

	return releasedPtr;
}

template<typename ObjectT>
void bwn::UniqueRef<ObjectT>::reset()
{
	if (m_objectPtr != nullptr)
	{
		deleteObject(*m_objectPtr);
	}

	m_objectPtr = nullptr;
}

template<typename ObjectT>
void bwn::UniqueRef<ObjectT>::reset(std::nullptr_t)
{
	reset();
}

template<typename ObjectT>
void bwn::UniqueRef<ObjectT>::reset(ObjectT*const _object)
{
	if (m_objectPtr != _object)
	{
		if (m_objectPtr != nullptr)
		{
			deleteObject(*m_objectPtr);
		}

		m_objectPtr = _object;
	}
}

template<typename ObjectT>
void bwn::UniqueRef<ObjectT>::initObject(MutableObjectType& _object)
{
	if constexpr (std::is_base_of_v<RefCounted, ObjectT>)
	{
		_object.reference();
	}
	else if constexpr (std::is_same_v<Object, ObjectT>)
	{
		if (RefCounted*const refCounted = Object::cast_to<RefCounted>(&_object))
		{
			refCounted->reference();
		}
	}
}

template<typename ObjectT>
void bwn::UniqueRef<ObjectT>::deleteObject(MutableObjectType& _object)
{
	if constexpr (std::is_base_of_v<Node, ObjectT>)
	{
		_object.queue_free();
	}
	else if constexpr (std::is_base_of_v<RefCounted, ObjectT>)
	{
		const bool lastReference = _object.unreference();
		BWN_ASSERT_WARNING_COND(lastReference, "UniqueRef holding not unique ptr to a ref_counted, this is probably not desired.");
		if (lastReference)
		{
			memdelete(&_object);
		}
	}
	else if constexpr (std::is_same_v<Object, ObjectT>)
	{
		if (Node*const castedNode = Object::cast_to<Node>(&_object))
		{
			castedNode->queue_free();
		}
		else if (RefCounted*const refCounted = Object::cast_to<RefCounted>(&_object))
		{
			const bool lastReference = refCounted->unreference();
			BWN_ASSERT_WARNING_COND(lastReference, "UniqueRef holding not unique ptr to a ref_counted, this is probably not desired.");
			if (lastReference)
			{
				memdelete(&_object);
			}
		}
		else
		{
			memdelete(&_object);
		}
	}
	else
	{
		memdelete(&_object);
	}
}

template<typename OtherObjectT>
bool bwn::operator==(const UniqueRef<OtherObjectT>& _left, const std::nullptr_t)
{
	return !_left.valid();
}

template<typename OtherObjectT>
bool bwn::operator!=(const UniqueRef<OtherObjectT>& _left, const std::nullptr_t)
{
	return _left.valid();
}

template<typename OtherObjectT>
bool bwn::operator==(const std::nullptr_t, const UniqueRef<OtherObjectT>& _right)
{
	return !_right.valid();
}

template<typename OtherObjectT>
bool bwn::operator!=(const std::nullptr_t, const UniqueRef<OtherObjectT>& _right)
{
	return _right.valid();
}

template<typename ToT, typename FromT>
bwn::UniqueRef<ToT> bwn::objectCast(UniqueRef<FromT>&& _objectRef)
{
	static_assert(std::is_base_of_v<ToT, FromT> || std::is_base_of_v<FromT, ToT>, "Casted types should exist in the same hierarchy.");
	static constexpr bool k_isToConst = std::is_same_v<ToT, std::remove_const_t<ToT>>;
	static constexpr bool k_isFromConst = std::is_same_v<ToT, std::remove_const_t<ToT>>;
	static_assert(k_isToConst == k_isFromConst || k_isToConst, "Can't const cast UniqueRef.");

	FromT*const objectPtr = _objectRef.get();
	if (objectPtr == nullptr)
	{
		return nullptr;
	}

	if constexpr (std::is_base_of_v<ToT, FromT>)
	{
		bwn::UniqueRef<ToT> castedReference(*objectPtr);
		_objectRef.release();
		return castedReference;
	}
	else
	{
		ToT*const castedPtr = Object::cast_to<ToT>(objectPtr);
		if (castedPtr == nullptr)
		{
			// In case if casting was unsuccessful, we will not clear original reference.
			return nullptr;
		}

		bwn::UniqueRef<ToT> castedReference(*castedPtr);
		_objectRef.release();
		return castedReference;
	}
}

template<typename ToT, typename FromT>
bwn::UniqueRef<const ToT> bwn::objectCast(UniqueRef<const FromT>&& _objectRef)
{
	static_assert(std::is_base_of_v<ToT, FromT> || std::is_base_of_v<FromT, ToT>, "Casted types should exist in the same hierarchy.");

	const FromT*const objectPtr = _objectRef.get();
	if (objectPtr == nullptr)
	{
		return nullptr;
	}

	if constexpr (std::is_base_of_v<ToT, FromT>)
	{
		bwn::UniqueRef<const ToT> castedReference(*objectPtr);
		_objectRef.release();
		return castedReference;
	}
	else
	{
		const ToT*const castedPtr = Object::cast_to<ToT>(objectPtr);
		if (castedPtr == nullptr)
		{
			// In case if casting was unsuccessful, we will not clear original reference.
			return nullptr;
		}

		bwn::UniqueRef<const ToT> castedReference(*castedPtr);
		_objectRef.release();
		return castedReference;
	}
}