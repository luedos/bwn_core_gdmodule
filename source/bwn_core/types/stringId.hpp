#pragma once

#include "bwn_core/types/stringView.hpp"

#include <core/typedefs.h>

#if defined(DEBUG_ENABLED)
	#include <core/string/ustring.h>
	#include <core/string/string_name.h>
#endif

#include <string>
#include <string_view>

class Variant;
class String;
class StringName;
struct StaticCString;

namespace bwn
{
	class StringId
	{
		//
		// Public constants.
		//
	public:
		static constexpr uint32_t k_invalidHash = 0;

		//
		// Construction and destruction.
		//
	public:
	#if defined(DEBUG_ENABLED)
		StringId();
		explicit StringId(const StaticCString &_staticString);
		template<typename CharT>
		explicit StringId(const std::basic_string_view<CharT> _view);
	#else
		constexpr _ALWAYS_INLINE_ StringId();
		explicit constexpr _ALWAYS_INLINE_ StringId(const uint32_t _hash);
		template<typename CharT>
		constexpr _ALWAYS_INLINE_ StringId(const std::basic_string_view<CharT> _view);
	#endif
		StringId(const Variant& _variant);
		StringId(const StringName& _stringName);

		//
		// Public interface.
		//
	public:
		// Returns hash value.
		uint32_t getHash() const;
		// Returns true if id was initialized with something other then k_invalidHash.
		bool isValid() const;
		// Operator which will allow the usage of StringId in godot.
		operator Variant() const;
		// Uses basically a copy of a godot String hash function.
		template<typename CharT>
		static constexpr uint32_t createHash(const CharT*const _key, const size_t _length);
		template<typename CharT>
		static constexpr uint32_t createHash(const std::basic_string_view<CharT> _view);

	#if defined(DEBUG_ENABLED)
		String getString() const;
	#endif

		//
		// Private members.
		//
	private:
		// Hash value.
	#if defined(DEBUG_ENABLED)
		StringName m_hash;
	#else
		uint32_t m_hash = k_invalidHash;
	#endif
};

bool operator==(const bwn::StringId _left, const bwn::StringId _right);
bool operator!=(const bwn::StringId _left, const bwn::StringId _right);
bool operator<=(const bwn::StringId _left, const bwn::StringId _right);
bool operator>=(const bwn::StringId _left, const bwn::StringId _right);
bool operator>(const bwn::StringId _left, const bwn::StringId _right);
bool operator<(const bwn::StringId _left, const bwn::StringId _right);

} // namespace bwn


#if defined(DEBUG_ENABLED)
MAKE_TYPE_INFO(bwn::StringId, Variant::STRING);
template <>
struct PtrToArg<bwn::StringId>
{
	_FORCE_INLINE_ static bwn::StringId convert(const void*const _ptr)
	{
		return bwn::StringId(bwn::wrap_view(*reinterpret_cast<const String *>(_ptr)));
	}
	_FORCE_INLINE_ static void encode(const bwn::StringId _val, void*const _ptr)
	{
		*((String*)_ptr) = _val.getString();
	}
};
template <>
struct PtrToArg<const bwn::StringId&>
{
	_FORCE_INLINE_ static bwn::StringId convert(const void*const _ptr)
	{
		return bwn::StringId(bwn::wrap_view(*reinterpret_cast<const String*>(_ptr)));
	}
	_FORCE_INLINE_ static void encode(const bwn::StringId _val, void*const _ptr)
	{
		*((String*)_ptr) = _val.getString();
	}
};
template<>
struct VariantInternalAccessor<bwn::StringId>
{
	static _FORCE_INLINE_ bwn::StringId get(const Variant*const _variant)
	{ 
		return bwn::StringId(*_variant);
	}

	static _FORCE_INLINE_ void set(Variant*const _variant, const bwn::StringId& _id)
	{ 
		*_variant = _id.getString();
	}
};
#else
template <>
struct PtrToArg<bwn::StringId>
{
	_FORCE_INLINE_ static bwn::StringId convert(const void*const _ptr)
	{
		return bwn::StringId(*reinterpret_cast<const uint32_t*>(_ptr));
	}
	_FORCE_INLINE_ static void encode(const bwn::StringId _val, void*const _ptr)
	{
		*((uint32_t*)_ptr) = _val.getHash();
	}
};
template <>
struct PtrToArg<const bwn::StringId&>
{
	_FORCE_INLINE_ static bwn::StringId convert(const void*const _ptr)
	{
		return bwn::StringId(*reinterpret_cast<const uint32_t*>(_ptr));
	}
	_FORCE_INLINE_ static void encode(const bwn::StringId _val, void*const _ptr)
	{
		*((uint32_t*)_ptr) = _val.getHash();
	}
};
template<>
struct VariantInternalAccessor<bwn::StringId>
{
	static _FORCE_INLINE_ bwn::StringId get(const Variant*const _variant)
	{ 
		return bwn::StringId(*_variant);
	}

	static _FORCE_INLINE_ void set(Variant*const _variant, const bwn::StringId& _id)
	{ 
		*_variant = _id.getHash();
	}
};
#endif

#if defined(DEBUG_ENABLED)
	#define BWN_STRING_ID(_string) ::bwn::StringId(StaticCString::create(_string))
#else
	#define BWN_STRING_ID(_string) ::bwn::StringId(std::integral_constant<uint32_t, ::bwn::StringId::createHash(::bwn::wrap_view(_string))>::value)
#endif

#include "bwn_core/types/stringId.inl"