#pragma once

#include <core/string/ustring.h>
#include <core/variant/variant.h>

#include <string_view>
#include <utility>

namespace bwn
{

	// The only required thing from this string_view, is to be compliant with godot String.
	using string_view = std::basic_string_view<char32_t>;

	_FORCE_INLINE_ string_view wrap_view(const String& _string)
	{
		return string_view(_string.ptr(), _string.length());
	}

	constexpr _FORCE_INLINE_ string_view wrap_view(const string_view& _view)
	{
		return _view;
	}

	constexpr _FORCE_INLINE_ ::std::string_view wrap_view(const char*const _string)
	{
		return ::std::string_view(_string);
	}

	constexpr _FORCE_INLINE_ string_view wrap_view(const char32_t*const _string)
	{
		return string_view(_string);
	}

	// Split path into first folder and rest.
	std::pair<bwn::string_view, bwn::string_view> splitPathOnBegin(const bwn::string_view& _path);
	// Split path of rest path and last folder.
	std::pair<bwn::string_view, bwn::string_view> splitPathOnEnd(const bwn::string_view& _path);

	template<typename>
	struct VariantConverter;

	template<>
	struct VariantConverter<bwn::string_view>
	{
		static constexpr Variant::Type k_variantType = Variant::Type::STRING;

		static Variant toVariant(const bwn::string_view& _view)
		{
			return String(_view.data(), _view.length());
		}

		static String toVariantStorage(const bwn::string_view& _view)
		{
			return String(_view.data(), _view.length());
		}
	};

} // namespace bwn
