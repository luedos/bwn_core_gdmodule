#include "precompiled_core.hpp"

#include "bwn_core/types/sourcePoint.hpp"

#include "bwn_core/utility/mathUtility.hpp"

std::size_t std::hash<bwn::SourcePoint>::operator()(const bwn::SourcePoint& _obj) const
{
	const std::hash<std::string_view> stringHashCreator;
	const std::hash<uint32_t> uintHashCreator;

	const std::size_t fileHash = stringHashCreator(std::string_view(_obj.file));
	const std::size_t funcHash = stringHashCreator(std::string_view(_obj.func));
	const std::size_t lineHash = uintHashCreator(_obj.line);

	return fileHash 
		^ bwn::rotl(funcHash, 1)
		^ bwn::rotl(lineHash, 2);
}

bool bwn::operator==(const bwn::SourcePoint& _left, const bwn::SourcePoint& _right)
{
	// Because SSourcePoint is initialized with strings literals which taken from __FUNCTION__/__FILE__ macros,
	// we can just compare char pointers, and not strings itself.
	return _left.func == _right.func
		&& _left.file == _right.file
		&& _left.line == _right.line;
}