#include "precompiled_core.hpp"

#include "bwn_core/types/stringView.hpp"

std::pair<bwn::string_view, bwn::string_view> bwn::splitPathOnBegin(const bwn::string_view& _path)
{
	using ReturnPair = std::pair<bwn::string_view, bwn::string_view>;

	const char32_t*const startIt = _path.begin();
	const char32_t* leftEndIt = _path.begin();
	const char32_t* rightStartIt = _path.begin();
	const char32_t*const endIt = _path.end();

	while (rightStartIt != endIt)
	{
		const char32_t currentChar = *rightStartIt;
		++rightStartIt;
		if (currentChar == '\\' || currentChar == '/')
		{
			break;
		}
		leftEndIt = rightStartIt;
	}

	ReturnPair ret;
	ret.first = bwn::string_view(startIt, leftEndIt - startIt);
	ret.second = bwn::string_view(rightStartIt, endIt - rightStartIt);

	return ret;
}

std::pair<bwn::string_view, bwn::string_view> bwn::splitPathOnEnd(const bwn::string_view& _path)
{
	using ReturnPair = std::pair<bwn::string_view, bwn::string_view>;
	using RIterator = bwn::string_view::const_reverse_iterator;

	RIterator leftEndIt = _path.rbegin();
	RIterator rightStartIt = _path.rbegin();
	const RIterator endIt = _path.rend();

	while (leftEndIt != endIt)
	{
		const char32_t currentChar = *leftEndIt;
		++leftEndIt;
		if (currentChar == '\\' || currentChar == '/')
		{
			break;
		}
		rightStartIt = leftEndIt;
	}

	const char32_t*const startPtr = _path.begin();
	const char32_t*const leftEndPtr = leftEndIt.base();
	const char32_t*const rightStartPtr = rightStartIt.base();
	const char32_t*const endPtr = _path.end();

	ReturnPair ret;
	ret.first = bwn::string_view(startPtr, leftEndPtr - startPtr);
	ret.second = bwn::string_view(rightStartPtr, endPtr - rightStartPtr);

	return ret;
}