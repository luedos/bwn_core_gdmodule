#pragma once

#include <functional>

#define BWN_SOURCE_POINT() (bwn::SourcePoint{__FUNCTION__, __FILE__, __LINE__})

namespace bwn
{
	struct SourcePoint
	{
		//
		// Construction and destruction.
		//
	public:
		SourcePoint()
			: func ( "" )
			, file ( "" )
			, line ( 0 )
		{}
		SourcePoint(const char* _func, const char* _file, uint32_t _line)
			: func ( _func )
			, file ( _file )
			, line ( _line )
		{}

		//
		// Public members.
		//
	public:
		// String members are meant to be initialized with string literals.
		// Because ot that we use simple const char* instead of full string classes.
		// Name of the function.
		const char* func = nullptr;
		// Name of the source file.
		const char* file = nullptr;
		// Line from the source file.
		uint32_t line = 0;
	};

	bool operator==(const bwn::SourcePoint& left, const bwn::SourcePoint& right);

} // namespace bwn

namespace std
{
	template<>
	struct hash<bwn::SourcePoint>
	{
		std::size_t operator()(const bwn::SourcePoint& _obj) const;
	};

} // namespace std