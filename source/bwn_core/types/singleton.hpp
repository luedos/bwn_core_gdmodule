#pragma once

#include <type_traits>

namespace bwn
{
	template<typename>
	class PlainSingleton;

	template<typename TypeT>
	class SingletonHandle
	{
		// Basicaly, where is no need to be friends with everyone
		friend PlainSingleton<typename std::remove_cv<TypeT>::type>;

		//
		// Construction and destruction.
		//
		// Constructor is deliberately marked as private.
	private:
		// Construction for ptr.
		SingletonHandle(TypeT*const _ptr);
		// No copy constructors.
		SingletonHandle(const SingletonHandle&) = delete;
		SingletonHandle& operator=(const SingletonHandle&) = delete;
	public:
		// Destructor.
		~SingletonHandle();
		// Move constructor and operator.
		SingletonHandle(SingletonHandle&& _other);
		SingletonHandle& operator=(SingletonHandle&& _other);

		//
		// Public interface.
		//
	public:
		// Just an operators for singleton calling. 
		TypeT* operator->() const;
		// Operator which tells if singletone is initialized.
		operator bool() const;
		// Comparison operators, because sometimes it's usefull.
		bool operator==(const TypeT*const _other) const;

		//
		// Private members.
		//
	private:
		TypeT* m_ptr = nullptr;
	};

	// This is simple singletone class, wich just handles holding and returning of the instance, 
	// not the logic how this instance is going to be initialized.
	template<typename TypeT>
	class PlainSingleton
	{
		//
		// Public interface.
		//
	public:
		// Returns an instance of the singletone
		static SingletonHandle<TypeT> getInstance();
		// Returns an instance of type singleton but doesn't check if instance is empty.
		static SingletonHandle<TypeT> getInstanceUnsafe();
		// Checks if singletone is initialized.
		static bool isSingletonValid();

		//
		// Protected members.
		//
	protected:
		// Initialses singletone instance with given value.
		static void initSingleton(TypeT*const _instance);
		// Clears and returns current instance.
		static TypeT* clearSingleton();
		// Clears singleton only if current instance is _instance.
		static void clearSingletonSafe(TypeT*const _instance);

		//
		// Private members.
		//
	private:
		// Actual singleton instance.
		static TypeT* ms_instance;

	};

	// This singleton initialized manually, by calling createSingleton/destroySingleton.
	template<typename TypeT>
	class ManualSingleton : public PlainSingleton<TypeT>
	{
		//
		// Public interface.
		//
	public:
		// Creates new instance of the singleton.
		static void createSingleton();
		// Destroys instance of the singleton.
		static void destroySingleton();

		using PlainSingleton<TypeT>::isSingletonValid;
		using PlainSingleton<TypeT>::getInstance;

		//
		// Protected members.
		//
	protected:
	    using PlainSingleton<TypeT>::initSingleton;
	    using PlainSingleton<TypeT>::clearSingleton;
	};

	// This singleton is initialized by constructor/destructor.
	// This mostly done so godot singletons can be used from c++ as well.
	template<typename TypeT>
	class AutoSingleton : public PlainSingleton<TypeT>
	{
		//
		// Construction and destruction.
		//
	public:
		AutoSingleton();
		~AutoSingleton();

		//
		// Public interface.
		//
	public:
		using PlainSingleton<TypeT>::isSingletonValid;
		using PlainSingleton<TypeT>::getInstance;

		//
		// Protected members.
		//
	protected:
	    using PlainSingleton<TypeT>::initSingleton;
	    using PlainSingleton<TypeT>::clearSingleton;
	};

} // namespace bwn

#include "bwn_core/types/singleton.inl"