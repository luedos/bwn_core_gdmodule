#pragma once 

#include "bwn_core/types/stringId.hpp"

#include "bwn_core/utility/godotFormating.hpp"

#include <functional>
#include <cstring>

template<>
struct std::hash<bwn::StringId>
{
	std::size_t operator()(const bwn::StringId& _id) const noexcept
	{
		return _id.getHash();
	}
};

template<>
struct fmt::formatter<bwn::StringId, char>
{
	template <typename FormatParserT>
	constexpr auto parse (FormatParserT& _ctx) 
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const bwn::StringId& _id, FormatContextT& _ctx)
	{
		if (_id.isValid())
		{
			// By default, we will try to pasrse string, not hash.
#if defined(DEBUG_ENABLED)
			const String idString = _id.getString();
			if (!idString.is_empty())
			{
				return fmt::format_to(_ctx.out(), "ID<'{}' 0x{:0>8x}>", idString, _id.getHash());
			}
			else
#endif
			{
				return fmt::format_to(_ctx.out(), "ID<0x{:0>8x}>", _id.getHash());
			}
		}
		else
		{
			return fmt::detail::write(_ctx.out(), fmt::basic_string_view<char>("ID<invalid>"));
		}
	}
};

template<>
struct fmt::formatter<bwn::StringId, char32_t>
{
	template <typename FormatParserT>
	constexpr auto parse (FormatParserT& _ctx)
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const bwn::StringId& _id, FormatContextT& _ctx)
	{
		if (_id.isValid())
		{
			// By default, we will try to pasrse string, not hash.
#if defined(DEBUG_ENABLED)
			const String idString = _id.getString();
			if (!idString.is_empty())
			{
				return fmt::format_to(_ctx.out(), U"ID<'{}' 0x{:0>8x}>", idString, _id.getHash());
			}
			else
#endif
			{
				return fmt::format_to(_ctx.out(), U"ID<0x{:0>8x}>", _id.getHash());
			}
		}
		else
		{
			return fmt::detail::write(_ctx.out(), fmt::basic_string_view<char32_t>(U"ID<invalid>"));
		}
	}
};

#if !defined(DEBUG_ENABLED)
constexpr _ALWAYS_INLINE_ bwn::StringId::StringId()
	: m_hash( k_invalidHash )
{}

constexpr _ALWAYS_INLINE_ bwn::StringId::StringId(const uint32_t _hash)
	: m_hash( _hash )
{}

template<typename CharT>
constexpr _ALWAYS_INLINE_ bwn::StringId::StringId(const std::basic_string_view<CharT> _view)
	: m_hash( createHash(_view) )
{}
#else
template<typename CharT>
bwn::StringId::StringId(const std::basic_string_view<CharT> _view)
	: m_hash( String( _view.data(), _view.length() ) )
{}
#endif

template<typename CharT>
constexpr uint32_t bwn::StringId::createHash(const CharT*const _key, std::size_t _length)
{
	uint32_t hashv = 5381;
	for (size_t index = 0; index < _length; index++)
	{
		hashv = ((hashv << 5) + hashv) + _key[index]; /* hash * 33 + c */
	}

	return hashv;
}

template<typename CharT>
constexpr uint32_t bwn::StringId::createHash(std::basic_string_view<CharT> _view)
{
	return createHash(_view.data(), _view.length());
}