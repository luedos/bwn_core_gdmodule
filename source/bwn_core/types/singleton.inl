#pragma once

#include "bwn_core/types/singleton.hpp"
#include "bwn_core/debug/debug.hpp"

template<typename TypeT>
bwn::SingletonHandle<TypeT>::SingletonHandle(TypeT*const _ptr)
	: m_ptr{ _ptr }
{}

template<typename TypeT>
bwn::SingletonHandle<TypeT>::~SingletonHandle()
{
	// nothing for now, but mb, we will add some sort of lock in the future.
}

template<typename TypeT>
bwn::SingletonHandle<TypeT>::SingletonHandle(SingletonHandle&& _other)
	: m_ptr{ _other.m_ptr }
{
	_other.m_ptr = nullptr;
}

template<typename TypeT>
bwn::SingletonHandle<TypeT>& bwn::SingletonHandle<TypeT>::operator=(SingletonHandle&& _other)
{
	if (this != &_other)
	{
		m_ptr = _other.m_ptr;
		_other.m_ptr = nullptr;
	}

	return *this;
}

template<typename TypeT>
TypeT* bwn::SingletonHandle<TypeT>::operator->() const
{
	return m_ptr;
}

template<typename TypeT>
bwn::SingletonHandle<TypeT>::operator bool() const
{
	return m_ptr != nullptr;
}

template<typename TypeT>
bool bwn::SingletonHandle<TypeT>::operator==(const TypeT* _other) const
{
	return m_ptr == _other;
}

template<typename TypeT>
TypeT* bwn::PlainSingleton<TypeT>::ms_instance = nullptr;

template<typename TypeT>
bwn::SingletonHandle<TypeT> bwn::PlainSingleton<TypeT>::getInstance()
{
	BWN_TRAP_COND(ms_instance != nullptr, "Singleton is not initialized.");
	return SingletonHandle<TypeT>( ms_instance );
}

template<typename TypeT>
bwn::SingletonHandle<TypeT> bwn::PlainSingleton<TypeT>::getInstanceUnsafe()
{
	return SingletonHandle<TypeT>( ms_instance );
}

template<typename TypeT>
bool bwn::PlainSingleton<TypeT>::isSingletonValid()
{
	return ms_instance != nullptr;
}

template<typename TypeT>
void bwn::PlainSingleton<TypeT>::initSingleton(TypeT*const _instance)
{
	BWN_ASSERT_WARNING_COND(ms_instance == nullptr, "Repeated singleton initialization.");
	if (ms_instance == nullptr)
	{
		ms_instance = _instance;
	}
}

template<typename TypeT>
TypeT* bwn::PlainSingleton<TypeT>::clearSingleton()
{
	BWN_ASSERT_WARNING_COND(ms_instance != nullptr, "Freeing of uninitialized singleton.");
	TypeT*const current = ms_instance;
	ms_instance = nullptr;
	return current;
}

template<typename TypeT>
void bwn::PlainSingleton<TypeT>::clearSingletonSafe(TypeT*const _instance)
{
	if (ms_instance == _instance)
	{
		ms_instance = nullptr;
	}
}

template<typename TypeT>
void bwn::ManualSingleton<TypeT>::createSingleton()
{
	BWN_ASSERT_WARNING_COND(!isSingletonValid(), "Repeated singleton initialization.");
	if (!isSingletonValid())
	{
		initSingleton(new TypeT{});
	}
}

template<typename TypeT>
void bwn::ManualSingleton<TypeT>::destroySingleton()
{
	TypeT*const instance = clearSingleton();
	if (instance != nullptr)
	{
		delete instance;
	}
}

template<typename TypeT>
bwn::AutoSingleton<TypeT>::AutoSingleton()
{
	initSingleton(static_cast<TypeT *>(this));
}

template<typename TypeT>
bwn::AutoSingleton<TypeT>::~AutoSingleton()
{
	if (getInstance() == static_cast<TypeT*>(this)) {
		clearSingleton();
	}
}
