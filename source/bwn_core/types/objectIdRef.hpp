#pragma once

#include "bwn_core/utility/objectCast.hpp"

#include <core/object/object_id.h>

namespace bwn
{

	template<typename ObjectT>
	class ObjectIdRef
	{
		//
		// Construction and destruction.
		//
	public:
		ObjectIdRef();
		explicit ObjectIdRef(ObjectT*const _object);
		explicit ObjectIdRef(ObjectT& _object);
		ObjectIdRef(std::nullptr_t);

		ObjectIdRef(const ObjectIdRef& _other);
		ObjectIdRef& operator=(const ObjectIdRef& _other);

		ObjectIdRef(ObjectIdRef&& _other) noexcept;
		ObjectIdRef& operator=(ObjectIdRef&& _other) noexcept;

		// A copy constructor which is allowed only with down cast.
		template<typename OtherObjectT, typename=std::enable_if_t<std::is_base_of_v<OtherObjectT, ObjectT>>>
		explicit ObjectIdRef(const ObjectIdRef<OtherObjectT>& _other);
		// A copy operator which is allowed only with down cast.
		template<typename OtherObjectT, typename=std::enable_if_t<std::is_base_of_v<OtherObjectT, ObjectT>>>
		ObjectIdRef& operator=(const ObjectIdRef<OtherObjectT>& _other);

		// A move constructor which is allowed only with down cast.
		template<typename OtherObjectT, typename=std::enable_if_t<std::is_base_of_v<OtherObjectT, ObjectT>>>
		explicit ObjectIdRef(ObjectIdRef<OtherObjectT>&& _other) noexcept;
		// A move operator which is allowed only with down cast.
		template<typename OtherObjectT, typename=std::enable_if_t<std::is_base_of_v<OtherObjectT, ObjectT>>>
		ObjectIdRef& operator=(ObjectIdRef<OtherObjectT>&& _other) noexcept;

		// A simple operator helper for ref resetting.
		ObjectIdRef& operator=(std::nullptr_t);

		~ObjectIdRef();

		//
		// Public interface.
		//
	public:
		// Returns object pointer from the class db (can return nullptr).
		ObjectT* get() const;
		// Simple access operator (requires inner object to be valid).
		ObjectT* operator->() const;
		// Simple access operator (requires inner object to be valid).
		ObjectT& operator*() const;
		// Returns true if object id is set and object valid.
		bool valid() const;
		// Resets the inner pointer.
		void reset();
		void reset(std::nullptr_t);
		void reset(ObjectT*const _object);

		// Returns underlying object id (object id is unchanged, e.g. reference would not check if object still exist).
		ObjectID getId() const;

		//
		// Private members.
		//
	private:
		ObjectID m_id;
	};

	template<typename ObjectT>
	bool operator==(const ObjectIdRef<ObjectT>& _left, const std::nullptr_t);
	template<typename ObjectT>
	bool operator!=(const ObjectIdRef<ObjectT>& _left, const std::nullptr_t);

	template<typename ObjectT>
	bool operator==(const std::nullptr_t, const ObjectIdRef<ObjectT>& _right);
	template<typename ObjectT>
	bool operator!=(const std::nullptr_t, const ObjectIdRef<ObjectT>& _right);

	template<typename LeftObjectT, typename RightObjectT>
	bool operator==(const ObjectIdRef<LeftObjectT>& _left, const ObjectIdRef<RightObjectT>& _right);
	template<typename LeftObjectT, typename RightObjectT>
	bool operator!=(const ObjectIdRef<LeftObjectT>& _left, const ObjectIdRef<RightObjectT>& _right);

	template<typename ToT, typename FromT>
	ObjectIdRef<ToT> objectCast(const ObjectIdRef<FromT>& _objectRef);
	template<typename ToT, typename FromT>
	ObjectIdRef<const ToT> objectCast(const ObjectIdRef<const FromT>& _objectRef);
} // namespace bwn

#include "bwn_core/types/objectIdRef.inl"