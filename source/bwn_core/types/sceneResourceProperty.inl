#pragma once

#include "bwn_core/types/sceneResourceProperty.hpp"
#include "bwn_core/utility/objectCast.hpp"

#include <scene/resources/packed_scene.h>
#include <core/object/class_db.h>

#if defined(DEBUG_ENABLED)
template<typename NodeT>
bwn::SceneResourceProperty<NodeT>::SceneResourceProperty(const char*const _variableHint)
	: m_node()
	, m_resource()
	, m_variableHint( _variableHint )
{}
#else
template<typename NodeT>
bwn::SceneResourceProperty<NodeT>::SceneResourceProperty()
	: m_node()
	, m_resource()
{}
#endif

template<typename NodeT>
bool bwn::SceneResourceProperty<NodeT>::setResource(const Ref<PackedScene>& _resource, bool _autoLoad)
{
	m_resource = _resource;
	m_node.reset();

	if (m_resource.is_null())
	{
		// If resource was simply reset, no additional checks needed.
		return true;
	}

	const Ref<SceneState> state = m_resource->get_state();
	if (!ClassDB::is_parent_class(state->get_node_type(0), NodeT::get_class_static()))
	{
		BWN_ASSERT_WARNING(
			"Incorrect scene type provided as a resource for the {} variable. Expected '{}', but provided '{}'.",
			m_variableHint,
			NodeT::get_class_static(),
			state->get_node_type(0));

		m_resource.unref();
		return false;
	}

	if (_autoLoad)
	{
		return forceLoadResource();
	}

	return true;
}

template<typename NodeT>
const Ref<PackedScene>& bwn::SceneResourceProperty<NodeT>::getResource() const
{
	return m_resource;
}

template<typename NodeT>
_FORCE_INLINE_ NodeT* bwn::SceneResourceProperty<NodeT>::getNode() const
{
	return m_node.get();
}

template<typename NodeT>
_FORCE_INLINE_ NodeT* bwn::SceneResourceProperty<NodeT>::operator->() const
{
	BWN_ASSERT_ERROR_COND(m_node != nullptr, "Dereferensing SceneResourceProperty of {} variable with not loaded node.", m_variableHint);
	return m_node.get();
}

template<typename NodeT>
bool bwn::SceneResourceProperty<NodeT>::softLoadResource()
{
	if (m_node != nullptr)
	{
		return true;
	}

	return forceLoadResource();
}

template<typename NodeT>
bool bwn::SceneResourceProperty<NodeT>::forceLoadResource()
{
	m_node = instanceNode();
	return m_node != nullptr;
}

template<typename NodeT>
void bwn::SceneResourceProperty<NodeT>::reset()
{
	m_node.reset();
	m_resource.unref();
}

template<typename NodeT>
NodeT* bwn::SceneResourceProperty<NodeT>::releaseNode()
{
	return m_node.release();
}

template<typename NodeT>
bool bwn::SceneResourceProperty<NodeT>::isResourceValid() const
{
	return m_resource.is_valid();
}

template<typename NodeT>
bool bwn::SceneResourceProperty<NodeT>::isNodeValid() const
{
	return m_node != nullptr;
}

template<typename NodeT>
bwn::UniqueRef<NodeT> bwn::SceneResourceProperty<NodeT>::instanceNode() const
{
	if (m_resource.is_null())
	{
		return nullptr;
	}

	Node*const nodePtr = m_resource->instantiate();
	UniqueRef<Node> node = UniqueRef<Node>(nodePtr);
	BWN_ASSERT_WARNING_COND(
		nodePtr != nullptr,
		"Failed to load node for {} variable from scene '{}'.",
		m_variableHint,
		m_resource->get_path());
	
	UniqueRef<NodeT> castedNode = bwn::objectCast<NodeT>(std::move(node));
	BWN_ASSERT_WARNING_COND(
		castedNode == nullptr && nodePtr != nullptr,
		"Failed to cast node while loading {} variable from resource '{}'. Expecting type '{}', but provided '{}'.",
		m_variableHint,
		m_resource->get_path(),
		NodeT::get_class_static(),
		nodePtr->get_class());

	return castedNode;
}