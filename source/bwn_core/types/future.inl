#pragma once

#include "bwn_core/types/future.hpp"
#include "bwn_core/thread/fakelock.hpp"
#include "bwn_core/results/systemResult.hpp"
#include "bwn_core/results/systemFacility.hpp"

#include <optional>

template<typename ValueT>
class bwn::detail::FutureBase<ValueT>::Implementation
{
	//
	// Public classes.
	//
public:
	enum EState
	{
		k_processing = 1 << 0,
		k_finished = 1 << 1,
		k_released = 1 << 2,
		k_aborted = 1 << 3,

		k_all = 0xF
	};

	//
	// Private constants.
	//
private:
	// The number of bits for which stage number is shifted, or basically actual bit size of state.
	static constexpr uint32_t k_stateBitShift = 8;
	// The bit mask for the current resource load stage which is shifted to 8 bits.
	static constexpr uint32_t k_shiftedPercentMask = (~static_cast<uint32_t>(0)) << k_stateBitShift;
	// This is mask for the actual state part (EState) of 'state' variable Basically the oposite of stage mask.
	static constexpr uint32_t k_stateMask = ~k_shiftedPercentMask;

	//
	// Construction and destruction.
	//
public:
	Implementation();
	~Implementation();

	Implementation(Implementation&&) noexcept = delete;
	Implementation(const Implementation&) = delete;
	Implementation& operator=(Implementation&&) noexcept = delete;
	Implementation& operator=(const Implementation&) = delete;

	//
	// Public interface.
	//
public:
	// Sets current state of the future. Additionally accepts a valid current state
	// (if current state is not valid, will return without state change).
	// Returns true if state was changed. 
	bool setState(const EState _state, const EState _validState = EState::k_all);
	// Setting abort as a special state, because we should not have the ability to abort if future finished processing.
	void abort();
	// Sets current percent of resource loading.
	void setCurrentPercent(const uint32_t _percent);

	// Returns current state of the future.
	EState getState() const;
	// Returns current percent of resource loaded.
	uint32_t getCurrentPercent() const;

	// Returns system result of the operation. If the operation is not finished, returns result with error 'ERR_BUSY'.
	SystemResult getResult() const;

	// Returns the value by copying it from result. Works only if the result is copy constructible.
	template<typename OtherValueT>
	std::enable_if_t<std::is_copy_constructible_v<OtherValueT>, std::optional<ValueT>> getValue() const;
	// Returns value from the future, and also clears that result (works for both move/copy assignable values).
	// The release operation is always possible with the implementation, but it's unadvised to use releaseValue and getValue in the same parent class.
	std::optional<ValueT> releaseValue();

	// Sets the result. This is templated mostly just for universal reference.
	template<typename ValueRefT>
	void setResult(ValueRefT&& _object);
	// Sets result of a shared state as error.
	void setError(const SystemResult& _result);

	//
	// Private members.
	//
private:
	// The object from load operation.
	std::optional<ValueT> m_object;
	// The result from the load operation.
	SystemResult m_result;
	// The main atomic which holds everything together.
	// It tracks actual state of the operation,
	// as well as current stage of the resource polling.
	std::atomic_int_fast32_t m_state;
#if defined(DEBUG_ENABLED)
	// This lock is mostly used to debug that no race conditions are made.
	mutable bwn::fakelock m_fakelock;
#endif
};

template<typename ValueT>
bwn::detail::FutureBase<ValueT>::Implementation::Implementation()
	: m_object( nullptr )
	, m_result( SystemFacility::createSuccess() )
	, m_state( static_cast<int>(EState::k_processing) )
#if defined(DEBUG_ENABLED)
	, m_fakelock()
#endif
{}

template<typename ValueT>
bwn::detail::FutureBase<ValueT>::Implementation::~Implementation() = default;

template<typename ValueT>
bool bwn::detail::FutureBase<ValueT>::Implementation::setState(const EState _state, const EState _validState)
{
	int_fast32_t currentState = m_state.load(std::memory_order_acquire);

	while((currentState & _validState) != 0)
	{
		const int_fast32_t currentPercentNoState = (currentState & Implementation::k_shiftedPercentMask);
		const int_fast32_t nextState = currentPercentNoState | static_cast<int_fast32_t>(_state);

		if (m_state.compare_exchange_weak(currentState, nextState, std::memory_order_release))
		{
			return true;
		}
	}

	return false;
}

template<typename ValueT>
void bwn::detail::FutureBase<ValueT>::Implementation::setCurrentPercent(const uint32_t _percent)
{
	BWN_ASSERT_ERROR_COND(_percent < (1u << 24), "The percent of resource loaded can't be more then 2^24, this could lead to incorrect results.");

	int_fast32_t currentState = m_state.load(std::memory_order_acquire);
	const int_fast32_t shiftedPercent = _percent << Implementation::k_stateBitShift;

	for(;;)
	{
		const int_fast32_t currentStateNoPercent = (currentState & Implementation::k_stateMask);
		const int_fast32_t nextState = currentStateNoPercent | shiftedPercent;

		if (m_state.compare_exchange_weak(currentState, nextState, std::memory_order_release))
		{
			break;
		}
	}
}

template<typename ValueT>
typename bwn::detail::FutureBase<ValueT>::Implementation::EState bwn::detail::FutureBase<ValueT>::Implementation::getState() const
{
	const uint32_t currentState = static_cast<uint32_t>(m_state.load(std::memory_order_acquire));

	return static_cast<EState>(currentState & k_stateMask);
}

template<typename ValueT>
uint32_t bwn::detail::FutureBase<ValueT>::Implementation::getCurrentPercent() const
{
	const uint32_t currentState = static_cast<uint32_t>(m_state.load(std::memory_order_acquire));

	return (currentState & k_shiftedPercentMask) >> k_stateBitShift;
}

template<typename ValueT>
template<typename OtherValueT>
std::enable_if_t<std::is_copy_constructible_v<OtherValueT>, std::optional<ValueT>> bwn::detail::FutureBase<ValueT>::Implementation::getValue() const
{
	if (getState() != Implementation::EState::k_finished)
	{
		BWN_ASSERT_ERROR("The future isn't finished, can't return the object.");
		return std::nullopt;
	}

#if defined(DEBUG_ENABLED)
	std::unique_lock<bwn::fakelock> lock(m_fakelock);
#endif

	return m_object;
}

template<typename ValueT>
std::optional<ValueT> bwn::detail::FutureBase<ValueT>::Implementation::releaseValue()
{
	// Not only checking if current state is finished, but also setting state as released.
	if (!setState(EState::k_released, EState::k_finished))
	{
		BWN_ASSERT_ERROR("The future isn't finished, can't return the object.");
		return std::nullopt;
	}

#if defined(DEBUG_ENABLED)
	std::unique_lock<bwn::fakelock> lock(m_fakelock);
#endif

	std::optional<ValueT> ret = std::nullopt;
	ret.swap(m_object);
	return ret;
}

template<typename ValueT>
template<typename ValueRefT>
void bwn::detail::FutureBase<ValueT>::Implementation::setResult(ValueRefT&& _object)
{
	BWN_ASSERT_WARNING_COND(getState() != EState::k_finished, "This is generally unadvised to set the result of the future the second time.");

#if defined(DEBUG_ENABLED)
	std::unique_lock<bwn::fakelock> lock(m_fakelock);
#endif

	m_object = std::forward<ValueRefT>(_object);
	m_result = SystemFacility::createSuccess();

	m_state.store(static_cast<int_fast32_t>(Implementation::EState::k_finished) | static_cast<int_fast32_t>(100 << k_stateBitShift));
}

template<typename ValueT>
void bwn::detail::FutureBase<ValueT>::Implementation::setError(const SystemResult& _result)
{
	BWN_ASSERT_WARNING_COND(getState() != EState::k_finished, "This is generally unadvised to set the result of the future the second time.");
	BWN_ASSERT_WARNING_COND(!_result.success(), "Setting successful result to the future via 'setError' method. This should not have happen.");

#if defined(DEBUG_ENABLED)
	std::unique_lock<bwn::fakelock> lock(m_fakelock);
#endif

	m_object.reset();
	m_result = _result;

	// The result should not be successful, but just in case. 
	if (m_result.success())
	{
		m_result = SystemFacility::createError(Error::FAILED);
	}

	m_state.store(static_cast<int_fast32_t>(Implementation::EState::k_finished) | static_cast<int_fast32_t>(100 << k_stateBitShift));	
}

template<typename ValueT>
bwn::SystemResult bwn::detail::FutureBase<ValueT>::Implementation::getResult() const
{
	if (getState() == Implementation::EState::k_processing)
	{
		return SystemFacility::createError(Error::ERR_BUSY, L"The future is still running.");
	}

#if defined(DEBUG_ENABLED)
	std::unique_lock<bwn::fakelock> lock(m_fakelock);
#endif

	return m_result;
}

template<typename ValueT>
bwn::detail::FutureBase<ValueT>::FutureBase() = default;
template<typename ValueT>
bwn::detail::FutureBase<ValueT>::~FutureBase() = default;

template<typename ValueT>
void bwn::detail::FutureBase<ValueT>::init()
{
	m_implementation = std::make_shared<Implementation>();
}

template<typename ValueT>
void bwn::detail::FutureBase<ValueT>::clear()
{
	m_implementation.reset();
}

template<typename ValueT>
void bwn::detail::FutureBase<ValueT>::abort()
{
	if (m_implementation)
	{
		m_implementation->setState(Implementation::EState::k_aborted, Implementation::EState::k_processing);
	}
}

template<typename ValueT>
void bwn::detail::FutureBase<ValueT>::setLoadPercent(const uint32_t _percent)
{
	if (m_implementation)
	{
		m_implementation->setCurrentPercent(_percent);
	}
}

template<typename ValueT>
bool bwn::detail::FutureBase<ValueT>::isValid() const
{
	return m_implementation.operator bool();
}

template<typename ValueT>
bool bwn::detail::FutureBase<ValueT>::isFinished() const
{
	if (!m_implementation)
	{
		return false;
	}

	const typename Implementation::EState currentState = m_implementation->getState();

	return currentState == Implementation::EState::k_finished
		|| currentState == Implementation::EState::k_aborted
		|| currentState == Implementation::EState::k_released;
}

template<typename ValueT>
bool bwn::detail::FutureBase<ValueT>::isAborted() const
{
	return m_implementation && m_implementation->getState() == Implementation::EState::k_aborted;
}

template<typename ValueT>
bool bwn::detail::FutureBase<ValueT>::isSucceeded() const
{
	return m_implementation 
		&& m_implementation->getState() == Implementation::EState::k_finished 
		&& m_implementation->getResult().success();
}

template<typename ValueT>
uint32_t bwn::detail::FutureBase<ValueT>::getLoadPercent() const
{
	if (!m_implementation)
	{
		return 0;
	}

	return m_implementation->getCurrentPercent();
}

template<typename ValueT>
bwn::SystemResult bwn::detail::FutureBase<ValueT>::getResult() const
{
	if (!m_implementation)
	{
		return SystemFacility::createError( Error::ERR_UNCONFIGURED, L"The resource future is not initialized." );
	}

	return m_implementation->getResult();
}

template<typename ValueT>
void bwn::detail::FutureBase<ValueT>::setError(const SystemResult& _result)
{
	BWN_ASSERT_ERROR_COND(m_implementation, "Can't set error for the future, because future is not initialized.");
	if (m_implementation)
	{
		m_implementation->setError(_result);
	}
}

template<typename ValueT>
void bwn::detail::FutureBase<ValueT>::setResult(ValueT&& _object)
{
	BWN_ASSERT_ERROR_COND(m_implementation, "Error setting object to the future by moving, because future is not initialized.");
	if (m_implementation)
	{
		m_implementation->template setResult(std::move(_object));
	}
}

template<typename ValueT>
void bwn::detail::FutureBase<ValueT>::setResult(const ValueT& _object)
{
	if (m_implementation)
	{
		m_implementation->template setResult(_object);
	}
}

template<typename ValueT>
bwn::Future<ValueT, bwn::EFutureType::k_copy>::Future() = default;
template<typename ValueT>
bwn::Future<ValueT, bwn::EFutureType::k_copy>::~Future() = default;

template<typename ValueT>
std::optional<ValueT> bwn::Future<ValueT, bwn::EFutureType::k_copy>::getValue() const
{
	static_assert(std::is_copy_constructible_v<ValueT>, "The value can be copied from the future only in case if it's copy constructible.");

	if (!this->m_implementation)
	{
		BWN_ASSERT_ERROR("Can't return the object because the future is not initialized.");
		return std::nullopt;
	}

	return this->m_implementation->template getValue<ValueT>();
}

template<typename ValueT>
bwn::Future<ValueT, bwn::EFutureType::k_release>::Future() = default;
template<typename ValueT>
bwn::Future<ValueT, bwn::EFutureType::k_release>::~Future() = default;

template<typename ValueT>
std::optional<ValueT> bwn::Future<ValueT, bwn::EFutureType::k_release>::releaseValue() const
{
	if (!this->m_implementation)
	{
		BWN_ASSERT_ERROR("Can't return the object because the future is not initialized.");
		return std::nullopt;
	}

	return this->m_implementation->releaseValue();
}

template<typename ValueT>
bool bwn::Future<ValueT, bwn::EFutureType::k_release>::isReleased() const
{
	// If implementation doesn't exist, we think of it as not released.
	return this->m_implementation && this->m_implementation->getState() == detail::FutureBase<ValueT>::Implementation::EState::k_released;
}
