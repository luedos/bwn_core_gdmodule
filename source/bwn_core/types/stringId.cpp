#include "precompiled_core.hpp"

#include "bwn_core/types/stringId.hpp"

#if defined(DEBUG_ENABLED)
bwn::StringId::StringId()
	: m_hash()
{}

bwn::StringId::StringId(const StaticCString &_staticString)
	: m_hash( _staticString )
{}
#endif

bwn::StringId::StringId(const Variant& _variant)
{
#if defined(DEBUG_ENABLED)
	m_hash = _variant.operator StringName();
#else
	switch (_variant.get_type())
	{
		case Variant::Type::NIL:
		{
			m_hash = 0;
		}
		break;

		case Variant::Type::INT:
		{
			m_hash = _variant.operator unsigned int();
		}
		break;

		default:
		{
			const StringName name = _variant.operator StringName();
			m_hash = name.hash();
		}
		break;
	}
#endif
}

bwn::StringId::StringId(const StringName& _stringName)
#if defined(DEBUG_ENABLED)
	: m_hash( _stringName )
#else
	: m_hash( _stringName.hash() )
#endif
{

}

uint32_t bwn::StringId::getHash() const
{
#if defined(DEBUG_ENABLED)
	return m_hash.hash();
#else
	return m_hash;
#endif
}

bool bwn::StringId::isValid() const
{
#if defined(DEBUG_ENABLED)
	return m_hash.hash() != 0;
#else
	return m_hash != k_invalidHash;
#endif
}

bwn::StringId::operator Variant() const
{
	return Variant(m_hash);
}

#if defined(DEBUG_ENABLED)
String bwn::StringId::getString() const
{
	return m_hash;
}
#endif

bool bwn::operator==(const bwn::StringId _left, const bwn::StringId _right)
{
	return _left.getHash() == _right.getHash();
}

bool bwn::operator!=(const bwn::StringId _left, const bwn::StringId _right)
{
	return _left.getHash() != _right.getHash();
}

bool bwn::operator<=(const bwn::StringId _left, const bwn::StringId _right)
{
	return _left.getHash() <= _right.getHash();
}

bool bwn::operator>=(const bwn::StringId _left, const bwn::StringId _right)
{
	return _left.getHash() >= _right.getHash();
}

bool bwn::operator>(const bwn::StringId _left, const bwn::StringId _right)
{
	return _left.getHash() > _right.getHash();
}

bool bwn::operator<(const bwn::StringId _left, const bwn::StringId _right)
{
	return _left.getHash() < _right.getHash();
}
