#pragma once

#include "bwn_core/types/objectIdRef.hpp"
#include <core/object/object.h>
#include <type_traits>

template<typename ObjectT>
bwn::ObjectIdRef<ObjectT>::ObjectIdRef()
	: m_id()
{}

template<typename ObjectT>
bwn::ObjectIdRef<ObjectT>::ObjectIdRef(ObjectT*const _object)
	: m_id()
{
	if (_object != nullptr)
	{
		m_id = _object->get_instance_id();
	}
}

template<typename ObjectT>
bwn::ObjectIdRef<ObjectT>::ObjectIdRef(ObjectT& _object)
	: m_id( _object.get_instance_id() )
{}

template<typename ObjectT>
bwn::ObjectIdRef<ObjectT>::ObjectIdRef(std::nullptr_t)
	: m_id()
{}

template<typename ObjectT>
bwn::ObjectIdRef<ObjectT>::ObjectIdRef(const ObjectIdRef& _other)
	: m_id( _other.m_id )
{}

template<typename ObjectT>
bwn::ObjectIdRef<ObjectT>& bwn::ObjectIdRef<ObjectT>::operator=(const ObjectIdRef& _other)
{
	m_id = _other.m_id;
	return *this;
}

template<typename ObjectT>
bwn::ObjectIdRef<ObjectT>::ObjectIdRef(ObjectIdRef&& _other) noexcept
	: m_id( _other.m_id )
{
	_other.m_id = ObjectID();
}

template<typename ObjectT>
bwn::ObjectIdRef<ObjectT>& bwn::ObjectIdRef<ObjectT>::operator=(ObjectIdRef&& _other) noexcept
{
	if (this != &_other)
	{
		m_id = _other.m_id;
		_other.m_id = ObjectID();
	}

	return *this;
}

template<typename ObjectT>
template<typename OtherObjectT, typename>
bwn::ObjectIdRef<ObjectT>::ObjectIdRef(const ObjectIdRef<OtherObjectT>& _other)
	: m_id( _other.m_id )
{
	static constexpr bool k_isSelfConst = std::is_same_v<ObjectT, std::remove_const_t<ObjectT>>;
	static constexpr bool k_isOtherConst = std::is_same_v<OtherObjectT, std::remove_const_t<OtherObjectT>>;
	static_assert(k_isSelfConst == k_isOtherConst || k_isSelfConst, "Can't const cast ObjectIdRef.");
}

template<typename ObjectT>
template<typename OtherObjectT, typename>
bwn::ObjectIdRef<ObjectT>& bwn::ObjectIdRef<ObjectT>::operator=(const ObjectIdRef<OtherObjectT>& _other)
{
	static constexpr bool k_isSelfConst = std::is_same_v<ObjectT, std::remove_const_t<ObjectT>>;
	static constexpr bool k_isOtherConst = std::is_same_v<OtherObjectT, std::remove_const_t<OtherObjectT>>;
	static_assert(k_isSelfConst == k_isOtherConst || k_isSelfConst, "Can't const cast ObjectIdRef.");

	m_id = _other.m_id;
	return *this;
}

template<typename ObjectT>
template<typename OtherObjectT, typename>
bwn::ObjectIdRef<ObjectT>::ObjectIdRef(ObjectIdRef<OtherObjectT>&& _other) noexcept
	: m_id( _other.m_id )
{
	static constexpr bool k_isSelfConst = std::is_same_v<ObjectT, std::remove_const_t<ObjectT>>;
	static constexpr bool k_isOtherConst = std::is_same_v<OtherObjectT, std::remove_const_t<OtherObjectT>>;
	static_assert(k_isSelfConst == k_isOtherConst || k_isSelfConst, "Can't const cast ObjectIdRef.");

	_other.m_id = ObjectID();
}

template<typename ObjectT>
template<typename OtherObjectT, typename>
bwn::ObjectIdRef<ObjectT>& bwn::ObjectIdRef<ObjectT>::operator=(ObjectIdRef<OtherObjectT>&& _other) noexcept
{
	static constexpr bool k_isSelfConst = std::is_same_v<ObjectT, std::remove_const_t<ObjectT>>;
	static constexpr bool k_isOtherConst = std::is_same_v<OtherObjectT, std::remove_const_t<OtherObjectT>>;
	static_assert(k_isSelfConst == k_isOtherConst || k_isSelfConst, "Can't const cast ObjectIdRef.");
	
	if (this != &_other)
	{
		m_id = _other.m_id;
		_other.m_id = Object();
	}

	return *this;
}

template<typename ObjectT>
bwn::ObjectIdRef<ObjectT>& bwn::ObjectIdRef<ObjectT>::operator=(std::nullptr_t)
{
	reset();
	return *this;
}

template<typename ObjectT>
bwn::ObjectIdRef<ObjectT>::~ObjectIdRef() = default;

template<typename ObjectT>
ObjectT* bwn::ObjectIdRef<ObjectT>::get() const
{
	Object*const instance = ObjectDB::get_instance(m_id);
	if constexpr (std::is_same_v<ObjectT, Object>)
	{
		return instance;
	}
	else
	{
		if (instance == nullptr)
		{
			return nullptr;
		}
		BWN_TRAP_COND(
			instance->is_class(ObjectT::get_class_static()),
			"Incorrectly retrieving object of type '{}' when the desired type reference was assigned with is '{}'.",
			instance->get_class(),
			ObjectT::get_class_static());

		return static_cast<ObjectT*>(instance);
	}
}

template<typename ObjectT>
ObjectT* bwn::ObjectIdRef<ObjectT>::operator->() const
{
	ObjectT*const instance = get();
	BWN_TRAP_COND(instance != nullptr, "Dereferencing nullptr object id ref.");
	return instance;
}

template<typename ObjectT>
ObjectT& bwn::ObjectIdRef<ObjectT>::operator*() const
{
	ObjectT*const instance = get();
	BWN_TRAP_COND(instance != nullptr, "Dereferencing nullptr object id ref.");
	return *instance;
}

template<typename ObjectT>
bool bwn::ObjectIdRef<ObjectT>::valid() const
{
	return m_id.is_valid() && ObjectDB::get_instance(m_id) != nullptr;
}

template<typename ObjectT>
void bwn::ObjectIdRef<ObjectT>::reset()
{
	m_id = ObjectID();
}

template<typename ObjectT>
void bwn::ObjectIdRef<ObjectT>::reset(std::nullptr_t)
{
	m_id = ObjectID();
}

template<typename ObjectT>
void bwn::ObjectIdRef<ObjectT>::reset(ObjectT*const _object)
{
	*this = ObjectIdRef(_object);
}

template<typename ObjectT>
ObjectID bwn::ObjectIdRef<ObjectT>::getId() const
{
	return m_id;
}

template<typename OtherObjectT>
bool bwn::operator==(const ObjectIdRef<OtherObjectT>& _left, const std::nullptr_t)
{
	return !_left.valid();
}

template<typename OtherObjectT>
bool bwn::operator!=(const ObjectIdRef<OtherObjectT>& _left, const std::nullptr_t)
{
	return _left.valid();
}

template<typename OtherObjectT>
bool bwn::operator==(const std::nullptr_t, const ObjectIdRef<OtherObjectT>& _right)
{
	return !_right.valid();
}

template<typename OtherObjectT>
bool bwn::operator!=(const std::nullptr_t, const ObjectIdRef<OtherObjectT>& _right)
{
	return _right.valid();
}

template<typename LeftObjectT, typename RightObjectT>
bool bwn::operator==(const ObjectIdRef<LeftObjectT>& _left, const ObjectIdRef<RightObjectT>& _right)
{
	return _left.getId() == _right.getId();
}

template<typename LeftObjectT, typename RightObjectT>
bool bwn::operator!=(const ObjectIdRef<LeftObjectT>& _left, const ObjectIdRef<RightObjectT>& _right)
{
	return _left.getId() != _right.getId();
}

template<typename ToT, typename FromT>
bwn::ObjectIdRef<ToT> bwn::objectCast(const ObjectIdRef<FromT>& _objectRef)
{
	static_assert(std::is_base_of_v<ToT, FromT> || std::is_base_of_v<FromT, ToT>, "Casted types should exist in the same hierarchy.");
	static constexpr bool k_isToConst = std::is_same_v<ToT, std::remove_const_t<ToT>>;
	static constexpr bool k_isFromConst = std::is_same_v<ToT, std::remove_const_t<ToT>>;
	static_assert(k_isToConst == k_isFromConst || k_isToConst, "Can't const cast ObjectIdRef.");

	FromT*const objectPtr = _objectRef.get();
	if constexpr (std::is_base_of_v<ToT, FromT>)
	{
		return bwn::ObjectIdRef<ToT>(objectPtr);
	}
	else
	{
		ToT*const castedPtr = Object::cast_to<ToT>(objectPtr);
		if (castedPtr == nullptr)
		{
			// In case if casting was unsuccessful, we will not clear original reference.
			return nullptr;
		}

		return bwn::ObjectIdRef<ToT>(castedPtr);
	}
}

template<typename ToT, typename FromT>
bwn::ObjectIdRef<const ToT> bwn::objectCast(const ObjectIdRef<const FromT>& _objectRef)
{
	static_assert(std::is_base_of_v<ToT, FromT> || std::is_base_of_v<FromT, ToT>, "Casted types should exist in the same hierarchy.");

	FromT*const objectPtr = _objectRef.get();
	if constexpr (std::is_base_of_v<ToT, FromT>)
	{
		return bwn::ObjectIdRef<ToT>(objectPtr);
	}
	else
	{
		ToT*const castedPtr = Object::cast_to<ToT>(objectPtr);
		if (castedPtr == nullptr)
		{
			// In case if casting was unsuccessful, we will not clear original reference.
			return nullptr;
		}

		return bwn::ObjectIdRef<ToT>(castedPtr);
	}
}