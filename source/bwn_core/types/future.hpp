#pragma once

#include "bwn_core/results/systemResult.hpp"

#include <core/error/error_list.h>

#include <memory>
#include <type_traits>
#include <optional>

namespace bwn
{
	namespace detail
	{
		template<typename ValueT>
		class FutureBase
		{
		protected:
			class Implementation;

			//
			// Public interface.
			//
		public:
			FutureBase();
			~FutureBase();

			//
			// Public interface.
			//
		public:
			// Inits future.
			void init();
			// Deinitializes future.
			void clear();
			// Aborts the operation which loads resource.
			void abort();
			// Sets current percent of resource loaded.
			void setLoadPercent(const uint32_t _percent);
			// Returns true if future is initialized.
			bool isValid() const;
			// Returns true if operation is concluded or aborted.
			bool isFinished() const;
			// Returns true if operation was aborted, and resource loading no longer needed.
			bool isAborted() const;
			// Returns true if future is finished and result resource is ready.
			bool isSucceeded() const;
			// Returns max number of stages (if not initialized, returns 0).
			uint32_t getLoadPercent() const;
			// Returns load error. If not initialized returns ERR_UNCONFIGURED, and if not finished return ERR_BUSY.
			SystemResult getResult() const;

			// Sets result of a shared state as error (if future is not initialized doesn't do anything).
			void setError(const SystemResult& _result);

			// Sets result if future is initialized. Sets the result via move constructor if we can.
			void setResult(ValueT&& _object);
			// Sets result if future is initialized.
			void setResult(const ValueT& _object);

			//
			// Private members.
			//
		protected:
			std::shared_ptr<Implementation> m_implementation;
		};

	} // namespace detail

	// Basically this enum defines how future should behave itself with the value.
	// Release < the future will only do the release of the value (and not copy).
	// Copy < the future will only copy the value (and not release).
	// This is mostly done due to conflicts which could be raised
	// then both release and copy of the value called in the same time.
	// Because of it, it's better to restrict the future to work only with one type of operation.
	enum class EFutureType
	{
		k_release,
		k_copy
	};

	// By default the type of the future will be determinant on whenever the value could be copied (aka, k_copy is preferred).
	// But it could be manually changed to k_release, if you want the future to release the value even in a case when it could be copied.
	template<typename ValueT, EFutureType k_type = std::is_copy_constructible_v<ValueT> ? EFutureType::k_copy : EFutureType::k_release>
	class Future;

	template<typename ValueT>
	class Future<ValueT, EFutureType::k_copy> : public detail::FutureBase<ValueT>
	{
		//
		// Construction and destruction.
		//
	public:
		Future();
		~Future();

		//
		// Public interface.
		//
	public:
		// Returns the value by copying it from result. Works only if the result is copy constructible.
		std::optional<ValueT> getValue() const;
	};

	template<typename ValueT>
	class Future<ValueT, EFutureType::k_release> : public detail::FutureBase<ValueT>
	{
		//
		// Construction and destruction.
		//
	public:
		Future();
		~Future();

		//
		// Public interface.
		//
	public:
		// Returns value from the future, and also clears that result (works for both move/copy assignable values).
		std::optional<ValueT> releaseValue() const;
		// Returns true if future have finished, but value have been released (aka now absent from the future).
		bool isReleased() const;
	};

} // namespace bwn

#include "bwn_core/types/future.inl"