#pragma once

#include "bwn_core/types/nodeProperty.hpp"

#if defined(DEBUG_ENABLED)
template<typename NodeT, typename ParentClassT, typename SignalBindsT>
bwn::NodeProperty<NodeT, ParentClassT, SignalBindsT>::NodeProperty(const char*const _variableHint)
	: m_ptr( nullptr )
	, m_path()
	, m_variableHint( _variableHint )
{}
#else
template<typename NodeT, typename ParentClassT, typename SignalBindsT>
bwn::NodeProperty<NodeT, ParentClassT, SignalBindsT>::NodeProperty()
	: m_ptr( nullptr )
	, m_path()
{}
#endif

template<typename NodeT, typename ParentClassT, typename SignalBindsT>
void bwn::NodeProperty<NodeT, ParentClassT, SignalBindsT>::setNode(ParentClassT& _this, NodeT*const _nodePtr)
{
	if (_nodePtr == nullptr)
	{
		m_path = NodePath();
	}
	else
	{
		m_path = _nodePtr->get_path();
	}

	rebindNodePtr(_this, m_ptr, _nodePtr);
	m_ptr = _nodePtr;
}

template<typename NodeT, typename ParentClassT, typename SignalBindsT>
void bwn::NodeProperty<NodeT, ParentClassT, SignalBindsT>::setNode(ParentClassT& _this, const NodePath& _path)
{
	m_path = _path;

	if (!_this.is_inside_tree())
	{
		clearPtr(_this);
	}
	else
	{
		updateNode(_this);
	}
}

template<typename NodeT, typename ParentClassT, typename SignalBindsT>
void bwn::NodeProperty<NodeT, ParentClassT, SignalBindsT>::onEnterTree(ParentClassT& _this)
{
	updateNode(_this);
}

template<typename NodeT, typename ParentClassT, typename SignalBindsT>
NodeT* bwn::NodeProperty<NodeT, ParentClassT, SignalBindsT>::getNode() const
{
	return m_ptr;
}

template<typename NodeT, typename ParentClassT, typename SignalBindsT>
NodeT* bwn::NodeProperty<NodeT, ParentClassT, SignalBindsT>::operator->() const
{
	BWN_ASSERT_ERROR_COND(m_ptr != nullptr, "Dereferensing NodeProperty of variable {} with uninitialized node.", m_variableHint);
	return m_ptr;
}

template<typename NodeT, typename ParentClassT, typename SignalBindsT>
const NodePath& bwn::NodeProperty<NodeT, ParentClassT, SignalBindsT>::getPath() const
{
	return m_path;
}

template<typename NodeT, typename ParentClassT, typename SignalBindsT>
bool bwn::NodeProperty<NodeT, ParentClassT, SignalBindsT>::isValid() const
{
	return m_ptr != nullptr;
}

template<typename NodeT, typename ParentClassT, typename SignalBindsT>
void bwn::NodeProperty<NodeT, ParentClassT, SignalBindsT>::clearPtr(ParentClassT& _this)
{
	if constexpr (!std::is_same_v<SignalBindsT, void>)
	{
		if (m_ptr != nullptr)
		{
			SignalBindsT::k_binds.disconnect(*m_ptr, _this);
		}
	}

	m_ptr = nullptr;
}

template<typename NodeT, typename ParentClassT, typename SignalBindsT>
void bwn::NodeProperty<NodeT, ParentClassT, SignalBindsT>::updateNode(ParentClassT& _this)
{
	if (m_path.is_empty())
	{
		clearPtr(_this);
		return;
	}

	Node*const node = _this.get_node(m_path);
	BWN_ASSERT_WARNING_COND(
		node != nullptr,
		"Can't find node from path '{}' to initialize variable {} of type {}.",
		m_path,
		m_variableHint,
		NodeT::get_class_static());

	// We don't really need to cast node inside 'node != nullptr' guard. cast_to will do this basic check anyway.
	NodeT*const castedNode = Object::cast_to<NodeT>(node);

#if defined(DEBUG_ENABLED)
	if (node != nullptr)
	{
		BWN_ASSERT_WARNING_COND(
			castedNode != nullptr,
			"Can't cast node '{}' to variable {} of type {}. Actual type of node is {}.",
			m_path,
			m_variableHint,
			NodeT::get_class_static(),
			node->get_class());
	}
#endif

	// Even if castedNode is nullptr, we still want to initialize m_ptr.
	// If node didn't exist we still want to initialize m_ptr with nullptr.
	rebindNodePtr(_this, m_ptr, castedNode);
	m_ptr = castedNode;
}

template<typename NodeT, typename ParentClassT, typename SignalBindsT>
void bwn::NodeProperty<NodeT, ParentClassT, SignalBindsT>::rebindNodePtr(ParentClassT& _this, NodeT*const _oldPtr, NodeT*const _newPtr) const
{
	if constexpr (!std::is_same_v<SignalBindsT, void>)
	{
		if (_newPtr == _oldPtr)
		{
			return;
		}

		if (_oldPtr != nullptr)
		{
			SignalBindsT::k_binds.disconnect(*_oldPtr, _this);
		}

		if (_newPtr != nullptr)
		{
			SignalBindsT::k_binds.connect(*_newPtr, _this);
		}
	}
}