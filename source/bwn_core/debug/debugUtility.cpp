#include "precompiled_core.hpp"

#include "bwn_core/debug/debugUtility.hpp"

const char* bwn::extractFileName(const char* _path)
{
	if (_path == nullptr) {
		return nullptr;
	}

	const char* ret = _path;
	do
	{
		if (*_path == '\\' || *_path == '/') {
			ret = _path + 1;
		}
	}
	while(*_path++);

	return ret;
}

String bwn::debug::getNodePath(const Node*const _node)
{
	if (_node == nullptr)
	{
		return "<nullptr>";
	}

	if (!_node->is_inside_tree())
	{
		return _node->get_name();
	}

	return _node->get_path();
}

String bwn::debug::getNodeName(const Node*const _node)
{
	return _node == nullptr
		? "<nullptr>"
		: _node->get_name().operator String();
}