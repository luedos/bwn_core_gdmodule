#pragma once 

namespace bwn
{
	// This is just a quick function which will return pointer for the next symbol after '/' or '\\' in the string.
	// If no such symbols were found, it will return begging of the string.
	const char* extractFileName(const char* _path);
} // namespace bwn

// Intended to use in debug
#if DEBUG_ENABLED
	#define BWN_ONLY_IN_DEBUG(...) __VA_ARGS__
#else
	#define BWN_ONLY_IN_DEBUG(...)
#endif

class Node;

namespace bwn
{
	namespace debug
	{
		template<typename TypeT>
		String getClassName(const TypeT& _obj)
		{
			return _obj.get_class();
		}

		template<typename TypeT>
		String getClassName(Ref<TypeT> _ref)
		{
			return _ref.is_valid()
				? _ref->get_class()
				: "NULL";
		}

		template<typename TypeT>
		String getClassName(TypeT* _ptr)
		{
			return _ptr != nullptr
				? _ptr->get_class()
				: "NULL";
		}

		String getNodePath(const Node*const _node);
		String getNodeName(const Node*const _node);
	} // namespace debug
} // namespace bwn