#pragma once

#if defined(DEBUG_ENABLED)

	#include "bwn_core/types/singleton.hpp"

	#include <core/error/error_macros.h>

	#include <mutex>
	#include <unordered_map>

namespace bwn
{
	struct SourcePoint;

	class AssertManager : public ManualSingleton<AssertManager>
	{
		//
		// Private classes.
		//
	private:
		// Assert statistics.
		struct AssertStat
		{
			// How much assert was called..
			uint32_t count = 0;
			// Is assert should be triggered or no. (generally defined by assert message, user will have ability to turn off same assert at runtime).
			bool enabled = true;
		};
		// Return codes from assert popup.
		enum EPopupResult
		{
			//...
			k_nothing   = 0,
			// Should the program halt or at least break?
			k_break     = 1 << 1,
			// Should this assert be disabled?
			k_disable   = 1 << 2,
			
		};

		//
		// Construction and destruction.
		//
	public:
		AssertManager();
		~AssertManager();

		//
		// Public interface.
		//
	public:
		// Triggers assert with general level. Returns true if program should be paused.
		// This method is mutex protected.
		bool triggerAssert(
			ErrorHandlerType _type,
			const char* _conditionStr, 
			const char* _msgStr, 
			const SourcePoint& _sourcePoint);

		// Sets minimal required level of asserts which can be triggered.
		// If assert is lower level, it will not be triggered (log message still will be produced).
		// This method is mutex protected.
		void switchErrorHandler(ErrorHandlerType _type, bool _switch);

		//
		// Private methods.
		//
	private:
		// Returns assert stat for specific source point. If this source point haven't been triggered yet, stat will be created.
		AssertStat& getAssertStat(const SourcePoint& _sourcePoint);
		// Just transfers an error level for the more readable form.
		static const char* formatErrorType(ErrorHandlerType _type);
		// Shows assert message, and returns respected code from user.
		EPopupResult showAssertPopup(
			ErrorHandlerType _type, 
			const char* _conditionStr, 
			const char* _msgStr, 
			const SourcePoint& _sourcePoint);

		// This method is mutex protected.
		void enableAssertGroup(const String& _group);
		// This method is mutex protected.
		void disableAssertGroup(const String& _group);

		//
		// Private members.
		//
	private:
		// Please don't scream at me for this hardcode..
		static constexpr size_t k_godotErrorHandlerCount = 4;

		// Statistics for all triggered asserts.
		std::unordered_map<SourcePoint, AssertStat> m_assertStats;
		// Error handler, which will get signals from godot errors.
		ErrorHandlerList m_errorHandler;
		// Minimal required assert level.
		bool m_assertFlags[k_godotErrorHandlerCount];
		// This mutex exist just to allow throwing errors from different threads.
		std::mutex m_mutex;
	};

} // namespace bwn

#endif // DEBUG_ENABLED