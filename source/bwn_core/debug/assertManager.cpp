#include "precompiled_core.hpp"

#if defined(DEBUG_ENABLED)

	#include "bwn_core/debug/assertManager.hpp"
	#include "bwn_core/utility/engineUtility.hpp"

	#include <core/os/os.h>
	#include <core/io/logger.h>

	#if defined(WINDOWS_ENABLED)
		#include <Windows.h>
		#include <WinUser.h>
	#elif defined(X11_ENABLED) && !defined(SOWRAP_ENABLED)
		#include "messagebox_x11.h"
	#endif

	#include <fmt/xchar.h>
namespace
{
	// Function for godot error handling.
	void handleError(
		void* _hadnlerData, 
		const char* _function, 
		const char* _file, 
		int _line, 
		const char* _error, 
		const char* _message,
		const bool _editor_notify,
		ErrorHandlerType _type)
	{
		bwn::SourcePoint point;
		point.file = _file;
		point.line = _line;
		point.func = _function;

		bwn::AssertManager*const thisptr = static_cast<bwn::AssertManager*>(_hadnlerData);

		if (thisptr->triggerAssert(_type, _error, _message, point))
		{
			BWN_BREAK();
		}
	}

} // namespace

bwn::AssertManager::AssertManager()
	: m_assertStats()
{
	// This is basically to mitigate the fact we hardcoded k_godotErrorHandlerCount
	static_assert(
		ERR_HANDLER_ERROR == (k_godotErrorHandlerCount - 1)
		|| ERR_HANDLER_WARNING  == (k_godotErrorHandlerCount - 1)
		|| ERR_HANDLER_SCRIPT  == (k_godotErrorHandlerCount - 1)
		|| ERR_HANDLER_SHADER  == (k_godotErrorHandlerCount - 1),
		"At least one of the error handler must be the last index."
	);
	static_assert(
		ERR_HANDLER_ERROR < k_godotErrorHandlerCount
		&& ERR_HANDLER_WARNING < k_godotErrorHandlerCount
		&& ERR_HANDLER_SCRIPT < k_godotErrorHandlerCount
		&& ERR_HANDLER_SHADER < k_godotErrorHandlerCount,
		"Any of the error handlers should be less then the max count."
	);
	static_assert(
		(ERR_HANDLER_ERROR + ERR_HANDLER_WARNING + ERR_HANDLER_SCRIPT + ERR_HANDLER_SHADER) == (k_godotErrorHandlerCount * (k_godotErrorHandlerCount - 1)) / 2,
		"And none of them should be equal to any other of them."
	);

	// the default arguments.
	// Do not assert errors in editor by default
	m_assertFlags[ERR_HANDLER_ERROR] = !bwn::isEditor();
	m_assertFlags[ERR_HANDLER_WARNING] = false;
	m_assertFlags[ERR_HANDLER_SCRIPT] = false;
	m_assertFlags[ERR_HANDLER_SHADER] = false;

	m_errorHandler.errfunc = &::handleError;
	m_errorHandler.userdata = this;

	add_error_handler(&m_errorHandler);
}

bwn::AssertManager::~AssertManager()
{
	remove_error_handler(&m_errorHandler);
}

void bwn::AssertManager::switchErrorHandler(ErrorHandlerType _type, bool _switch)
{
	BWN_TRAP_COND(uint32_t(_type) < k_godotErrorHandlerCount, "Type of the handler error is incorrect.");
	
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		m_assertFlags[_type] = _switch;
	}
}

bool bwn::AssertManager::triggerAssert(
	ErrorHandlerType _type,
	const char* _conditionStr, 
	const char* _msgStr, 
	const SourcePoint& _sourcePoint)
{
	std::unique_lock<std::mutex> lock(m_mutex);

	AssertStat& stats = getAssertStat(_sourcePoint);
	++stats.count;
	
	// If _type is incorrect, we still want to show assert.
	if ((size_t(_type) < k_godotErrorHandlerCount && !m_assertFlags[_type]) || !stats.enabled)
	{
		return false;
	}
	
	const EPopupResult result = showAssertPopup(
		_type, 
		_conditionStr, 
		_msgStr, 
		_sourcePoint);

	if (result & EPopupResult::k_disable) {
		stats.enabled = false;
	}

	return result & EPopupResult::k_break;
}

const char* bwn::AssertManager::formatErrorType(ErrorHandlerType _type)
{
	switch(_type)
	{
		case ErrorHandlerType::ERR_HANDLER_ERROR: return "ERROR";
		case ErrorHandlerType::ERR_HANDLER_WARNING: return "WARNING";
		case ErrorHandlerType::ERR_HANDLER_SCRIPT: return "ERR_SCRIPT";
		case ErrorHandlerType::ERR_HANDLER_SHADER: return "ERR_SHADER";
		default: return "ERR_UNKNOWN";
	}
}

#if defined(WINDOWS_ENABLED)

bwn::AssertManager::EPopupResult DebugManager::showAssertPopup(
	ErrorHandlerType _type, 
	const char* _conditionStr, 
	const char* _msgStr, 
	const SourcePoint& _sourcePoint)
{
	const std::string caption = fmt::format(
		"[BWN][{}] {}:{}", 
		formatErrorType(_type), 
		extractFileName(_sourcePoint.file), 
		_sourcePoint.line);
	
	const std::string message = fmt::format(
		"In: {}\n"
		"Error: {}\n"
		"Message: {}\n"
		"----\n"
		"Break execution: \'Abort\'\n"
		"Don't show again: \'Ignore\'\n"
		"Ignore and continue: \'Retry\'\n",
		_sourcePoint.func,
		_conditionStr,
		_msgStr
	);

	const uint32_t boxIcon = (_level == EErrorLevel::k_warning) * MB_ICONWARNING
		+ (_level == EErrorLevel::k_error || _level == EErrorLevel::k_fatal) * MB_ICONERROR;

	const int result = MessageBox(
		nullptr, 
		message.c_str(), 
		caption.c_str(), 
		boxIcon | MB_ABORTRETRYIGNORE | MB_DEFBUTTON3 | MB_TASKMODAL | MB_SETFOREGROUND);

	return EPopupResult(EPopupResult::k_nothing
		| ((result == IDABORT) * EPopupResult::k_break)
		| ((result == IDIGNORE) * EPopupResult::k_disable));
}

#elif defined(X11_ENABLED) && !defined(SOWRAP_ENABLED)

bwn::AssertManager::EPopupResult bwn::AssertManager::showAssertPopup(
	ErrorHandlerType _type, 
	const char* _conditionStr, 
	const char* _msgStr, 
	const SourcePoint& _sourcePoint)
{
	const std::string caption = fmt::format(
		"[BWN][{}] {}:{}", 
		formatErrorType(_type), 
		extractFileName(_sourcePoint.file), 
		_sourcePoint.line);
	
	const std::wstring message = fmt::format(
		L"In: {}\n"
		L"Error: {}\n"
		L"Message: {}\n"
		L"----\n"
		L"Break execution: \'Abort\'\n"
		L"Don't show again: \'Ignore\'\n"
		L"Ignore and continue: \'Continue\'\n",
		bwn::wrap_utf(_sourcePoint.func),
		bwn::wrap_utf(_conditionStr),
		bwn::wrap_utf(_msgStr)
	);

	Button buttons[3];
    buttons[0].label = L"Abort";
    buttons[1].label = L"Ignore";
    buttons[2].label = L"Continue";

    buttons[0].result = 1;
    buttons[1].result = 2;
    buttons[2].result = 3;

	const int result = Messagebox(caption.c_str(), message.c_str(), buttons, sizeof(buttons) / sizeof(buttons[0]));

	return EPopupResult(EPopupResult::k_nothing
		| ((result == 1) * EPopupResult::k_break)
		| ((result == 2) * EPopupResult::k_disable));
}

#else

bwn::AssertManager::EPopupResult bwn::AssertManager::showAssertPopup(
	ErrorHandlerType, 
	const char*, 
	const char*, 
	const SourcePoint&)
{
	return EPopupResult::k_nothing;
}

#endif

bwn::AssertManager::AssertStat& bwn::AssertManager::getAssertStat(const SourcePoint& _sourcePoint)
{
	// if stat does not exist, it will be created any way.
	return m_assertStats[_sourcePoint];
}

void bwn::AssertManager::enableAssertGroup(const String& _group)
{
	if (_group == "ERROR")
	{
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			m_assertFlags[ERR_HANDLER_ERROR] = true;
		}

		print_line("ERROR assert group enabled.");
	}
	else if (_group == "WARNING")
	{
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			m_assertFlags[ERR_HANDLER_WARNING] = true;
		}
		
		print_line("WARNING assert group enabled.");
	}
	else if (_group == "SCRIPT")
	{
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			m_assertFlags[ERR_HANDLER_SCRIPT] = true;
		}

		print_line("SCRIPT assert group enabled.");
	}
	else if (_group == "SHADER")
	{
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			m_assertFlags[ERR_HANDLER_SHADER] = true;
		}

		print_line("SHADER assert group enabled.");
	}
	else
	{
		print_error(bwn::format("Unknown assert group format '{}'.", _group));
	}
}
void bwn::AssertManager::disableAssertGroup(const String& _group)
{
	if (_group == "ERROR")
	{
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			m_assertFlags[ERR_HANDLER_ERROR] = false;
		}

		print_line("ERROR assert group disabled.");
	}
	else if (_group == "WARNING")
	{
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			m_assertFlags[ERR_HANDLER_WARNING] = false;
		}

		print_line("WARNING assert group disabled.");
	}
	else if (_group == "SCRIPT")
	{
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			m_assertFlags[ERR_HANDLER_SCRIPT] = false;
		}

		print_line("SCRIPT assert group disabled.");
	}
	else if (_group == "SHADER")
	{
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			m_assertFlags[ERR_HANDLER_SHADER] = false;
		}
		
		print_line("SHADER assert group disabled.");
	}
	else
	{
		print_error(bwn::format("Unknown assert group format '{}'.", _group));
	}
}

#endif // DEBUG_ENABLED