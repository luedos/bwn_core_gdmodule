#pragma once

#include "bwn_core/utility/formatUtility.hpp"
#include "bwn_core/types/sourcePoint.hpp"

#include <core/os/os.h>

#if defined(WINDOWS_ENABLED)

	#if defined(DEBUG_ENABLED)
		#define BWN_BREAK() __debugbreak()
	#else // DEBUG_ENABLED
		#define BWN_BREAK() std::abort()
	#endif // !DEBUG_ENABLED

#elif defined(UNIX_ENABLED)

	#if defined(DEBUG_ENABLED)
		#include <signal.h>
		#define BWN_BREAK() ::raise(SIGINT)
	#else // DEBUG_ENABLED
		#define BWN_BREAK() std::abort()
	#endif // !DEBUG_ENABLED

#else // no system macro defined

	#error "Incorrect system defined."

#endif


//
// Logging
//
namespace bwn
{
	namespace detail
	{
	#if defined(DEBUG_ENABLED)
		void log(const char*const _msg, const SourcePoint& _point);
		void log(const char32_t*const _msg, const SourcePoint& _point);

		#define BWN_LOG(...) ::bwn::detail::log(::fmt::format(__VA_ARGS__).c_str(), BWN_SOURCE_POINT())
		#define BWN_DEBUG_LOG(...) ::bwn::detail::log(::fmt::format(__VA_ARGS__).c_str(), BWN_SOURCE_POINT())
		#define BWN_VERBOSE_LOG(...) \
			do { \
				if (::OS::get_singleton()->is_stdout_verbose()) { \
					::bwn::detail::log(::fmt::format(__VA_ARGS__).c_str(), BWN_SOURCE_POINT()); \
				} \
			} while(false)
		#define BWN_DEBUG_VERBOSE_LOG(...) BWN_VERBOSE_LOG(__VA_ARGS__)

	#else // DEBUG_ENABLED

		void log(const char*const _msg);
		void log(const char32_t*const _msg);

		#define BWN_LOG(...) ::bwn::detail::log(::fmt::format(__VA_ARGS__).c_str())
		#define BWN_DEBUG_LOG(...) (void)(0)
		#define BWN_VERBOSE_LOG(...) \
			do { \
				if (::OS::get_singleton()->is_stdout_verbose()) { \
					::bwn::detail::log(fmt::format(__VA_ARGS__).c_str()); \
				} \
			} while(false)
		#define BWN_DEBUG_VERBOSE_LOG(...) (void)(0)

	#endif // !DEBUG_ENABLED
	} // namespace detail
} // namespace bwn


//
// Asserting
//
#if defined(DEBUG_ENABLED)

namespace bwn
{
	namespace debug
	{
		bool setTrapEnabled(const bool _enabled);
		bool getTrapEnabled();

		void triggerWarning(const char*const _cond, const char*const _msg, const SourcePoint& _point);
		void triggerError(const char*const _cond, const char*const _msg, const SourcePoint& _point);
		void triggerTrap(const char*const _cond, const char*const _msg, const SourcePoint& _point);

	} // namespace debug
} // namespace bwn

	#define BWN_ASSERT_WARNING(...) ::bwn::debug::triggerWarning("", ::fmt::format(__VA_ARGS__).c_str(), BWN_SOURCE_POINT())
	#define BWN_ASSERT_WARNING_COND(_cond, ...) \
		do { \
			if (unlikely(!(_cond))) { \
				::bwn::debug::triggerWarning(#_cond, ::fmt::format(__VA_ARGS__).c_str(), BWN_SOURCE_POINT()); \
			} \
		} while(false)

	#define BWN_ASSERT_ERROR(...) ::bwn::debug::triggerError("", ::fmt::format(__VA_ARGS__).c_str(), BWN_SOURCE_POINT())
	#define BWN_ASSERT_ERROR_COND(_cond, ...) \
		do { \
			if (unlikely(!(_cond))) { \
				::bwn::debug::triggerError(#_cond, ::fmt::format(__VA_ARGS__).c_str(), BWN_SOURCE_POINT()); \
			} \
		} while(false)

	#define BWN_TRAP(...) ::bwn::debug::triggerTrap("", ::fmt::format(__VA_ARGS__).c_str(), BWN_SOURCE_POINT())
	#define BWN_TRAP_COND(_cond, ...) \
		do { \
			if (unlikely(!(_cond))) { \
				::bwn::debug::triggerTrap(#_cond, ::fmt::format(__VA_ARGS__).c_str(), BWN_SOURCE_POINT()); \
			} \
		} while(false)

#else // DEBUG_ENABLED

	#define BWN_ASSERT_WARNING(...) (void)(0)
	#define BWN_ASSERT_WARNING_COND(_cond, ...) (void)(0)
	#define BWN_ASSERT_ERROR(...) (void)(0)
	#define BWN_ASSERT_ERROR_COND(_cond, ...) (void)(0)
	#define BWN_TRAP(...) (void)(0)
	#define BWN_TRAP_COND(_cond, ...) (void)(0)

#endif // !DEBUG_ENABLED
