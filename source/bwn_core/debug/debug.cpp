#include "precompiled_core.hpp"

#include "bwn_core/debug/debug.hpp"
#include "bwn_core/debug/assertManager.hpp"

//
// Logging
//
#if defined(DEBUG_ENABLED)
void bwn::detail::log(const char*const _msg, const SourcePoint& _point)
{
	print_line(bwn::format(
		"[BWN][LOG][{}:{}][{}] {}", 
		bwn::extractFileName(_point.file),
		_point.line,
		_point.func,
		_msg));
}

void bwn::detail::log(const char32_t*const _msg, const SourcePoint& _point)
{
	print_line(bwn::format(
		U"[BWN][LOG][{}:{}][{}] {}",
		bwn::wrap_utf(bwn::extractFileName(_point.file)),
		_point.line,
		bwn::wrap_utf(_point.func),
		_msg));
}
#else

void bwn::detail::log(const char* _msg)
{
	print_line(_msg);
}

void bwn::detail::log(const wchar_t* _msg)
{
	print_line(_msg);
}

#endif

//
// Asserting
//

#if defined(DEBUG_ENABLED)

#include <core/error/error_macros.h>

namespace bwn
{
	namespace debug
	{
		std::atomic_bool& getTrapValue()
		{
			static std::atomic_bool s_instance = true;
			return s_instance;
		}	
	} // namespace debug
} // namespace bwn

bool bwn::debug::setTrapEnabled(bool _enabled)
{
	std::atomic_bool& trapValue = getTrapValue();
	const bool startValue = trapValue;

	bool oldValue = startValue;
	while (!trapValue.compare_exchange_weak(oldValue, _enabled, std::memory_order_release))
	{}

	return startValue;
}

bool bwn::debug::getTrapEnabled()
{
	return bwn::debug::getTrapValue();
}

void bwn::debug::triggerWarning(const char*const _cond, const char*const _msg, const SourcePoint& _point)
{
	_err_print_error(
		_point.func,
		_point.file,
		_point.line,
		_cond,
		_msg,
		false,
		ErrorHandlerType::ERR_HANDLER_WARNING);
}

void bwn::debug::triggerError(const char*const _cond, const char*const _msg, const SourcePoint& _point)
{
	_err_print_error(
		_point.func,
		_point.file,
		_point.line,
		_cond,
		_msg,
		false,
		ErrorHandlerType::ERR_HANDLER_ERROR);
}

void bwn::debug::triggerTrap(const char*const _cond, const char*const _msg, const SourcePoint& _point)
{
	bwn::debug::triggerError(_cond, _msg, _point);

	if (bwn::debug::getTrapEnabled())
	{
		int*volatile null = nullptr;
		*null = 42;
	}
}

#endif // DEBUG_ENABLED