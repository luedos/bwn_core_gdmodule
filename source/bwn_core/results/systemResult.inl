#pragma once

#include "bwn_core/results/systemResult.hpp"
#include "bwn_core/utility/formatUtility.hpp"

_FORCE_INLINE_ bool bwn::SystemResult::success() const
{
	return m_godotErrorCode == Error::OK;
}

_FORCE_INLINE_ const String& bwn::SystemResult::getErrorDescription() const
{
	return m_errorDescription;
}

_FORCE_INLINE_ Error bwn::SystemResult::getError() const
{
	return static_cast<Error>(m_godotErrorCode);
}

_FORCE_INLINE_ bwn::SystemResult::NativeError bwn::SystemResult::getNativeError() const
{
	return m_nativeError;
}

template<>
struct fmt::formatter<bwn::SystemResult, char> 
{
	constexpr auto parse (basic_format_parse_context<char>& _ctx) 
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const bwn::SystemResult& _result, FormatContextT& _ctx)
	{
		if (_result.success())
		{
			return fmt::detail::write(_ctx.out(), fmt::basic_string_view<char>("Success"));
		}
		else
		{
			return fmt::format_to(_ctx.out(), "Failed (error code: {}, description: '{}')", _result.stringFromError(), _result.getErrorDescription());
		}
	}
};

template<>
struct fmt::formatter<bwn::SystemResult, char32_t> 
{
	constexpr auto parse (basic_format_parse_context<char32_t>& _ctx) 
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const bwn::SystemResult& _result, FormatContextT& _ctx)
	{
		if (_result.success())
		{
			return fmt::detail::write(_ctx.out(), fmt::basic_string_view<char32_t>(U"Success"));
		}
		else
		{
			return fmt::format_to(_ctx.out(), U"Failed (error code: {}, description: '{}')",
				bwn::wrap_utf(_result.stringFromError()), _result.getErrorDescription());
		}
	}
};