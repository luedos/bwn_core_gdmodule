#include "precompiled_core.hpp"

#include "bwn_core/results/systemFacility.hpp"
#include "bwn_core/utility/referenceUtility.hpp"

bwn::SystemFacility::SystemFacility()
	: BaseFacility( 0 ) // The default facility code is 0
{}
bwn::SystemFacility::~SystemFacility() = default;

void bwn::SystemFacility::_bind_methods()
{
	ClassDB::bind_static_method("SystemFacility", D_METHOD("create_success"), &SystemFacility::createSuccessObject);
	ClassDB::bind_static_method("SystemFacility", D_METHOD("create_error", "error_code", "error_description"), &SystemFacility::createErrorObject);
}

bwn::SystemFacility::NativeError bwn::SystemFacility::getSuccessCodeV()
{
	return Error::OK;
} 

Error bwn::SystemFacility::castNativeErrorV(NativeError _nativeError) const
{
	return static_cast<Error>(_nativeError);
}

const char* bwn::SystemFacility::stringFromErrorV(NativeError _nativeError) const
{
	return stringFromError(castNativeErrorV(_nativeError));
}

const char* bwn::SystemFacility::stringFromError(const Error _error)
{
	switch(_error)
	{
		case Error::OK: return "OK";
		case Error::FAILED: return "FAILED";
		case Error::ERR_UNAVAILABLE: return "ERR_UNAVAILABLE";
		case Error::ERR_UNCONFIGURED: return "ERR_UNCONFIGURED";
		case Error::ERR_UNAUTHORIZED: return "ERR_UNAUTHORIZED";
		case Error::ERR_PARAMETER_RANGE_ERROR: return "ERR_PARAMETER_RANGE_ERROR";
		case Error::ERR_OUT_OF_MEMORY: return "ERR_OUT_OF_MEMORY";
		case Error::ERR_FILE_NOT_FOUND: return "ERR_FILE_NOT_FOUND";
		case Error::ERR_FILE_BAD_DRIVE: return "ERR_FILE_BAD_DRIVE";
		case Error::ERR_FILE_BAD_PATH: return "ERR_FILE_BAD_PATH";
		case Error::ERR_FILE_NO_PERMISSION: return "ERR_FILE_NO_PERMISSION";
		case Error::ERR_FILE_ALREADY_IN_USE: return "ERR_FILE_ALREADY_IN_USE";
		case Error::ERR_FILE_CANT_OPEN: return "ERR_FILE_CANT_OPEN";
		case Error::ERR_FILE_CANT_WRITE: return "ERR_FILE_CANT_WRITE";
		case Error::ERR_FILE_CANT_READ: return "ERR_FILE_CANT_READ";
		case Error::ERR_FILE_UNRECOGNIZED: return "ERR_FILE_UNRECOGNIZED";
		case Error::ERR_FILE_CORRUPT: return "ERR_FILE_CORRUPT";
		case Error::ERR_FILE_MISSING_DEPENDENCIES: return "ERR_FILE_MISSING_DEPENDENCIES";
		case Error::ERR_FILE_EOF: return "ERR_FILE_EOF";
		case Error::ERR_CANT_OPEN: return "ERR_CANT_OPEN";
		case Error::ERR_CANT_CREATE: return "ERR_CANT_CREATE";
		case Error::ERR_QUERY_FAILED: return "ERR_QUERY_FAILED";
		case Error::ERR_ALREADY_IN_USE: return "ERR_ALREADY_IN_USE";
		case Error::ERR_LOCKED: return "ERR_LOCKED";
		case Error::ERR_TIMEOUT: return "ERR_TIMEOUT";
		case Error::ERR_CANT_CONNECT: return "ERR_CANT_CONNECT";
		case Error::ERR_CANT_RESOLVE: return "ERR_CANT_RESOLVE";
		case Error::ERR_CONNECTION_ERROR: return "ERR_CONNECTION_ERROR";
		case Error::ERR_CANT_ACQUIRE_RESOURCE: return "ERR_CANT_ACQUIRE_RESOURCE";
		case Error::ERR_CANT_FORK: return "ERR_CANT_FORK";
		case Error::ERR_INVALID_DATA: return "ERR_INVALID_DATA";
		case Error::ERR_INVALID_PARAMETER: return "ERR_INVALID_PARAMETER";
		case Error::ERR_ALREADY_EXISTS: return "ERR_ALREADY_EXISTS";
		case Error::ERR_DOES_NOT_EXIST: return "ERR_DOES_NOT_EXIST";
		case Error::ERR_DATABASE_CANT_READ: return "ERR_DATABASE_CANT_READ";
		case Error::ERR_DATABASE_CANT_WRITE: return "ERR_DATABASE_CANT_WRITE";
		case Error::ERR_COMPILATION_FAILED: return "ERR_COMPILATION_FAILED";
		case Error::ERR_METHOD_NOT_FOUND: return "ERR_METHOD_NOT_FOUND";
		case Error::ERR_LINK_FAILED: return "ERR_LINK_FAILED";
		case Error::ERR_SCRIPT_FAILED: return "ERR_SCRIPT_FAILED";
		case Error::ERR_CYCLIC_LINK: return "ERR_CYCLIC_LINK";
		case Error::ERR_INVALID_DECLARATION: return "ERR_INVALID_DECLARATION";
		case Error::ERR_DUPLICATE_SYMBOL: return "ERR_DUPLICATE_SYMBOL";
		case Error::ERR_PARSE_ERROR: return "ERR_PARSE_ERROR";
		case Error::ERR_BUSY: return "ERR_BUSY";
		case Error::ERR_SKIP: return "ERR_SKIP";
		case Error::ERR_HELP: return "ERR_HELP";
		case Error::ERR_BUG: return "ERR_BUG";
		case Error::ERR_PRINTER_ON_FIRE: return "ERR_PRINTER_ON_FIRE";

		case Error::ERR_MAX: [[ fallthrough ]];
		default: return "UNKNOWN_GODOT_ERR";
	};
}

bwn::SystemResult bwn::SystemFacility::createSuccess()
{
	return getInstance()->createResult(Error::OK, static_cast<NativeError>(Error::OK), String());
}

bwn::SystemResult bwn::SystemFacility::createError(const Error _resultCode, const String& _description)
{
	return getInstance()->createResult(_resultCode, static_cast<NativeError>(_resultCode), _description);
}

Ref<bwn::SystemResultObject> bwn::SystemFacility::createSuccessObject()
{
	return bwn::instantiateRef<SystemResultObject>(createSuccess());
}

Ref<bwn::SystemResultObject> bwn::SystemFacility::createErrorObject(const Error _resultCode, const String& _description)
{
	return bwn::instantiateRef<SystemResultObject>(createError(_resultCode, _description));
}
