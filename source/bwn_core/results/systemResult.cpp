#include "precompiled_core.hpp"

#include "bwn_core/results/systemResult.hpp"
#include "bwn_core/results/baseFacility.hpp"
#include "bwn_core/results/systemFacility.hpp"
#include "bwn_core/utility/objectUtility.hpp"

bwn::SystemResult::SystemResult(BaseFacility& _facility)
	: m_errorDescription()
	, m_nativeError( _facility.getSuccessCodeV() )
	, m_godotErrorCode( Error::OK )
	, m_facilityId( _facility.getId() )
{
#if defined(DEBUG_ENABLED)
	_facility.referencerResult(*this);
#endif
}

bwn::SystemResult::SystemResult(NativeError _nativeError, String _errorDesc, BaseFacility& _facility)
	: m_errorDescription( _errorDesc )
	, m_nativeError( _nativeError )
	, m_godotErrorCode( _facility.castNativeErrorV(_nativeError) )
	, m_facilityId( _facility.getId() )
{
#if defined(DEBUG_ENABLED)
	_facility.referencerResult(*this);
#endif
}

bwn::SystemResult::SystemResult(Error _godotCode, NativeError _nativeError, BaseFacility& _facility)
	: m_errorDescription()
	, m_nativeError( _nativeError )
	, m_godotErrorCode( _godotCode )
	, m_facilityId( _facility.getId() )
{
#if defined(DEBUG_ENABLED)
	_facility.referencerResult(*this);
#endif
}

bwn::SystemResult::SystemResult(Error _godotCode, NativeError _nativeError, String _errorDesc, BaseFacility& _facility)
	: m_errorDescription( _errorDesc )
	, m_nativeError( _nativeError )
	, m_godotErrorCode( _godotCode )
	, m_facilityId( _facility.getId() )
{
#if defined(DEBUG_ENABLED)
	_facility.referencerResult(*this);
#endif
}

#if defined(DEBUG_ENABLED)
bwn::SystemResult::SystemResult(const SystemResult& _other)
{
	m_errorDescription = _other.m_errorDescription;
	m_nativeError = _other.m_nativeError;
	m_godotErrorCode = _other.m_godotErrorCode;
	m_facilityId = _other.m_facilityId;

#if defined(DEBUG_ENABLED)
	BaseFacility*const facility = BaseFacility::findFacility(m_facilityId);
	if (facility != nullptr)
	{
		facility->referencerResult(*this);
	}
#endif
}
bwn::SystemResult& bwn::SystemResult::operator=(const SystemResult& _other)
{
	if (&_other == this)
	{
		return *this;
	}

#if defined(DEBUG_ENABLED)
	BaseFacility*const oldFacility = BaseFacility::findFacility(m_facilityId);
	if (oldFacility != nullptr)
	{
		oldFacility->unreferencerResult(*this);
	}
#endif

	m_errorDescription = _other.m_errorDescription;
	m_nativeError = _other.m_nativeError;
	m_godotErrorCode = _other.m_godotErrorCode;
	m_facilityId = _other.m_facilityId;

#if defined(DEBUG_ENABLED)
	BaseFacility*const newFacility = BaseFacility::findFacility(m_facilityId);
	if (newFacility != nullptr)
	{
		newFacility->referencerResult(*this);
	}
#endif

	return *this;
}
bwn::SystemResult::~SystemResult()
{

#if defined(DEBUG_ENABLED)
	BaseFacility*const facility = BaseFacility::findFacility(m_facilityId);
	if (facility != nullptr)
	{
		facility->unreferencerResult(*this);
	}
#endif
}
#else
bwn::SystemResult::SystemResult(const SystemResult& _other) = default;
bwn::SystemResult& bwn::SystemResult::operator=(const SystemResult& _other) = default;
bwn::SystemResult::~SystemResult() = default;
#endif

const bwn::BaseFacility* bwn::SystemResult::getFacility() const
{
	return BaseFacility::findFacility(m_facilityId);
}

const char* bwn::SystemResult::stringFromError() const
{
	const bwn::BaseFacility*const facility = getFacility();
	if (facility != nullptr)
	{
		return facility->stringFromErrorV(m_nativeError);
	}

	return "<invalid system result>";
}

bwn::SystemResultObject::SystemResultObject()
	: m_errorDescription()
	, m_nativeError( 0 )
	, m_godotErrorCode( Error::FAILED )
	, m_facilityId( BaseFacility::k_invalidFacilityId )
{}

bwn::SystemResultObject::SystemResultObject(const SystemResult& _result)
	: m_errorDescription( _result.getErrorDescription() )
	, m_nativeError( _result.getNativeError() )
	, m_godotErrorCode( _result.getError() )
	, m_facilityId( _result.getFacility()->getId() )
{}

void bwn::SystemResultObject::_bind_methods()
{
	bwn::bindMethod(D_METHOD("success"), &SystemResultObject::success);
	bwn::bindMethod(D_METHOD("get_error_description"), &SystemResultObject::getErrorDescription);
	bwn::bindMethod(D_METHOD("get_error"), &SystemResultObject::getError);
	bwn::bindMethod(D_METHOD("get_native_error"), &SystemResultObject::getNativeError);
	bwn::bindMethod(D_METHOD("string_from_error"), &SystemResultObject::stringFromError);
}

bool bwn::SystemResultObject::success() const
{
	return m_godotErrorCode == Error::OK;
}

const String& bwn::SystemResultObject::getErrorDescription() const
{
	return m_errorDescription;
}

Error bwn::SystemResultObject::getError() const
{
	return static_cast<Error>(m_godotErrorCode);
}

bwn::SystemResult::NativeError bwn::SystemResultObject::getNativeError() const
{
	return m_nativeError;
}

String bwn::SystemResultObject::stringFromError() const
{
	const bwn::BaseFacility*const facility = BaseFacility::findFacility(m_facilityId);
	if (facility != nullptr)
	{
		return facility->stringFromErrorV(m_nativeError);
	}

	return "<invalid system result>";
}
