#pragma once

#include <core/typedefs.h>
#include <core/string/ustring.h>
#include <core/error/error_list.h>
#include <core/object/ref_counted.h>

namespace bwn
{
	class BaseFacility;

	class SystemResult
	{
		friend BaseFacility;

		//
		// Public typedefs.
		//
	public:
		using NativeError = int32_t;

		//
		// Construction and destruction.
		//
	public:
		// By default, the result is ok.
		SystemResult(BaseFacility& _facility);
		SystemResult(NativeError _nativeError, String _errorDesc, BaseFacility& _facility);

		// Those two created mostly for optimisations (to not call virtual functions) Preferebly not to call them accept from facility.
	private:
		SystemResult(Error _godotCode, NativeError _nativeError, BaseFacility& _facility);
		SystemResult(Error _godotCode, NativeError _nativeError, String _errorDesc, BaseFacility& _facility);

	public:
		SystemResult(const SystemResult& _other);
		SystemResult& operator=(const SystemResult& _other);

		~SystemResult();

		//
		// Public interface.
		//
	public:
		// Returns true if result is simple success.
		_FORCE_INLINE_ bool success() const;
		// If result is not a success, it could hold an additional error message.
		_FORCE_INLINE_ const String& getErrorDescription() const;
		// Returns system error custed from native error code.
		_FORCE_INLINE_ Error getError() const;
		// Returns error code, which is native to the system which produced this result.
		_FORCE_INLINE_ NativeError getNativeError() const;
		// Returns facility which produced this error.
		const BaseFacility* getFacility() const;
		// Tries to retrieve facility and coverts native error to const char*. If facility invalid returns "<invalid system result>".
		const char* stringFromError() const;

		//
		// Private members.
		//
	private:
		// The description for the error.
		String m_errorDescription;
		// Native error code, from the system which produced this error.
		NativeError m_nativeError = 0;
		// Error code of the godot (aka Error type). This is 2 byte so the entire type whould only be 16 bytes.
		uint16_t m_godotErrorCode = 0;
		// The id of the facility which corresponds to the native error code.
		uint16_t m_facilityId = 0;
	};

	class SystemResultObject : public RefCounted
	{
		GDCLASS(SystemResultObject, RefCounted);

		//
		// Construction and destruction.
		//
	public:
		// Default constructor is generally unadvised to use. It's used specifically only for instantiation of this class in ClassDB.
		SystemResultObject();
		SystemResultObject(const SystemResult& _result);

		//
		// Godot methods.
		//
	private:
		static void _bind_methods();

		//
		// Public interface.
		//
	public:
		// Returns true if result is simple success.
		bool success() const;
		// If result is not a success, it could hold an additional error message.
		const String& getErrorDescription() const;
		// Returns system error custed from native error code.
		Error getError() const;
		// Returns error code, which is native to the system which produced this result.
		SystemResult::NativeError getNativeError() const;
		// Tries to retrieve facility and coverts native error to const char*. If facility invalid returns "<invalid system result>".
		String stringFromError() const;

		//
		// Private members.
		//
	private:
		// The description for the error.
		String m_errorDescription;
		// Native error code, from the system which produced this error.
		SystemResult::NativeError m_nativeError = 0;
		// Error code of the godot (aka Error type). This is 2 byte so the entire type whould only be 16 bytes.
		uint16_t m_godotErrorCode = 0;
		// The id of the facility which corresponds to the native error code.
		uint16_t m_facilityId = 0;
	};

} // namespace bwn

#include "bwn_core/results/systemResult.inl"