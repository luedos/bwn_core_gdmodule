#include "precompiled_core.hpp"

#include "bwn_core/results/baseFacility.hpp"

using FacilityRegestry = std::unordered_map<bwn::BaseFacility::FacilityId, bwn::BaseFacility*>;
static FacilityRegestry g_facilityRegestry;

bwn::BaseFacility::BaseFacility(FacilityId _id)
	: m_id( _id )
#if defined(DEBUG_ENABLED)
	, m_resultsCount( 0 )
#endif
{
	registerFacility(m_id, *this);
}

bwn::BaseFacility::~BaseFacility()
{
#if defined(DEBUG_ENABLED)
	{
		std::unique_lock<bwn::spinlock> lock(m_countSpinlock);
		BWN_ASSERT_ERROR_COND(m_resultsCount == 0, "Facility by type {} still have {} undestructed results.", get_class(), m_resultsCount);
	}
#endif

	unregisterFacility(m_id);
}

bwn::BaseFacility::FacilityId bwn::BaseFacility::getId() const
{
	return m_id;
}

void bwn::BaseFacility::registerFacility(FacilityId _id, BaseFacility& _facility)
{
	FacilityRegestry::iterator it = g_facilityRegestry.find(_id);
	BWN_ASSERT_WARNING_COND(
		it == g_facilityRegestry.end(), 
		"Another Facility already registered by that id ({}). The old Facility: '{}'. The new Facility: '{}'.",
		_id,
		it->second->get_class(),
		_facility.get_class());
	if (it != g_facilityRegestry.end())
	{
		return;
	}

	g_facilityRegestry.emplace(_id, &_facility);
}

void bwn::BaseFacility::unregisterFacility(FacilityId _id)
{
	FacilityRegestry::iterator it = g_facilityRegestry.find(_id);
	BWN_ASSERT_WARNING_COND(it != g_facilityRegestry.end(), "No facility found by the id '{}'.", _id);
	if (it != g_facilityRegestry.end())
	{
		g_facilityRegestry.erase(it);
	}
}
	
#if defined(DEBUG_ENABLED)
void bwn::BaseFacility::referencerResult(SystemResult& _result)
{
	std::unique_lock<bwn::spinlock> lock(m_countSpinlock);
	++m_resultsCount;
}
void bwn::BaseFacility::unreferencerResult(SystemResult& _result)
{
	std::unique_lock<bwn::spinlock> lock(m_countSpinlock);
	BWN_ASSERT_WARNING_COND(m_resultsCount != 0, "Can't unreference result in facility '{}', the result count already 0.", get_class());
	if (m_resultsCount != 0)
	{
		--m_resultsCount;
	}
}
#endif

bwn::BaseFacility* bwn::BaseFacility::findFacility(FacilityId _id)
{
	FacilityRegestry::iterator it = g_facilityRegestry.find(_id);
	BWN_ASSERT_ERROR_COND(it != g_facilityRegestry.end(), "No facility found by the id '{}'.", _id);
	if (it != g_facilityRegestry.end())
	{
		return it->second;
	}

	return nullptr;
}