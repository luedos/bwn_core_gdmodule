#pragma once

#include "bwn_core/results/systemResult.hpp"
#include "bwn_core/thread/spinlock.hpp"

#include <core/error/error_list.h>
#include <core/object/object.h>

#include <limits>

namespace bwn
{
	class SystemResult;

	class BaseFacility : public Object
	{
		friend SystemResult;
		
		GDCLASS(BaseFacility, Object);

		//
		// Public typedefs.
		//
	public:
		using NativeError = SystemResult::NativeError;
		using FacilityId = uint16_t;

		static constexpr FacilityId k_invalidFacilityId = std::numeric_limits<FacilityId>::max();

		//
		// Construction and destruction.
		//
	protected:
		BaseFacility(FacilityId _id);
		virtual ~BaseFacility();

		//
		// Public interface.
		//
	public:
		virtual NativeError getSuccessCodeV() = 0; 
		virtual Error castNativeErrorV(NativeError _nativeError) const = 0;
		// Returns string pointer. String pointer is guaranteed to not be nullptr.
		virtual const char* stringFromErrorV(NativeError _nativeError) const = 0;
		FacilityId getId() const;
		// Returns facility from the stack.
		static BaseFacility* findFacility(FacilityId _id);

		//
		// Protected methods.
		//
	protected:
		_FORCE_INLINE_ SystemResult createResult(Error _godotError, NativeError _nativeError, String _description);
		
		//
		// Private methods.
		//
	private:
		// Registers facility in the stack (SystemError can requiere facility from the stack).
		static void registerFacility(FacilityId _id, BaseFacility& _facility);
		// Unregisters facility from the stack.
		static void unregisterFacility(FacilityId _id);
		
	#if defined(DEBUG_ENABLED)
		// Just for the debug purpuses.
		virtual void referencerResult(SystemResult& _result);
		virtual void unreferencerResult(SystemResult& _result);
	#endif
		
		//
		// Private members.
		//
	private:
		FacilityId m_id = 0;
	#if defined(DEBUG_ENABLED)
		// This spinlock is used for the counts.
		bwn::spinlock m_countSpinlock;
		// Count of the results currently pointing to this facility. 
		uint32_t m_resultsCount = 0;
	#endif
	};

	_FORCE_INLINE_ SystemResult BaseFacility::createResult(Error _godotError, NativeError _nativeError, String _description)
	{
		return SystemResult(_godotError, _nativeError, _description, *this);	
	}
} // namespace bwn