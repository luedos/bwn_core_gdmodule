#pragma once

#include "bwn_core/results/baseFacility.hpp"
#include "bwn_core/types/singleton.hpp"

namespace bwn
{
	class SystemFacility final : public BaseFacility, public ManualSingleton<SystemFacility>
	{
		friend ManualSingleton<SystemFacility>;

		GDCLASS(SystemFacility, BaseFacility);

		//
		// Construction and destruction.
		//
	private:
		SystemFacility();
		virtual ~SystemFacility() override;

		//
		// Godot methods.
		//
	protected:
		static void _bind_methods();

		//
		// Public interface.
		//
	public:
		virtual NativeError getSuccessCodeV() override; 
		virtual Error castNativeErrorV(const NativeError _nativeError) const override;
		virtual const char* stringFromErrorV(const NativeError _nativeError) const override;

		static const char* stringFromError(const Error _error);

		static SystemResult createSuccess();
		static SystemResult createError(const Error _resultCode, const String& _description = String());

		//
		// Private methods.
		//
	private:
		// Godot wrappers for our methods.
		static Ref<SystemResultObject> createSuccessObject();
		static Ref<SystemResultObject> createErrorObject(const Error _resultCode, const String& _description = String());
	};

} // namespace bwn