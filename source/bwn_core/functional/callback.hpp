#pragma once

#include "bwn_core/utility/templateAlgorithms.hpp"

#include <type_traits>
#include <cstring>

namespace bwn
{
	namespace callback_detail
	{
		template<typename SignatureT>
		struct BaseFunctorWrapper;

		template<typename SignatureT, typename FunctorT>
		struct DerivedFunctorWrapper;

		struct UnknownType { char blank[1]; };

		template<typename SignatureT>
		struct BasicCallable;

		template<typename ReturnT, typename...ArgTs>
		struct BasicCallable<ReturnT(ArgTs...)>
		{
		    template<typename LocalClassT>
		    void set(LocalClassT* _obj, ReturnT(LocalClassT::*_method)(ArgTs...));
		    template<typename LocalClassT>
		    void set(const LocalClassT* _obj, ReturnT(LocalClassT::*_method)(ArgTs...) const);

			UnknownType* m_this;
			void* m_method;
		};
	} // namespace callback_detail

	template<typename SignatureT, size_t k_bufferSize>
	class basic_callback;

	template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
	class basic_callback<ReturnT(ArgTs...), k_bufferSize>
	{
		template<typename LocalSignatureT, size_t k_localBufferSize>
		friend class basic_callback;

	    //
	    // Private classes.
	    //
	private:
	    using FunctionPrototype = ReturnT(*)(ArgTs...);
	    using MethodPrototype = ReturnT(callback_detail::UnknownType::*)(ArgTs...);

	    using Signature = ReturnT(ArgTs...);

		// Simple sfinae to check if provided type is a basic_callback.
		template<typename TestTypeT>
		struct IsSelf : public std::false_type
		{};

		template<typename LocalReturnT, typename...LocalArgTs, size_t k_otherBufferSize>
		struct IsSelf<basic_callback<LocalReturnT(LocalArgTs...), k_otherBufferSize> > : public std::true_type
		{};

	    //
	    // Construction and destruction.
	    //
	public:
	    // Default constructor.
	    basic_callback();

	    // Constructors for the simple function pointer.
	    basic_callback(ReturnT(*_functionPtr)(ArgTs...));
	    basic_callback(std::nullptr_t);

	    // Constructors for the member methods.
	    template<typename LocalClassT>
	    basic_callback(LocalClassT* _obj, ReturnT(LocalClassT::*_methodPtr)(ArgTs...));

	    template<typename LocalClassT>
	    basic_callback(const LocalClassT* _obj, ReturnT(LocalClassT::*_methodPtr)(ArgTs...) const);

	    // Constructor for the custom functor.
	    template<
			typename LocalFunctorT,
			typename=typename std::enable_if<
				!IsSelf<typename std::remove_reference<LocalFunctorT>::type>::value
			>::type>
	    basic_callback(LocalFunctorT&& _functor);

	    basic_callback(const basic_callback& _other);
	    basic_callback(basic_callback&& _other) noexcept;

	    // Copy/Move constructors for callbacks with different buffer size from this one.
		template<size_t k_otherBufferSize>
	    basic_callback(const basic_callback<ReturnT(ArgTs...), k_otherBufferSize>& _other);
		template<size_t k_otherBufferSize>
	    basic_callback(basic_callback<ReturnT(ArgTs...), k_otherBufferSize>&& _other) noexcept;

	    ~basic_callback();

	    basic_callback& operator=(const basic_callback& _other);
	    basic_callback& operator=(basic_callback&& _other) noexcept;

	    // Copy/Move operators for callbacks with different buffer size from this one.
		template<size_t k_otherBufferSize>
	    basic_callback& operator=(const basic_callback<ReturnT(ArgTs...), k_otherBufferSize>& _other);
		template<size_t k_otherBufferSize>
	    basic_callback& operator=(basic_callback<ReturnT(ArgTs...), k_otherBufferSize>&& _other) noexcept;

	    //
	    // Public interface.
	    //
	public:
		// Compares functions from callback. If callback holds functor with operator==, it will be used.
		template<size_t k_otherBufferSize>
	    bool is_equal(const basic_callback<ReturnT(ArgTs...), k_otherBufferSize>& _other) const;
		// Just a wrapper for 'isEqual'
	    operator bool() const;
		// Returns true if callback was initialized.
	    bool is_valid() const;
		// Invokes function held in callback.
	    ReturnT operator() (ArgTs..._args) const;
	    ReturnT invoke(ArgTs..._args) const;
		// Swaps contents of two callbacks
	    void swap(basic_callback& _other) noexcept;
		// Swaps contents of two callbacks
		template<size_t k_otherBufferSize, typename=typename std::enable_if<k_otherBufferSize != k_bufferSize>::type>
	    void swap(basic_callback<ReturnT(ArgTs...), k_otherBufferSize>& _other);
	    // Resets callback to nullptr.
	    void clear();
		// Basically, returns true if callback, which was initialized the same way this one was, could be compared with this one.
		// In most cases it's true, but it will return false if callback holds functor, and this functor does not have operator==.
		// This is because functor is copied into the callback, and if functor doesn't has compare operator, 
		// only pointers of functor copies will be compared (which is basically nothing).
		bool is_clone_comparable() const;

	    //
	    // Private methods.
	    //
	private:
	    
	    bool has_functor() const;
	    // This function doesn't check if we actually has functor, it just calculates pointer to it.
	    // Check if functor even exist before using it!
	    const callback_detail::BaseFunctorWrapper<Signature>* get_functor() const;

	    void* allocate(size_t _size);
	    bool is_local(void* _ptr) const;

	    void initialize_empty();
	    void initialize_empty_buffer();

	    //
	    // Private members.
	    //
	private:
	    callback_detail::BasicCallable<Signature> m_callable;
	    // This buffer is basically an sso for functor wrapper.
	    // Because of this we need at least 1 size_t for virtual pointer, and another one for functor.
		static constexpr size_t k_totalBufferCount = 1 + (k_bufferSize + sizeof(size_t) - 1) / sizeof(size_t);
	    size_t m_buffer[k_totalBufferCount];
	};

	namespace callback_detail
	{
		// Basically a struct for functor comparison.
		// If the type has equal operator, then we will use that,
		// but by default we just compare addresses
		template<typename LocalTypeT, typename=void>
		struct Comparator : public std::false_type
		{
			static bool compare(const LocalTypeT& _left, const LocalTypeT& _right)
			{
				return &_left == &_right;
			}
		};

		template<typename LocalTypeT>
		struct Comparator<
			LocalTypeT, 
			typename std::enable_if<
				std::is_convertible<
					decltype(std::declval<LocalTypeT>() == std::declval<LocalTypeT>()), 
					bool
				>::value
			>::type> : std::true_type
		{
			static bool compare(const LocalTypeT& _left, const LocalTypeT& _right)
			{
				return _left == _right;
			}
		};

		template<typename ReturnT, typename...ArgTs>
		struct BaseFunctorWrapper<ReturnT(ArgTs...)>
		{
			virtual ~BaseFunctorWrapper() = default;
			virtual void cloneIntoV(void* _memory, BasicCallable<ReturnT(ArgTs...)>& _callable) const = 0;
			virtual size_t getSizeV() const = 0;
			virtual bool isEqualV(const BaseFunctorWrapper& _other) const = 0;
			virtual bool canBeComparedV() const = 0;
		};

		template<typename FunctorT, typename ReturnT, typename...ArgTs>
		struct DerivedFunctorWrapper<ReturnT(ArgTs...), FunctorT> : public BaseFunctorWrapper<ReturnT(ArgTs...)>
		{
			DerivedFunctorWrapper(FunctorT _functor)
				: m_functor( std::move(_functor) )
			{}
			virtual ~DerivedFunctorWrapper() override = default;

			virtual void cloneIntoV(void* _memory, BasicCallable<ReturnT(ArgTs...)>& _callable) const override
			{
				FunctorT functorCopy = m_functor;
				DerivedFunctorWrapper*const clonedWrapper = new (_memory) DerivedFunctorWrapper(std::move(functorCopy));

				_callable.set(&clonedWrapper->m_functor, &FunctorT::operator());
			}

			virtual size_t getSizeV() const override
			{
				return sizeof(DerivedFunctorWrapper);
			}

			virtual bool isEqualV(const BaseFunctorWrapper<ReturnT(ArgTs...)>& _other) const override
			{
				// Ok, this is kind of ub, but this is fine in our situation.
				// To check if two classes is the same type, we could symply check their virtual pointers.
				// In all main compilers, for basic case of one Derive from one Base,
				// virtual pointer will be placed in front of the class.
				const bool sameClass = *reinterpret_cast<const size_t*>(&_other) == *reinterpret_cast<const size_t*>(this);
				if (!sameClass)
				{
					return false;
				}

				const DerivedFunctorWrapper*const castedOther = static_cast<const DerivedFunctorWrapper*>(&_other);
				return Comparator<FunctorT>::compare(m_functor, castedOther->m_functor);
			}

			virtual bool canBeComparedV() const override
			{
				return Comparator<FunctorT>::value;
			}

			FunctorT m_functor;
		};

		template<typename ReturnT, typename...ArgsTs>
		auto decayFunctionType(ReturnT(*)(ArgsTs...)) -> ReturnT(*)(ArgsTs...);

		template<typename TypeT, typename ReturnT, typename...ArgsTs>
		auto decayFunctionType(ReturnT(TypeT::*)(ArgsTs...)) -> ReturnT(*)(ArgsTs...);

		template<typename TypeT, typename ReturnT, typename...ArgsTs>
		auto decayFunctionType(ReturnT(TypeT::*)(ArgsTs...) const) -> ReturnT(*)(ArgsTs...);

		template<typename FunctorT>
		auto decayFunctionType(FunctorT, ...) -> decltype(decayFunctionType(&FunctorT::operator()));

		template<typename TypeT>
		struct DecayFunctionType
		{
			using Type = typename std::remove_pointer<decltype(decayFunctionType(std::declval<TypeT>()))>::type;
		};

	} // namespace callback_detail

} // namespace bwn

#include "bwn_core/functional/callback.inl"

namespace bwn
{
	template<typename SignatureT>
	using callback = basic_callback<SignatureT, sizeof(void*) * 2>;

	template<typename TypeT>
	_FORCE_INLINE_ auto wrap_callback(TypeT&& _callable)
	{
		using DecayedType = typename std::remove_cv<typename std::remove_reference<TypeT>::type>::type;
		using FunctionType = typename callback_detail::DecayFunctionType<DecayedType>::Type;
		return bwn::callback<FunctionType>(std::forward<TypeT>(_callable));
	}

	template<typename TypeT, typename ReturnT, typename...ArgsTs>
	_FORCE_INLINE_ auto wrap_callback(TypeT*const _callable, ReturnT (TypeT::*const _function)(ArgsTs...))
	{
		return bwn::callback<ReturnT(ArgsTs...)>(_callable, _function);
	}

	template<typename TypeT, typename ReturnT, typename...ArgsTs>
	_FORCE_INLINE_ auto wrap_callback(const TypeT*const _callable, ReturnT (TypeT::*const _function)(ArgsTs...) const)
	{
		return bwn::callback<ReturnT(ArgsTs...)>(_callable, _function);
	}
} // namespace bwn