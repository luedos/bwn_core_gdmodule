#pragma once

#include "bwn_core/functional/callback.hpp"
#include "bwn_core/debug/debug.hpp"

template<typename ReturnT, typename...ArgTs>
template<typename LocalClassT>
void bwn::callback_detail::BasicCallable<ReturnT(ArgTs...)>::set(LocalClassT* _obj, ReturnT(LocalClassT::*_method)(ArgTs...))
{
	// Ok, this is very dangerous ub, don't do it at home.
	struct PlainThisGetter
	{
		UnknownType* get()
		{
			// Basically, anything will be passed here as 'this' will be returned as UnknownType*
			return (UnknownType*)(void*)(this);
		}
	};

	union
	{
		UnknownType*(PlainThisGetter::*realGetThis)(); // This will override function pointer.
		UnknownType*(LocalClassT::*masskedGetThis)(); // This is just combination of two things above.
		ReturnT(LocalClassT::*method)(ArgTs...); // This will give us all neccecery info beside simple function pointer.
	} conversion;

	conversion.method = _method;
	conversion.realGetThis = &PlainThisGetter::get;

	m_this = (_obj->*conversion.masskedGetThis)();
	m_method = bwn::union_cast<void*>(_method);
}

template<typename ReturnT, typename...ArgTs>
template<typename LocalClassT>
void bwn::callback_detail::BasicCallable<ReturnT(ArgTs...)>::set(const LocalClassT* _obj, ReturnT(LocalClassT::*_method)(ArgTs...) const)
{
	set(const_cast<LocalClassT*>(_obj), bwn::union_cast<ReturnT(LocalClassT::*)(ArgTs...)>(_method));
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback()
{
	initialize_empty();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(ReturnT(*_functionPtr)(ArgTs...))
{
	if (_functionPtr == nullptr)
	{
		initialize_empty();
		return;
	}

	struct FunctionCaller
	{
		ReturnT call(ArgTs..._args) const
		{
			return ((FunctionPrototype)(void*)(this))(std::forward<ArgTs>(_args)...);
		}
	};

	m_callable.set((FunctionCaller*)(void*)(_functionPtr), &FunctionCaller::call);
	initialize_empty_buffer();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(std::nullptr_t)
	: basic_callback()
{}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<typename LocalClassT>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(LocalClassT* _obj, ReturnT(LocalClassT::*_methodPtr)(ArgTs...))
{
	if (_methodPtr == nullptr)
	{
		initialize_empty();
		return;
	}

	BWN_ASSERT_WARNING_COND(_obj != nullptr, "This is not safe to pass to callback non null method pointer, but nullptr for the object.");

	m_callable.set(_obj, _methodPtr);
	initialize_empty_buffer();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<typename LocalClassT>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(const LocalClassT* _obj, ReturnT(LocalClassT::*_methodPtr)(ArgTs...) const)
{
	if (_methodPtr == nullptr)
	{
		initialize_empty();
		return;
	}

	BWN_ASSERT_WARNING_COND(_obj != nullptr, "This is not safe to pass to callback non null method pointer, but nullptr for the object.");

	m_callable.set(_obj, _methodPtr);
	initialize_empty_buffer();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<typename LocalFunctorT, typename>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(LocalFunctorT&& _functor)
{
	using Functor = typename std::remove_reference<LocalFunctorT>::type;
	using LocalFunctorWrapper = callback_detail::DerivedFunctorWrapper<Signature, Functor>;
	
	void*const ptr = allocate(sizeof(LocalFunctorWrapper));
	LocalFunctorWrapper*const wrapper = new (ptr) LocalFunctorWrapper(std::forward<LocalFunctorT>(_functor));

	m_callable.set(&wrapper->m_functor, &Functor::operator());
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(const basic_callback& _other)
{
	if (!_other.is_valid())
	{
		initialize_empty();
		return;
	}

	if (_other.has_functor())
	{
		const callback_detail::BaseFunctorWrapper<Signature>*const wrapper = _other.get_functor();
		void*const memory = allocate(wrapper->getSizeV());
		wrapper->cloneIntoV(memory, m_callable);
	}
	else
	{
		m_callable.m_this = _other.m_callable.m_this;
		m_callable.m_method = _other.m_callable.m_method;
		initialize_empty_buffer();
	}
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(basic_callback&& _other) noexcept
{
	initialize_empty();
	swap(_other);
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<size_t k_otherBufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(const basic_callback<ReturnT(ArgTs...), k_otherBufferSize>& _other)
{
	if (!_other.is_valid())
	{
		initialize_empty();
		return;
	}

	if (_other.has_functor())
	{
		const callback_detail::BaseFunctorWrapper<Signature>*const wrapper = _other.get_functor();
		void*const memory = allocate(wrapper->getSizeV());
		wrapper->cloneIntoV(memory, m_callable);
	}
	else
	{
		m_callable.m_this = _other.m_callable.m_this;
		m_callable.m_method = _other.m_callable.m_method;
		initialize_empty_buffer();
	}
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<size_t k_otherBufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(basic_callback<ReturnT(ArgTs...), k_otherBufferSize>&& _other) noexcept
{
	initialize_empty();
	swap(_other);
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::~basic_callback()
{
	clear();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>& bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::operator=(
	const basic_callback& _other)
{
	if (&_other == this)
	{
		return *this;
	}

	basic_callback clonedCallback = _other;
	swap(clonedCallback);

	return *this;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>& bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::operator=(
	basic_callback&& _other) noexcept
{
	if (&_other == this)
	{
		return *this;
	}

	clear();
	swap(_other);

	return *this;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<size_t k_otherBufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>& bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::operator=(
	const basic_callback<ReturnT(ArgTs...),k_otherBufferSize>& _other)
{
	if (&_other == this)
	{
		return *this;
	}

	basic_callback clonedCallback = _other;
	swap(clonedCallback);

	return *this;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<size_t k_otherBufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>& bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::operator=(
	basic_callback<ReturnT(ArgTs...),k_otherBufferSize>&& _other) noexcept
{
	if (&_other == this)
	{
		return *this;
	}

	clear();
	swap(_other);

	return *this;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<size_t k_otherBufferSize>
bool bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::is_equal(const basic_callback<ReturnT(ArgTs...),k_otherBufferSize>& _other) const
{
	const bool isFunctor = has_functor();

	if (isFunctor != _other.has_functor())
	{
		// If we hold functor but other does not, or in reverse, where is no point in comparison.
		return false;
	}

	if (isFunctor)
	{
		const callback_detail::BaseFunctorWrapper<Signature>*const selfWrapper = get_functor();
		const callback_detail::BaseFunctorWrapper<Signature>*const otherWrapper = _other.get_functor();

		return selfWrapper->isEqualV(*otherWrapper);
	}

	// If this is not a functor comparison, then we have more or less no other option, other then to just compare function and class pointers.
	return m_callable.m_this == _other.m_callable.m_this 
		&& m_callable.m_method == _other.m_callable.m_method;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::operator bool() const
{
	return is_valid();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bool bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::is_valid() const
{
	// theoretically m_this could equal to 0, and still be called correctly, 
	// but if m_method == nullptr, that's mean we simply not initialized basic_callback yet.
	return m_callable.m_method != nullptr;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
ReturnT bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::operator()(ArgTs..._args) const
{
	BWN_TRAP_COND(is_valid(), "Calling empty callback! Why?");

	union
	{
		MethodPrototype method;
		void* voidPtr;
	} conversion;
	conversion.method = 0;
	conversion.voidPtr = m_callable.m_method;

	return ((m_callable.m_this)->*conversion.method)(std::forward<ArgTs>(_args)...);
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
ReturnT bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::invoke(ArgTs..._args) const
{
	BWN_TRAP_COND(is_valid(), "Calling empty callback! Why?");

	union
	{
		MethodPrototype method;
		void* voidPtr;
	} conversion;
	conversion.method = 0;
	conversion.voidPtr = m_callable.m_method;

	return ((m_callable.m_this)->*conversion.method)(std::forward<ArgTs>(_args)...);
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
void bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::swap(basic_callback& _other) noexcept
{
	callback_detail::UnknownType*const oldThis = m_callable.m_this;

	if (_other.is_local(_other.m_callable.m_this))
	{
		const size_t thisOffset = (size_t)_other.m_callable.m_this - (size_t)(char*)_other.m_buffer;
		m_callable.m_this = (callback_detail::UnknownType*)((char*)(m_buffer) + thisOffset);
	}
	else
	{
		m_callable.m_this = _other.m_callable.m_this;
	}

	if (is_local(oldThis))
	{
		const size_t thisOffset = (size_t)oldThis - (size_t)(char*)m_buffer;
		_other.m_callable.m_this = (callback_detail::UnknownType*)((char*)(_other.m_buffer) + thisOffset);
	}
	else
	{
		_other.m_callable.m_this = oldThis;
	}

	std::swap(m_callable.m_method, _other.m_callable.m_method);

	size_t*const bufferEnd = (size_t*)((char*)m_buffer + sizeof(m_buffer));

	for (std::size_t* selfBufferIt = m_buffer, *otherBufferIt = _other.m_buffer; 
		selfBufferIt < bufferEnd; 
		++selfBufferIt, ++otherBufferIt)
	{
		std::swap(*selfBufferIt, *otherBufferIt);
	}
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<size_t k_otherBufferSize, typename>
void bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::swap(basic_callback<ReturnT(ArgTs...),k_otherBufferSize>& _other)
{
	const callback_detail::BasicCallable<Signature> oldCallable = m_callable;
	size_t oldBuffer[k_totalBufferCount];
	memcpy(oldBuffer, m_buffer, sizeof(m_buffer));

	// Moving other 'this' into our this.
	if (_other.is_local(_other.m_callable.m_this))
	{
		const size_t otherFunctorSize = _other.get_functor()->getSizeV();

		if (k_otherBufferSize < k_bufferSize || otherFunctorSize < sizeof(m_buffer))
		{
			// If other buffer is smaller, that we could just copy other functor from local into our local. 
			// Copying will be done in the end of swap, by swaping buffers.
			const size_t thisOffset = (size_t)_other.m_callable.m_this - (size_t)(char*)_other.m_buffer;
			m_callable.m_this = (callback_detail::UnknownType*)((char*)(m_buffer) + thisOffset);
			m_callable.m_method = _other.m_callable.m_method;

			memcpy(m_buffer, _other.m_buffer, otherFunctorSize);
		}
		else
		{
			void*const memory = allocate(otherFunctorSize);
			_other.get_functor()->cloneIntoV(memory, m_callable);
		}
	}
	else
	{
		m_callable.m_this = _other.m_callable.m_this;
		m_callable.m_method = _other.m_callable.m_method;
		m_buffer[0] = _other.m_buffer[0];
	}

	if (is_local(oldCallable.m_this))
	{
		// at this moment we already could override buffer, so getFunctor will return invalid result.
		const callback_detail::BaseFunctorWrapper<Signature>*const oldWrapper = (const callback_detail::BaseFunctorWrapper<Signature>*)(oldBuffer);
		const size_t oldFunctorSize = oldWrapper->getSizeV();

		if (k_bufferSize < k_otherBufferSize || oldFunctorSize < sizeof(_other.m_buffer))
		{
			// If our buffer is smaller, that we could just copy functor from local into other local. 
			// Copying will be done in the end of swap, by swaping buffers.
			const size_t thisOffset = (size_t)oldCallable.m_this - (size_t)(char*)m_buffer;
			_other.m_callable.m_this = (callback_detail::UnknownType*)((char*)(_other.m_buffer) + thisOffset);
			_other.m_callable.m_method = oldCallable.m_method;

			memcpy(_other.m_buffer, oldBuffer, oldFunctorSize);
		}
		else
		{
			void*const memory = _other.allocate(oldFunctorSize);
			oldWrapper->cloneIntoV(memory, _other.m_callable);
		}
	}
	else
	{
		_other.m_callable.m_this = oldCallable.m_this;
		_other.m_callable.m_method = oldCallable.m_method;
		_other.m_buffer[0] = oldBuffer[0];
	}
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
void bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::clear()
{
	if (has_functor())
	{
		const bool local = is_local(m_callable.m_this);

		void*const blankPtr = local ? m_buffer : (void*)m_buffer[0];
		((callback_detail::BaseFunctorWrapper<Signature>*)blankPtr)->~BaseFunctorWrapper();

		if (!local)
		{
			free(blankPtr);
		}
	}

	initialize_empty();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bool bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::is_clone_comparable() const
{
	return !has_functor() || get_functor()->canBeComparedV();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bool bwn::basic_callback<ReturnT(ArgTs...), k_bufferSize>::has_functor() const
{
	return m_buffer[0] != 0;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
const bwn::callback_detail::BaseFunctorWrapper<ReturnT(ArgTs...)>* bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::get_functor() const
{
	const void*const rawWrapperPtr = is_local(m_callable.m_this)
		? m_buffer
		: (const void*)(m_buffer[0]);

	return (const callback_detail::BaseFunctorWrapper<ReturnT(ArgTs...)>*)(rawWrapperPtr);
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
void* bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::allocate(size_t _size)
{
	if (_size > sizeof(m_buffer))
	{
		void*const ptr = malloc(_size);
		m_buffer[0] = size_t(ptr);
		return ptr;
	}
	else
	{
		return m_buffer;
	}
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bool bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::is_local(void* _ptr) const
{
	return _ptr >= m_buffer && _ptr < (((const char*)m_buffer) + sizeof(m_buffer));
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
void bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::initialize_empty()
{
	m_callable.m_this = nullptr;
	m_callable.m_method = nullptr;
	initialize_empty_buffer();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
void bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::initialize_empty_buffer()
{
	memset(m_buffer, 0, sizeof(m_buffer));
}

template<typename SignatureT, size_t k_leftBufferIntPtrCount, size_t k_rightBufferIntPtrCount>
inline bool operator==(
	const bwn::basic_callback<SignatureT,k_leftBufferIntPtrCount>& _left, 
	const bwn::basic_callback<SignatureT,k_rightBufferIntPtrCount>& _right)
{
	return _left.is_equal(_right);
}
template<typename SignatureT, size_t k_bufferSize>
inline bool operator==(const bwn::basic_callback<SignatureT,k_bufferSize>& _left, std::nullptr_t)
{
	return !_left.is_valid();
}
template<typename SignatureT, size_t k_bufferSize>
inline bool operator==(std::nullptr_t, const bwn::basic_callback<SignatureT,k_bufferSize>& _right)
{
	return !_right.is_valid();
}

template<typename SignatureT, size_t k_leftBufferIntPtrCount, size_t k_rightBufferIntPtrCount>
inline bool operator!=(
	const bwn::basic_callback<SignatureT,k_leftBufferIntPtrCount>& _left, 
	const bwn::basic_callback<SignatureT,k_rightBufferIntPtrCount>& _right)
{
	return !_left.is_equal(_right);
}
template<typename SignatureT, size_t k_bufferSize>
inline bool operator!=(const bwn::basic_callback<SignatureT,k_bufferSize>& _left, std::nullptr_t)
{
	return _left.is_valid();
}
template<typename SignatureT, size_t k_bufferSize>
inline bool operator!=(std::nullptr_t, const bwn::basic_callback<SignatureT,k_bufferSize>& _right)
{
	return _right.is_valid();
}