#pragma once

#include "bwn_core/thread/spinlock.hpp"

bwn::spinlock::spinlock()
	: m_lock( false )
{}

void bwn::spinlock::lock() noexcept 
{
	for (;;) 
	{
		// Optimistically assume the lock is free on the first try
		if (!m_lock.exchange(true, std::memory_order_acquire)) 
		{
			return;
		}
		// Wait for lock to be released without generating cache misses
		while (m_lock.load(std::memory_order_relaxed)) 
		{
			// Issue X86 PAUSE or ARM YIELD instruction to reduce contention between
			// hyper-threads
		#if defined(WINDOWS_ENABLED)
			_mm_pause();
		#elif defined(UNIX_ENABLED)
			__builtin_ia32_pause();
		#else
			#error "Not supported system for spinlock implementation"
		#endif
		}
	}
}

inline bool bwn::spinlock::try_lock() noexcept 
{
	// First do a relaxed load to check if lock is free in order to prevent
	// unnecessary cache misses if someone does while(!try_lock())
	return !m_lock.load(std::memory_order_relaxed) 
		&& !m_lock.exchange(true, std::memory_order_acquire);
}

inline void bwn::spinlock::unlock() noexcept 
{
	m_lock.store(false, std::memory_order_release);
}