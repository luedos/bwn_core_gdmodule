#pragma once

#include <atomic>

#if defined(DEBUG_ENABLED)
	#include <mutex>
	#define BWN_FAKE_SCOPE_LOCK(_lock) ::std::unique_lock<::bwn::fakelock> fakelockScopedLock ## __LINE__{_lock}
#else
	#define BWN_FAKE_SCOPE_LOCK(_lock) (void)(0)
#endif

namespace bwn
{
	class fakelock
	{
		//
		// Construction and destruction.
		//
	public:
		inline fakelock();
		inline ~fakelock();

		//
		// Public interface.
		//
	public:
		inline void lock() noexcept;
		inline void unlock() noexcept;

		//
		// Private members.
		//
	private:
		std::atomic_bool m_flag;

	};
} // namespace bwn

#include "bwn_core/thread/fakelock.inl"