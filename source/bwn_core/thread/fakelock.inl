#pragma once

#include "bwn_core/thread/fakelock.hpp"
#include "bwn_core/debug/debug.hpp"

inline bwn::fakelock::fakelock()
	: m_flag( false )
{}
inline bwn::fakelock::~fakelock()
{
	BWN_TRAP_COND(!m_flag.load(), "Destroing fake lock when it's not unlocked. Something bad happened!");
}

inline void bwn::fakelock::lock() noexcept
{
	BWN_TRAP_COND(!m_flag.exchange(true, std::memory_order_acquire), "Locking fake lock when it's already locked.");
}
inline void bwn::fakelock::unlock() noexcept
{
	m_flag.store(false, std::memory_order_release);
}
