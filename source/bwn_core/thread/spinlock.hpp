#pragma once

// Many thanks to the Erik Rigtorp!
// His blog (https://rigtorp.se/spinlock/) not only provided good and easy spinlock implementation,
// but also explanes each line of code in it, which very helps understanding of multithreading.

#include <atomic>

namespace bwn
{
	class spinlock 
	{
		//
		// Construction and destruction.
		//
	public:
		inline spinlock();

		//
		// Public interface.
		//
	public:
		inline void lock() noexcept;
		inline bool try_lock() noexcept;
		inline void unlock() noexcept;

		//
		// Private members.
		//
	private:
		std::atomic<bool> m_lock;
	};

} // namespace bwn

#include "bwn_core/thread/spinlock.inl"