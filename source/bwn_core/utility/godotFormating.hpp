#pragma once

#include "bwn_core/utility/formatUtility.hpp"

#include <core/variant/variant.h>
#include <core/templates/vector.h>
#include <core/string/ustring.h>
#include <core/string/string_name.h>
#include <core/string/node_path.h>

//
// godot Variant
//
template<> 
struct fmt::formatter<Variant, char> 
{
	template<typename ParseContextT>
	constexpr auto parse (ParseContextT& _ctx) 
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const Variant& _variant, FormatContextT& _ctx)
	{
		const String str = _variant; 
		return fmt::detail::write(_ctx.out(), bwn::wrap_utf(str.ptr(), str.length()));
	}
};

template<> 
struct fmt::formatter<Variant, char32_t> 
{
	template<typename ParseContextT>
	constexpr auto parse (ParseContextT& _ctx) 
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const Variant& _variant, FormatContextT& _ctx)
	{
		const String str = _variant; 
		return fmt::detail::write(_ctx.out(), fmt::basic_string_view<char32_t>(str.ptr(), str.length()));
	}
};

//
// godot String
//
template<> 
struct fmt::formatter<String, char> 
{
	template<typename ParseContextT>
	constexpr auto parse (ParseContextT& _ctx) 
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const String& _string, FormatContextT& _ctx)
	{
		auto outIt = _ctx.out();
		auto unchangedOutIt = outIt;

		using Converter = utf::Converter<char, char32_t>;
		const char32_t* inputBeg = _string.ptr();
		const char32_t*const inputEnd = inputBeg + _string.length();
		const bool success = Converter::template covertStringUnsafe<decltype(outIt), const char32_t*, false>(outIt, inputBeg, inputEnd);

		return success
			? outIt
			: unchangedOutIt;
	}
};
template<> 
struct fmt::formatter<String, char32_t> 
{
	template<typename ParseContextT>
	constexpr auto parse (ParseContextT& _ctx) 
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const String& _string, FormatContextT& _ctx)
	{
		return fmt::detail::write(_ctx.out(), fmt::basic_string_view<char32_t>(_string.ptr(), _string.length()));
	}
};

//
// Godot string name
//

template<typename CharT>
struct fmt::formatter<StringName, CharT>
{
	template<typename ParseContextT>
	constexpr auto parse (ParseContextT& _ctx)
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const StringName& _stringName, FormatContextT& _ctx)
	{
		fmt::formatter<String, CharT> stringFormatter;
		return stringFormatter.format(_stringName, _ctx);
	}
};

//
// Godot node path.
//
template<> 
struct fmt::formatter<NodePath, char> 
{
	template<typename ParseContextT>
	constexpr auto parse (ParseContextT& _ctx) 
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const NodePath& _path, FormatContextT& _ctx)
	{
		auto outIt = _ctx.out();
		auto unchangedOutIt = outIt;

		if (_path.is_empty())
		{
			return outIt;
		}

		if (_path.is_absolute())
		{
			*outIt++ = '/';
		}

		const auto writeString = [](auto& outIt, const String& _string) -> bool
		{
			using StringConverter = utf::Converter<char, char32_t>;
			const char32_t* inputBeg = _string.ptr();
			const char32_t*const inputEnd = inputBeg + _string.length();
			return StringConverter::template covertStringUnsafe<decltype(outIt), const char32_t*, false>(outIt, inputBeg, inputEnd);
		};

		const Vector<StringName> pathNames = _path.get_names();
		for (size_t nameId = 0; nameId < pathNames.size(); ++nameId)
		{
			if (nameId > 0)
			{
				*outIt++ = '/';
			}

			if (!writeString(outIt, pathNames[nameId].operator String()))
			{
				return unchangedOutIt;
			}
		}

		const Vector<StringName> pathSubnames = _path.get_subnames();
		for (size_t subnameId = 0; subnameId < pathSubnames.size(); ++subnameId)
		{
			*outIt++ = ':';

			if (!writeString(outIt, pathSubnames[subnameId].operator String()))
			{
				return unchangedOutIt;
			}
		}

		return outIt;
	}
};
template<> 
struct fmt::formatter<NodePath, char32_t> 
{
	template<typename ParseContextT>
	constexpr auto parse (ParseContextT& _ctx) 
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const NodePath& _path, FormatContextT& _ctx)
	{
		auto outIt = _ctx.out();

		if (_path.is_empty())
		{
			return outIt;
		}

		if (_path.is_absolute())
		{
			*outIt++ = '/';
		}

		const auto writeString = [](auto& outIt, const String& _string) -> bool
		{
			fmt::detail::write(outIt, fmt::basic_string_view<char32_t>(_string.ptr(), _string.length()));	
		};

		const Vector<StringName> pathNames = _path.get_names();
		for (size_t nameId = 0; nameId < pathNames.size(); ++nameId)
		{
			if (nameId > 0)
			{
				*outIt++ = '/';
			}
			writeString(outIt, pathNames[nameId].operator String());
		}

		const Vector<StringName> pathSubnames = _path.get_subnames();
		for (size_t subnameId = 0; subnameId < pathSubnames.size(); ++subnameId)
		{
			*outIt++ = ':';
			writeString(outIt, pathSubnames[subnameId].operator String());
		}

		return outIt;
	}
};

//
// godot Vector
//
template<typename TypeT, typename CharT> 
struct fmt::formatter<Vector<TypeT>, CharT> 
{
	template<typename ParseContextT>
	constexpr auto parse(ParseContextT& _ctx) 
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const Vector<TypeT>& _vector, FormatContextT& _ctx)
	{
		auto outIt = _ctx.out();

		auto write = [&outIt](const fmt::basic_string_view<CharT> _str)
		{
			outIt = fmt::detail::write(outIt, _str);
		};
		
		write(::bwn::detail::ArrayOpeningStrLiteral<CharT>::k_value);

		const TypeT* it = _vector.ptr();
		const TypeT*const end = it + _vector.size();
		bool first = true;

		for(; it != end; ++it)
		{
			if (!first) {
				write(::bwn::detail::ArrayDividerStrLiteral<CharT>::k_value);
			}
			first = false;

			outIt = fmt::format_to(outIt, ::bwn::detail::FormatTagStrLiteral<CharT>::k_value, *it);			
		}
		
		write(::bwn::detail::ArrayClosingStrLiteral<CharT>::k_value);

		return outIt;
	}
};