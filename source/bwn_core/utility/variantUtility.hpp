#pragma once

#include "bwn_core/types/stringId.hpp"

#include <core/variant/variant.h>

#include <type_traits>

namespace bwn
{
	namespace detail
	{
		// In general all we want is just to remove const/volatile qualifiers and reference.
		template<typename TypeT>
		auto variantTypeDecayHelper(TypeT) -> typename std::remove_cv<typename std::remove_reference<TypeT>::type>::type;

		template<typename TypeT>
		Object* variantTypeDecayHelper(const Ref<TypeT>&);

		// For the strings.
		const char* variantTypeDecayPtrHelper(const char*);
		const char32_t* variantTypeDecayPtrHelper(const char32_t*);

		// All object types we want to pass as object.
		// This will allow as to cast any types which inherits from Object
		Object* variantTypeDecayPtrHelper(Object*);
		Object* variantTypeDecayPtrHelper(const Object*);

		template<typename TypeT>
		struct VariantTypeDecay
		{
			// By default we just using our helper.
			using Type = decltype(variantTypeDecayHelper(std::declval<TypeT>()));
		};

		template<typename TypeT>
		struct VariantTypeDecay<TypeT*>
		{
			// By default we just using our helper.
			using Type = decltype(variantTypeDecayPtrHelper(std::declval<TypeT*>()));
		};

		template<>
		struct VariantTypeDecay<void>
		{
			// void can't be turned into argument to be passed into function VariantTypeDecayHelper,
			// so we will create special case for it.
			using Type = void;
		};
	} // namespace detail

	template<Variant::Type k_type>
	struct GetVariantStorage;

	template<> struct GetVariantStorage<Variant::Type::NIL> { using Type = void; };
	template<> struct GetVariantStorage<Variant::Type::BOOL> { using Type = bool; };
	template<> struct GetVariantStorage<Variant::Type::INT> { using Type = int64_t; };
	template<> struct GetVariantStorage<Variant::Type::FLOAT> { using Type = double; };
	template<> struct GetVariantStorage<Variant::Type::STRING> { using Type = String; };
	template<> struct GetVariantStorage<Variant::Type::VECTOR2> { using Type = Vector2; };
	template<> struct GetVariantStorage<Variant::Type::VECTOR2I> { using Type = Vector2i; };
	template<> struct GetVariantStorage<Variant::Type::RECT2> { using Type = Rect2; };
	template<> struct GetVariantStorage<Variant::Type::RECT2I> { using Type = Rect2i; };
	template<> struct GetVariantStorage<Variant::Type::VECTOR3> { using Type = Vector3; };
	template<> struct GetVariantStorage<Variant::Type::VECTOR3I> { using Type = Vector3i; };
	template<> struct GetVariantStorage<Variant::Type::TRANSFORM2D> { using Type = Transform2D; };
	template<> struct GetVariantStorage<Variant::Type::VECTOR4> { using Type = Vector4; };
	template<> struct GetVariantStorage<Variant::Type::VECTOR4I> { using Type = Vector4i; };
	template<> struct GetVariantStorage<Variant::Type::PLANE> { using Type = Plane; };
	template<> struct GetVariantStorage<Variant::Type::QUATERNION> { using Type = Quaternion; };
	template<> struct GetVariantStorage<Variant::Type::AABB> { using Type = ::AABB; };
	template<> struct GetVariantStorage<Variant::Type::BASIS> { using Type = Basis; };
	template<> struct GetVariantStorage<Variant::Type::TRANSFORM3D> { using Type = Transform3D; };
	template<> struct GetVariantStorage<Variant::Type::PROJECTION> { using Type = Projection; };
	template<> struct GetVariantStorage<Variant::Type::COLOR> { using Type = Color; };
	template<> struct GetVariantStorage<Variant::Type::STRING_NAME> { using Type = StringName; };
	template<> struct GetVariantStorage<Variant::Type::NODE_PATH> { using Type = NodePath; };
	template<> struct GetVariantStorage<Variant::Type::RID> { using Type = RID; };
	template<> struct GetVariantStorage<Variant::Type::OBJECT> { using Type = Object*; };
	template<> struct GetVariantStorage<Variant::Type::CALLABLE> { using Type = Callable; };
	template<> struct GetVariantStorage<Variant::Type::SIGNAL> { using Type = Signal; };
	template<> struct GetVariantStorage<Variant::Type::DICTIONARY> { using Type = Dictionary; };
	template<> struct GetVariantStorage<Variant::Type::ARRAY> { using Type = Array; };
	template<> struct GetVariantStorage<Variant::Type::PACKED_BYTE_ARRAY> { using Type = PackedByteArray; };
	template<> struct GetVariantStorage<Variant::Type::PACKED_INT32_ARRAY> { using Type = PackedInt32Array; };
	template<> struct GetVariantStorage<Variant::Type::PACKED_INT64_ARRAY> { using Type = PackedInt64Array; };
	template<> struct GetVariantStorage<Variant::Type::PACKED_FLOAT32_ARRAY> { using Type = PackedFloat32Array; };
	template<> struct GetVariantStorage<Variant::Type::PACKED_FLOAT64_ARRAY> { using Type = PackedFloat64Array; };
	template<> struct GetVariantStorage<Variant::Type::PACKED_STRING_ARRAY> { using Type = PackedStringArray; };
	template<> struct GetVariantStorage<Variant::Type::PACKED_VECTOR2_ARRAY> { using Type = PackedVector2Array; };
	template<> struct GetVariantStorage<Variant::Type::PACKED_VECTOR3_ARRAY> { using Type = PackedVector3Array; };
	template<> struct GetVariantStorage<Variant::Type::PACKED_COLOR_ARRAY> { using Type = PackedColorArray; };

	template<typename ValueT>
	struct GetVariantTypeEnum;

	template<> struct GetVariantTypeEnum<void>{ static constexpr Variant::Type k_value = Variant::Type::NIL; };
	template<> struct GetVariantTypeEnum<Variant>{ static constexpr Variant::Type k_value = Variant::Type::NIL; };
	template<> struct GetVariantTypeEnum<bool>{ static constexpr Variant::Type k_value = Variant::Type::BOOL; };
	template<> struct GetVariantTypeEnum<signed int>{ static constexpr Variant::Type k_value = Variant::Type::INT; };
	template<> struct GetVariantTypeEnum<unsigned int>{ static constexpr Variant::Type k_value = Variant::Type::INT; };
	template<> struct GetVariantTypeEnum<signed short>{ static constexpr Variant::Type k_value = Variant::Type::INT; };
	template<> struct GetVariantTypeEnum<unsigned short>{ static constexpr Variant::Type k_value = Variant::Type::INT; };
	template<> struct GetVariantTypeEnum<signed char>{ static constexpr Variant::Type k_value = Variant::Type::INT; };
	template<> struct GetVariantTypeEnum<unsigned char>{ static constexpr Variant::Type k_value = Variant::Type::INT; };
	template<> struct GetVariantTypeEnum<int64_t>{ static constexpr Variant::Type k_value = Variant::Type::INT; };
	template<> struct GetVariantTypeEnum<uint64_t>{ static constexpr Variant::Type k_value = Variant::Type::INT; };

#if defined(NEED_LONG_INT)
	template<> struct GetVariantTypeEnum<signed long>{ static constexpr Variant::Type k_value = Variant::Type::INT; };
	template<> struct GetVariantTypeEnum<unsigned long>{ static constexpr Variant::Type k_value = Variant::Type::INT; };
#endif

	template<> struct GetVariantTypeEnum<ObjectID>{ static constexpr Variant::Type k_value = Variant::Type::INT; };

	template<> struct GetVariantTypeEnum<float>{ static constexpr Variant::Type k_value = Variant::Type::FLOAT; };
	template<> struct GetVariantTypeEnum<double>{ static constexpr Variant::Type k_value = Variant::Type::FLOAT; };

	template<> struct GetVariantTypeEnum<String>{ static constexpr Variant::Type k_value = Variant::Type::STRING; };
	template<> struct GetVariantTypeEnum<StringName>{ static constexpr Variant::Type k_value = Variant::Type::STRING_NAME; };
	template<> struct GetVariantTypeEnum<const char32_t*>{ static constexpr Variant::Type k_value = Variant::Type::STRING; };
	template<> struct GetVariantTypeEnum<const char*>{ static constexpr Variant::Type k_value = Variant::Type::STRING; };

	template<> struct GetVariantTypeEnum<Vector2>{ static constexpr Variant::Type k_value = Variant::Type::VECTOR2; };
	template<> struct GetVariantTypeEnum<Vector2i>{ static constexpr Variant::Type k_value = Variant::Type::VECTOR2I; };
	template<> struct GetVariantTypeEnum<Rect2>{ static constexpr Variant::Type k_value = Variant::Type::RECT2; };
	template<> struct GetVariantTypeEnum<Rect2i>{ static constexpr Variant::Type k_value = Variant::Type::RECT2I; };
	template<> struct GetVariantTypeEnum<Vector3>{ static constexpr Variant::Type k_value = Variant::Type::VECTOR3; };
	template<> struct GetVariantTypeEnum<Vector3i>{ static constexpr Variant::Type k_value = Variant::Type::VECTOR3I; };
	template<> struct GetVariantTypeEnum<Vector4>{ static constexpr Variant::Type k_value = Variant::Type::VECTOR4; };
	template<> struct GetVariantTypeEnum<Vector4i>{ static constexpr Variant::Type k_value = Variant::Type::VECTOR4I; };

	template<> struct GetVariantTypeEnum<Plane>{ static constexpr Variant::Type k_value = Variant::Type::PLANE; };
	template<> struct GetVariantTypeEnum<::AABB>{ static constexpr Variant::Type k_value = Variant::Type::AABB; };
	template<> struct GetVariantTypeEnum<Quaternion>{ static constexpr Variant::Type k_value = Variant::Type::QUATERNION; };
	template<> struct GetVariantTypeEnum<Basis>{ static constexpr Variant::Type k_value = Variant::Type::BASIS; };
	template<> struct GetVariantTypeEnum<Transform2D>{ static constexpr Variant::Type k_value = Variant::Type::TRANSFORM2D; };
	template<> struct GetVariantTypeEnum<Transform3D>{ static constexpr Variant::Type k_value = Variant::Type::TRANSFORM3D; };
	template<> struct GetVariantTypeEnum<Projection>{ static constexpr Variant::Type k_value = Variant::Type::PROJECTION; };
	template<> struct GetVariantTypeEnum<Color>{ static constexpr Variant::Type k_value = Variant::Type::COLOR; };

	template<> struct GetVariantTypeEnum<NodePath>{ static constexpr Variant::Type k_value = Variant::Type::NODE_PATH; };
	template<> struct GetVariantTypeEnum<RID>{ static constexpr Variant::Type k_value = Variant::Type::RID; };

	template<> struct GetVariantTypeEnum<Object*>{ static constexpr Variant::Type k_value = Variant::Type::OBJECT; };
	template<> struct GetVariantTypeEnum<Callable>{ static constexpr Variant::Type k_value = Variant::Type::CALLABLE; };
	template<> struct GetVariantTypeEnum<Signal>{ static constexpr Variant::Type k_value = Variant::Type::SIGNAL; };

	template<> struct GetVariantTypeEnum<Dictionary>{ static constexpr Variant::Type k_value = Variant::Type::DICTIONARY; };
	template<> struct GetVariantTypeEnum<Array>{ static constexpr Variant::Type k_value = Variant::Type::ARRAY; };

	template<> struct GetVariantTypeEnum<Vector<Variant>>{ static constexpr Variant::Type k_value = Variant::Type::ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<uint8_t>>{ static constexpr Variant::Type k_value = Variant::Type::PACKED_BYTE_ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<int32_t>>{ static constexpr Variant::Type k_value = Variant::Type::PACKED_INT32_ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<int64_t>>{ static constexpr Variant::Type k_value = Variant::Type::PACKED_INT64_ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<float>>{ static constexpr Variant::Type k_value = Variant::Type::PACKED_FLOAT32_ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<double>>{ static constexpr Variant::Type k_value = Variant::Type::PACKED_FLOAT64_ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<String>>{ static constexpr Variant::Type k_value = Variant::Type::PACKED_STRING_ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<StringName>>{ static constexpr Variant::Type k_value = Variant::Type::PACKED_STRING_ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<Vector2>>{ static constexpr Variant::Type k_value = Variant::Type::PACKED_VECTOR2_ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<Vector3>>{ static constexpr Variant::Type k_value = Variant::Type::PACKED_VECTOR3_ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<Color>>{ static constexpr Variant::Type k_value = Variant::Type::PACKED_COLOR_ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<RID>>{ static constexpr Variant::Type k_value = Variant::Type::ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<Plane>>{ static constexpr Variant::Type k_value = Variant::Type::ARRAY; };
	template<> struct GetVariantTypeEnum<Vector<Face3>>{ static constexpr Variant::Type k_value = Variant::Type::PACKED_VECTOR3_ARRAY; };

	template<> struct GetVariantTypeEnum<IPAddress>{  static constexpr Variant::Type k_value = Variant::Type::STRING;  };

#if defined(DEBUG_ENABLED)
	template<> struct GetVariantTypeEnum<StringId>{ static constexpr Variant::Type k_value = Variant::Type::STRING; };
#else
	template<> struct GetVariantTypeEnum<StringId>{ static constexpr Variant::Type k_value = Variant::Type::INT; };
#endif

	inline constexpr bool isMonoVariantType(const Variant::Type _type)
	{
		switch (_type)
		{
			case Variant::Type::BOOL:
			case Variant::Type::INT:
			case Variant::Type::FLOAT:
			case Variant::Type::STRING:
			case Variant::Type::VECTOR2:
			case Variant::Type::VECTOR2I:
			case Variant::Type::RECT2:
			case Variant::Type::RECT2I:
			case Variant::Type::VECTOR3:
			case Variant::Type::VECTOR3I:
			case Variant::Type::TRANSFORM2D:
			case Variant::Type::VECTOR4:
			case Variant::Type::VECTOR4I:
			case Variant::Type::PLANE:
			case Variant::Type::QUATERNION:
			case Variant::Type::AABB:
			case Variant::Type::BASIS:
			case Variant::Type::TRANSFORM3D:
			case Variant::Type::PROJECTION:
			case Variant::Type::COLOR:
			case Variant::Type::STRING_NAME:
			case Variant::Type::NODE_PATH:
			case Variant::Type::RID:
			case Variant::Type::OBJECT:
			case Variant::Type::CALLABLE:
			case Variant::Type::SIGNAL:
				return true;

			case Variant::Type::NIL:
			case Variant::Type::DICTIONARY:
			case Variant::Type::ARRAY:
			case Variant::Type::PACKED_BYTE_ARRAY:
			case Variant::Type::PACKED_INT32_ARRAY:
			case Variant::Type::PACKED_INT64_ARRAY:
			case Variant::Type::PACKED_FLOAT32_ARRAY:
			case Variant::Type::PACKED_FLOAT64_ARRAY:
			case Variant::Type::PACKED_STRING_ARRAY:
			case Variant::Type::PACKED_VECTOR2_ARRAY:
			case Variant::Type::PACKED_VECTOR3_ARRAY:
			case Variant::Type::PACKED_COLOR_ARRAY:
			case Variant::Type::VARIANT_MAX:
				return false;
		}
	}

	inline constexpr bool isContainerVariantType(const Variant::Type _type)
	{
		switch (_type)
		{
			case Variant::Type::DICTIONARY:
			case Variant::Type::ARRAY:
			case Variant::Type::PACKED_BYTE_ARRAY:
			case Variant::Type::PACKED_INT32_ARRAY:
			case Variant::Type::PACKED_INT64_ARRAY:
			case Variant::Type::PACKED_FLOAT32_ARRAY:
			case Variant::Type::PACKED_FLOAT64_ARRAY:
			case Variant::Type::PACKED_STRING_ARRAY:
			case Variant::Type::PACKED_VECTOR2_ARRAY:
			case Variant::Type::PACKED_VECTOR3_ARRAY:
			case Variant::Type::PACKED_COLOR_ARRAY:
				return true;

			case Variant::Type::NIL:
			case Variant::Type::BOOL:
			case Variant::Type::INT:
			case Variant::Type::FLOAT:
			case Variant::Type::STRING:
			case Variant::Type::VECTOR2:
			case Variant::Type::VECTOR2I:
			case Variant::Type::RECT2:
			case Variant::Type::RECT2I:
			case Variant::Type::VECTOR3:
			case Variant::Type::VECTOR3I:
			case Variant::Type::TRANSFORM2D:
			case Variant::Type::VECTOR4:
			case Variant::Type::VECTOR4I:
			case Variant::Type::PLANE:
			case Variant::Type::QUATERNION:
			case Variant::Type::AABB:
			case Variant::Type::BASIS:
			case Variant::Type::TRANSFORM3D:
			case Variant::Type::PROJECTION:
			case Variant::Type::COLOR:
			case Variant::Type::STRING_NAME:
			case Variant::Type::NODE_PATH:
			case Variant::Type::RID:
			case Variant::Type::OBJECT:
			case Variant::Type::CALLABLE:
			case Variant::Type::SIGNAL:
			case Variant::Type::VARIANT_MAX:
				return false;
		}
	}

	template<typename TypeT>
	struct VariantTraits
	{
		using DecayedType = typename detail::VariantTypeDecay<TypeT>::Type;

		static constexpr Variant::Type k_type = GetVariantTypeEnum<DecayedType>::k_value;
		static constexpr bool k_mono = isMonoVariantType(k_type);
		static constexpr bool k_container = isContainerVariantType(k_type);
		
		using StorageType = typename GetVariantStorage<k_type>::Type;

	};

	template<typename TypeT, typename DefaultTypeT, typename=void>
	struct TryVariantTraits
	{
		using Type = VariantTraits<DefaultTypeT>;
	};

	template<typename TypeT, typename DefaultTypeT>
	struct TryVariantTraits<
		TypeT,
		DefaultTypeT,
		std::enable_if_t<std::is_same_v<
			decltype(VariantTraits<TypeT>::k_type),
			Variant::Type
		>>
	>
	{
		using Type = VariantTraits<TypeT>;
	};

	template<typename TypeT>
	using DecayedVariantTraits = VariantTraits<typename detail::VariantTypeDecay<TypeT>::Type>;

	template<typename TypeT, typename DefaultTypeT>
	using DecayedTryVariantTraits = typename TryVariantTraits<typename detail::VariantTypeDecay<TypeT>::Type, typename detail::VariantTypeDecay<DefaultTypeT>>::Type;

	// The actual variant has function like that, but it's function returns String object, 
	// which leads to allocation of memory, and this function simply return static const char*.
	inline constexpr const char* getVariantTypeName(const Variant::Type type)
	{
		switch (type) 
		{
			case Variant::Type::NIL: return "Nil";
			// Atomic types.
			case Variant::Type::BOOL: return "bool";
			case Variant::Type::INT: return "int";
			case Variant::Type::FLOAT: return "float";
			case Variant::Type::STRING: return "String";
			// Math types.
			case Variant::Type::VECTOR2: return "Vector2";
			case Variant::Type::VECTOR2I: return "Vector2i";
			case Variant::Type::RECT2: return "Rect2";
			case Variant::Type::RECT2I: return "Rect2i";
			case Variant::Type::TRANSFORM2D: return "Transform2D";
			case Variant::Type::VECTOR3: return "Vector3";
			case Variant::Type::VECTOR3I: return "Vector3i";
			case Variant::Type::VECTOR4: return "Vector4";
			case Variant::Type::VECTOR4I: return "Vector4i";
			case Variant::Type::PLANE: return "Plane";
			case Variant::Type::AABB: return "AABB";
			case Variant::Type::QUATERNION: return "Quaternion";
			case Variant::Type::BASIS: return "Basis";
			case Variant::Type::TRANSFORM3D: return "Transform3D";
			case Variant::Type::PROJECTION: return "Projection";
			// Miscellaneous types.
			case Variant::Type::COLOR: return "Color";
			case Variant::Type::RID: return "RID";
			case Variant::Type::OBJECT: return "Object";
			case Variant::Type::CALLABLE: return "Callable";
			case Variant::Type::SIGNAL: return "Signal";
			case Variant::Type::STRING_NAME: return "StringName";
			case Variant::Type::NODE_PATH: return "NodePath";
			case Variant::Type::DICTIONARY: return "Dictionary";
			case Variant::Type::ARRAY: return "Array";
			// Arrays.
			case Variant::Type::PACKED_BYTE_ARRAY: return "PackedByteArray";
			case Variant::Type::PACKED_INT32_ARRAY: return "PackedInt32Array";
			case Variant::Type::PACKED_INT64_ARRAY: return "PackedInt64Array";
			case Variant::Type::PACKED_FLOAT32_ARRAY: return "PackedFloat32Array";
			case Variant::Type::PACKED_FLOAT64_ARRAY: return "PackedFloat64Array";
			case Variant::Type::PACKED_STRING_ARRAY: return "PackedStringArray";
			case Variant::Type::PACKED_VECTOR2_ARRAY: return "PackedVector2Array";
			case Variant::Type::PACKED_VECTOR3_ARRAY: return "PackedVector3Array";
			case Variant::Type::PACKED_COLOR_ARRAY: return "PackedColorArray";
			default: break;
		}

		return "";
	}

	Variant convertVariant(const Variant& _input, const Variant::Type _type);
} // namespace bwn