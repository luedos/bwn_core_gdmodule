#pragma once

#include "bwn_core/types/stringView.hpp"

#include <core/templates/list.h>

class Node;

namespace bwn
{
	// Tries to find scene node by simple node name (name could be partial). Returns first match. The search is recursive.
	Node* findSceneNodeByName(const bwn::string_view& _mask);
	// Tries to find scene node by node path (end of the path could be partial, but begin and middle should be exact names).
	Node* findSceneNodeByPath(const bwn::string_view& _path, const String& _typeHint = String());

#if defined(TOOLS_ENABLED)
	bool isEditor();
#else // TOOLS_ENABLED
	static constexpr bool isEditor()
	{
		return false;
	}
#endif // TOOLS_ENABLED

	// Returns true if specific command line argument exist in command line.
	bool hasCommandArgument(const List<String>& _argumentList, const bwn::string_view& _argument);
	// Returns the value of specific command line argument. If argument doesn't exist, or has no value, returns empty string.
	bwn::string_view getCommandArgument(const List<String>& _argumentList, const bwn::string_view& _argument);

} // namespace bwn