#pragma once

#include "bwn_core/types/uniqueRef.hpp"
#include "bwn_core/utility/templateAlgorithms.hpp"

#include <scene/gui/control.h>

#include <type_traits>

namespace bwn
{
	template<typename ControlT, typename...ConstructorStackT>
	class GuiConstructor;

	template<typename ControlT, typename OriginalConstructorT>
	class GuiCustomConstructor
	{
	};

	template<typename ControlT, typename OriginalConstructorT>
	class GuiConstructorBase : public GuiCustomConstructor<ControlT, OriginalConstructorT>
	{
		//
		// Construction and destruction.
		//
	public:
		template<typename...ArgTs>
		_FORCE_INLINE_ GuiConstructorBase(ArgTs&&..._constructionArgs)
			: m_control( memnew(ControlT(std::forward<ArgTs>(_constructionArgs)...)) )
		{}
		
		_FORCE_INLINE_ GuiConstructorBase(ControlT& _control)
			: m_control( &_control )
		{}

		_FORCE_INLINE_ ~GuiConstructorBase() = default;

		//
		// Public interface.
		//
	public:
		// Directly provides object for the visitor to setup.
		template<typename VisitorT>
		_FORCE_INLINE_ OriginalConstructorT& visit(VisitorT&& _visitor)
		{
			_visitor(*m_control);
			return static_cast<OriginalConstructorT&>(*this);
		}

		// Adds child constructor.
		template<typename ChildControlT, typename...ArgTs>
		_FORCE_INLINE_ GuiConstructor<ChildControlT, OriginalConstructorT> create_child(ArgTs&&..._constructionArgs)
		{
			GuiConstructor<ChildControlT, OriginalConstructorT> childConstructor(static_cast<OriginalConstructorT&>(*this), std::forward<ArgTs>(_constructionArgs)...);
			childConstructor.visit([this](Node& _child){ this->m_control->add_child(&_child); });
			return childConstructor;
		}

		// Adds child constructor.
		template<typename ChildControlT, typename ChildControlPtrT, typename...ArgTs>
		_FORCE_INLINE_ GuiConstructor<ChildControlT, OriginalConstructorT> create_child_into(ChildControlPtrT*& _child, ArgTs&&..._constructionArgs)
		{
			GuiConstructor<ChildControlT, OriginalConstructorT> childConstructor(static_cast<OriginalConstructorT&>(*this), std::forward<ArgTs>(_constructionArgs)...);
			childConstructor.visit([this](Node& _child){ this->m_control->add_child(&_child); });
			childConstructor.retrieve_ptr(_child);
			return childConstructor;
		}

		// Adds child constructor.
		template<typename ChildControlT, typename...ArgTs>
		_FORCE_INLINE_ GuiConstructor<ChildControlT, OriginalConstructorT> create_named_child(const String& _name, ArgTs&&..._constructionArgs)
		{
			GuiConstructor<ChildControlT, OriginalConstructorT> childConstructor(static_cast<OriginalConstructorT&>(*this), std::forward<ArgTs>(_constructionArgs)...);
			childConstructor.visit([this,&_name](Node& _child){ _child.set_name(_name); this->m_control->add_child(&_child); });
			return childConstructor;
		}

		// Adds child constructor.
		template<typename ChildControlT, typename ChildControlPtrT, typename...ArgTs>
		_FORCE_INLINE_ GuiConstructor<ChildControlT, OriginalConstructorT> create_named_child_into(ChildControlPtrT*& _child, const String& _name, ArgTs&&..._constructionArgs)
		{
			GuiConstructor<ChildControlT, OriginalConstructorT> childConstructor(static_cast<OriginalConstructorT&>(*this), std::forward<ArgTs>(_constructionArgs)...);
			childConstructor.visit([this,&_name](Node& _child){ _child.set_name(_name); this->m_control->add_child(&_child); });
			childConstructor.retrieve_ptr(_child);
			return childConstructor;
		}

		// Adds child constructor.
		template<typename ChildControlT>
		_FORCE_INLINE_ GuiConstructor<ChildControlT, OriginalConstructorT> add_child(ChildControlT& _child)
		{
			GuiConstructor<ChildControlT, OriginalConstructorT> childConstructor(static_cast<OriginalConstructorT&>(*this), _child);
			m_control->add_child(&_child);
			return childConstructor;
		}

		// Adds child constructor.
		template<typename ChildControlT, typename CreatorT>
		_FORCE_INLINE_ OriginalConstructorT& create_children(const size_t _childrenCount, CreatorT&& _creator)
		{
			for (size_t childIndex = 0; childIndex < _childrenCount; ++childIndex)
			{
				GuiConstructor<ChildControlT> childConstructor;
				childConstructor.visit([this](Node& _child){ this->m_control->add_child(&_child); });
				if constexpr (std::is_invocable_v<bwn::callable_signature_type_t<std::remove_reference_t<CreatorT>>, GuiConstructor<ChildControlT>&, size_t>)
				{
					_creator(childConstructor, childIndex);
				}
				else
				{
					_creator(childConstructor);
				}
			}
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_size(const Size2& _size, bool _keepOffsets = false)
		{
			m_control->set_size(_size, _keepOffsets);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_size(const float _x, const float _y, bool _keepOffsets = false)
		{
			m_control->set_size(Size2(_x, _y), _keepOffsets);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_custom_minimum_size(const Size2& _size)
		{
			m_control->set_custom_minimum_size(_size);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_position(const Point2& _pos, bool _keepOffsets = false)
		{
			m_control->set_position(_pos, _keepOffsets);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_position(const float _x, const float _y, bool _keepOffsets = false)
		{
			m_control->set_position(Point2(_x, _y), _keepOffsets);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_rect(const Rect2 &_rect)
		{
			m_control->set_rect(_rect);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_h_grow_direction(const Control::GrowDirection _direction)
		{
			m_control->set_h_grow_direction(_direction);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_v_grow_direction(const Control::GrowDirection _direction)
		{
			m_control->set_v_grow_direction(_direction);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_anchors_preset(const Control::LayoutPreset _preset, const bool _keepOffsets = true)
		{
			m_control->set_anchors_preset(_preset, _keepOffsets);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_offsets_preset(const Control::LayoutPreset _preset, const Control::LayoutPresetMode _resizeMode = Control::PRESET_MODE_MINSIZE, const int _margin = 0)
		{
			m_control->set_offsets_preset(_preset, _resizeMode, _margin);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_h_size_flags(BitField<Control::SizeFlags> _flags)
		{
			m_control->set_h_size_flags(_flags);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_v_size_flags(BitField<Control::SizeFlags> _flags)
		{
			m_control->set_v_size_flags(_flags);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_anchors_and_offsets_preset(const Control::LayoutPreset _preset, const Control::LayoutPresetMode _resizeMode = Control::PRESET_MODE_MINSIZE, const int _margin = 0)
		{
			m_control->set_anchors_and_offsets_preset(_preset, _resizeMode, _margin);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_grow_direction_preset(const Control::LayoutPreset _preset)
		{
			m_control->set_grow_direction_preset(_preset);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& connect(const StringName& _signal, const Callable& _callable, const uint32_t _flags = 0)
		{
			m_control->connect(_signal, _callable, _flags);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& add_theme_icon_override(const StringName& _name, const Ref<Texture2D>& _icon)
		{
			m_control->add_theme_icon_override(_name, _icon);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& add_theme_style_override(const StringName& _name, const Ref<StyleBox>& _style)
		{
			m_control->add_theme_style_override(_name, _style);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& add_theme_font_override(const StringName& _name, const Ref<Font>& _font)
		{
			m_control->add_theme_font_override(_name, _font);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& add_theme_font_size_override(const StringName& _name, const int _fontSize)
		{
			m_control->add_theme_font_size_override(_name, _fontSize);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& add_theme_color_override(const StringName& _name, const Color& _color)
		{
			m_control->add_theme_color_override(_name, _color);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& add_theme_constant_override(const StringName& _name, const int _constant)
		{
			m_control->add_theme_constant_override(_name, _constant);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_clip_contents(const bool _clip)
		{
			m_control->set_clip_contents(_clip);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_mouse_filter(const Control::MouseFilter _filter)
		{
			m_control->set_mouse_filter(_filter);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_focus_mode(const Control::FocusMode _mode)
		{
			m_control->set_focus_mode(_mode);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_offset(const Side _side, const float _value)
		{
			m_control->set_offset(_side, _value);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_flat(const bool _flat)
		{
			m_control->set_flat(_flat);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& hide()
		{
			m_control->set_visible(false);
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ OriginalConstructorT& set_tooltip_text(const String& _text)
		{
			m_control->set_tooltip_text(_text);
			return static_cast<OriginalConstructorT&>(*this);
		}

		template<typename ControlPtrT>
		_FORCE_INLINE_ OriginalConstructorT& retrieve_ptr(ControlPtrT*& _ptr)
		{
			_ptr = m_control;
			return static_cast<OriginalConstructorT&>(*this);
		}

		_FORCE_INLINE_ ControlT* release()
		{
			return m_control;
		}

		//
		// Protected members.
		//
	protected:
		ControlT* m_control;
	};

	template<typename ControlT>
	class GuiConstructor<ControlT> : public GuiConstructorBase<ControlT, GuiConstructor<ControlT>>
	{
		//
		// Construction and destruction.
		//
	public:
		using GuiConstructorBase<ControlT, GuiConstructor<ControlT>>::GuiConstructorBase;
	};

	template<typename ControlT, typename...ConstructorStackT>
	class GuiConstructor : public GuiConstructorBase<ControlT, GuiConstructor<ControlT, ConstructorStackT...>>
	{
		//
		// Public typedefs.
		//
	public:
		using ParentConstructorType = bwn::variadic_template_first_t<ConstructorStackT...>;

		//
		// Construction and destruction.
		//
	public:
		template<typename...ArgTs>
		_FORCE_INLINE_ GuiConstructor(ParentConstructorType& _parent, ArgTs&&..._constructionArgs)
			: GuiConstructorBase<ControlT, GuiConstructor<ControlT, ConstructorStackT...>>(std::forward<ArgTs>(_constructionArgs)...)
			, m_parent( _parent )
		{}

		_FORCE_INLINE_ GuiConstructor(ParentConstructorType& _parent, ControlT& _control)
			: GuiConstructorBase<ControlT, GuiConstructor<ControlT, ConstructorStackT...>>(_control)
			, m_parent( _parent )
		{}
		_FORCE_INLINE_ ~GuiConstructor() = default;

		//
		// Public interface.
		//
	public:
		_FORCE_INLINE_ ParentConstructorType& end()
		{
			return m_parent;
		}

		//
		// Private members.
		//
	private:
		ParentConstructorType& m_parent;
	};

	template<typename ControlT, typename...ArgTs>
	_FORCE_INLINE_ GuiConstructor<ControlT> createGui(ArgTs&&..._constructionArgs)
	{
		return GuiConstructor<ControlT>(std::forward<ArgTs>(_constructionArgs)...);
	}

	template<typename ControlT>
	_FORCE_INLINE_ GuiConstructor<ControlT> modifyGui(ControlT& _control)
	{
		return GuiConstructor<ControlT>(_control);
	}

} // namespace bwn

class Label;

namespace bwn
{
	template<typename OriginalConstructorT>
	class GuiCustomConstructor<Label, OriginalConstructorT>
	{
		//
		// Public interface.
		//
	public:
		OriginalConstructorT& set_text(const String& _text)
		{
			OriginalConstructorT& self = static_cast<OriginalConstructorT&>(*this);
			self.visit([&_text](auto& _label){ _label.set_text(_text); });
			return self;
		}
	};

	template<typename OriginalConstructorT>
	class GuiCustomConstructor<Button, OriginalConstructorT>
	{
		//
		// Public interface.
		//
	public:
		OriginalConstructorT& set_text(const String& _text)
		{
			OriginalConstructorT& self = static_cast<OriginalConstructorT&>(*this);
			self.visit([&_text](auto& _button){ _button.set_text(_text); });
			return self;
		}

		OriginalConstructorT& set_icon(const Ref<Texture2D>& _icon)
		{
			OriginalConstructorT& self = static_cast<OriginalConstructorT&>(*this);
			self.visit([&_icon](auto& _button){ _button.set_icon(_icon); });
			return self;
		}
	};
} // namespace bwn