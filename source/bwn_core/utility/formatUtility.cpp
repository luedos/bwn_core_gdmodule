#include "precompiled_core.hpp"

#include "bwn_core/utility/formatUtility.hpp"

String str_format(const char *_format, ...) {
	va_list list;

	va_start(list, _format);
	String res = str_format(_format, list);
	va_end(list);

	return res;
}
// va_copy was defined in the C99, but not in C++ standards before C++11.
// When you compile C++ without --std=c++<XX> option, compilers still define
// va_copy, otherwise you have to use the internal version (__va_copy).
#if !defined(va_copy)
	#if defined(__GNUC__)
		#define va_copy(d, s) __va_copy((d), (s))
	#else
		#define va_copy(d, s) ((d) = (s))
	#endif
#endif

#if defined(MINGW_ENABLED)
	#define gd_vsnprintf(m_buffer, m_count, m_format, m_args_copy) vsnprintf_s(m_buffer, m_count, _TRUNCATE, m_format, m_args_copy)
	#define gd_vscprintf(m_format, m_args_copy) _vscprintf(m_format, m_args_copy)
#else
	#define gd_vsnprintf(m_buffer, m_count, m_format, m_args_copy) vsnprintf(m_buffer, m_count, m_format, m_args_copy)
	#define gd_vscprintf(m_format, m_args_copy) vsnprintf(NULL, 0, _format, m_args_copy)
#endif

String str_format(const char *_format, va_list _list) 
{
	char *buffer = str_format_new(_format, _list);

	String res(buffer);
	memdelete_arr(buffer);

	return res;
}

char *str_format_new(const char *_format, ...) 
{
	va_list list;

	va_start(list, _format);
	char *res = str_format_new(_format, list);
	va_end(list);

	return res;
}

char *str_format_new(const char *_format, va_list _list) 
{
	va_list list;

	va_copy(list, _list);
	int len = gd_vscprintf(_format, list);
	va_end(list);

	len += 1; // for the trailing '/0'

	char *buffer(memnew_arr(char, len));

	va_copy(list, _list);
	gd_vsnprintf(buffer, len, _format, list);
	va_end(list);

	return buffer;
}