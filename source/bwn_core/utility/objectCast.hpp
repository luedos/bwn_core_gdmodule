#pragma once

#include <core/object/ref_counted.h>

#include <type_traits>

namespace bwn
{
	template<typename ToT, typename FromT>
	ToT* objectCast(FromT*const _objectPtr)
	{
		static_assert(std::is_base_of_v<ToT, FromT> || std::is_base_of_v<FromT, ToT>, "Casted types should exist in the same hierarchy.");

		if constexpr (std::is_base_of_v<ToT, FromT>)
		{
			return _objectPtr;
		}
		else
		{
			return Object::cast_to<ToT>(_objectPtr);
		}
	}

	template<typename ToT, typename FromT>
	const ToT* objectCast(const FromT*const _objectPtr)
	{
		static_assert(std::is_base_of_v<ToT, FromT> || std::is_base_of_v<FromT, ToT>, "Casted types should exist in the same hierarchy.");

		if constexpr (std::is_base_of_v<ToT, FromT>)
		{
			return _objectPtr;
		}
		else
		{
			return Object::cast_to<ToT>(_objectPtr);
		}
	}

	template<typename ToT, typename FromT>
	Ref<ToT> objectCast(Ref<FromT> _reference)
	{
		static_assert(std::is_base_of_v<ToT, FromT> || std::is_base_of_v<FromT, ToT>, "Casted types should exist in the same hierarchy.");

		ToT*const castedObject = Object::cast_to<ToT>(_reference.ptr());
		return Ref<ToT>(castedObject);
	}
} // namespace bwn