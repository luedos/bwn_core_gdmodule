#pragma once

#include <type_traits>

namespace bwn::detail
{
	template<typename TypeT>
	using BetterPropertyType = std::conditional_t<std::is_trivially_copyable_v<TypeT>, TypeT, const TypeT&>;
} // namespace bwn::detail

#define BWN_DECLARE_PROPERTY(_propertyType, _propertyMember) \
	void _propertyMember ## _property_setter(::bwn::detail::BetterPropertyType<_propertyType> _value); \
	::bwn::detail::BetterPropertyType<_propertyType> _propertyMember ## _property_getter() const; \
	_propertyType _propertyMember

#define BWN_DEFINE_PROPERTY(_class, _propertyMember) \
	void _class :: _propertyMember ## _property_setter(::bwn::detail::BetterPropertyType<decltype(_class :: _propertyMember)> _value) { _propertyMember = _value; } \
	::bwn::detail::BetterPropertyType<decltype(_class :: _propertyMember)> _class :: _propertyMember ## _property_getter() const { return _propertyMember; }

#define BWN_BIND_DECLARED_PROPERTY(_class, _propertyName, _propertyMember, ...) \
	BWN_BIND_PROPERTY(_propertyName, & _class :: _propertyMember ## _property_setter, & _class :: _propertyMember ## _property_getter ,##__VA_ARGS__)