#include "precompiled_core.hpp"

#include "bwn_core/utility/engineUtility.hpp"
#include "bwn_core/utility/nodeUtility.hpp"

#include <core/config/engine.h>

Node* bwn::findSceneNodeByName(const bwn::string_view& _mask)
{
	if (_mask.empty())
	{
		return nullptr;
	}

	SceneTree*const sceneTree = SceneTree::get_singleton();
	Node*const rootNode = sceneTree->get_current_scene();
	if (rootNode == nullptr)
	{
		return nullptr;
	}

	{
		const String rootName = rootNode->get_name();
		const bwn::string_view rootNameView = bwn::wrap_view(rootName);
		if (rootNameView.find(_mask) != bwn::string_view::npos)
		{
			return rootNode;
		}
	}

	return findDescendantByPartialName(*rootNode, _mask);
}

Node* bwn::findSceneNodeByPath(const bwn::string_view& _path, const String& _typeHint)
{
	if (_path.empty())
	{
		return nullptr;
	}

	SceneTree*const sceneTree = SceneTree::get_singleton();
	Node*const rootNode = sceneTree->get_current_scene();
	if (rootNode == nullptr)
	{
		return nullptr;
	}

	const bool isRootPath = _path.front() == '\\' || _path.front() == '/';
	const bwn::string_view relativePath = _path.substr(size_t(isRootPath));

	{
		const auto [firstNodeName, restPath] = splitPathOnBegin(relativePath);

		const String rootName = rootNode->get_name();
		const bwn::string_view rootNameView = bwn::wrap_view(rootName);

		// First of all check the corner case where path only has one node and this is a root node.
		if (!firstNodeName.empty()
			&& restPath.empty()
			&& (_typeHint.is_empty() || rootNode->is_class(_typeHint))
			&& rootNameView.find(firstNodeName) != bwn::string_view::npos)
		{
			return rootNode;
		}

		const bool possiblyRootPath = isRootPath || rootNameView == firstNodeName;
		// Even if this is not the actually root path, we are prioritise root paths,
		// so we first of all will check all paths starting from the root.
		if (possiblyRootPath)
		{
			Node*const foundNode = findChildByPartialPath(*rootNode, restPath);
			if (foundNode != nullptr || isRootPath)
			{
				// If node was found, then we returning it because we are prioritising paths from root.
				// And even if it's nullptr, but we are operating on root path, we will force the result.
				return foundNode;
			}
		}
	}

	return findDescendantByPartialPath(*rootNode, relativePath, _typeHint);
}

#if defined(TOOLS_ENABLED)

bool bwn::isEditor()
{
	Engine*const engine = Engine::get_singleton();
	return engine->is_editor_hint();
}

#endif // TOOLS_ENABLED

bool bwn::hasCommandArgument(const List<String>& _argumentList, const bwn::string_view& _argument)
{
	for (const List<String>::Element* elementIt = _argumentList.front(); elementIt != nullptr; elementIt = elementIt->next())
	{
		const bwn::string_view argumentView = bwn::wrap_view(elementIt->get());
		if (argumentView.find(_argument) != 0)
		{
			continue;
		}

		if (argumentView.length() == _argument.length() || argumentView[_argument.length()] == L'=')
		{
			return true;
		}
	}

	return false;
}

bwn::string_view bwn::getCommandArgument(const List<String>& _argumentList, const bwn::string_view& _argument)
{
	for (const List<String>::Element* elementIt = _argumentList.front(); elementIt != nullptr; elementIt = elementIt->next())
	{
		const bwn::string_view argumentView = bwn::wrap_view(elementIt->get());
		if (argumentView.find(_argument) != 0)
		{
			continue;
		}

		if (argumentView.length() == _argument.length() || argumentView[_argument.length()] != L'=')
		{
			continue;
		}

		return argumentView.substr(_argument.length() + 1);
	}

	return bwn::string_view();
}