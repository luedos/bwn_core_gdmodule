#pragma once

#include <core/object/ref_counted.h>
#include <core/os/memory.h>

#include <type_traits>

namespace bwn
{

template<typename ObjT, typename...ArgsTs>
_FORCE_INLINE_ typename std::enable_if<
	// Basically we are not allowing the creation of anything not derived from Reference class. 
	std::is_base_of<RefCounted, ObjT>::value, 
	Ref<ObjT> >::type 
instantiateRef(ArgsTs&&..._args)
{
	return Ref<ObjT>(memnew(ObjT(std::forward<ArgsTs>(_args)...)));
}

} // namespace bwn