#pragma once

#include "bwn_core/utility/variantUtility.hpp"

#include <core/object/object.h>
#include <core/string/ustring.h>

namespace bwn
{
	namespace detail
	{
		template<typename TypeT, typename=void>
		struct StaticClassNameUtility
		{
			static String getClassName() { return ""; }
		};

		template<typename TypeT>
		struct StaticClassNameUtility<TypeT, std::void_t<decltype(TypeT::get_class_static())>>
		{
			static String getClassName() { return TypeT::get_class_static(); }
		};

		template<typename TypeT>
		struct StaticClassNameUtility<Ref<TypeT>, void>
		{
			static String getClassName() { return StaticClassNameUtility<TypeT>::getClassName(); }
		};
	} // namespace detail


	template<typename TypeT>
	struct MethodUtility;

	template<typename RetT, typename...ArgTs>
	struct MethodUtility<RetT(ArgTs...)>
	{
		//
		// Construction and destruction.
		//
	public:
		explicit MethodUtility(const String& _name);

		//
		// Public interface.
		//
	public:
		// Seting the name of the return type.
		MethodUtility& retName(const String& _name);
		// Seting the hint of the return type.
		MethodUtility& retHint(const PropertyHint _hint);
		// Seting the hint string of the return type.
		MethodUtility& retHint(const String& _hint);
		// Seting the usage of the return type.
		MethodUtility& retUsage(const uint32_t _usageId);
		// Seting the name of the specific argument type.
		template<uint32_t ArgV>
		MethodUtility& argName(const String& _name);
		// Seting the hint of the specific argument type.
		template<uint32_t ArgV>
		MethodUtility& argHint(const PropertyHint _hint);
		// Seting the hint string of the specific argument type.
		template<uint32_t ArgV>
		MethodUtility& argHint(const String& _hint);
		// Seting the usage of the specific argument type.
		template<uint32_t ArgV>
		MethodUtility& argUsage(const uint32_t _usageId);
		// Creates method info from current data.
		MethodInfo get() const;

		//
		// Private members.
		//
	private:
		std::array<PropertyInfo, sizeof...(ArgTs)> m_args;
		PropertyInfo m_ret;
		String m_name;
	};

	namespace detail
	{
		// This is just simple helper class, to initiate function args.
		template<uint32_t ArgV, typename TupleT, typename...TypeTs>
		struct InitArgsTupleHelper;

		template<uint32_t ArgV, typename TupleT, typename FirstT, typename...RestTs>
		struct InitArgsTupleHelper<ArgV, TupleT, FirstT, RestTs...>
		{
			static void init(TupleT& _tuple)
			{
				std::get<ArgV>(_tuple).class_name = detail::StaticClassNameUtility<FirstT>::getClassName();
				std::get<ArgV>(_tuple).type = DecayedVariantTraits<FirstT>::k_type;

				InitArgsTupleHelper<ArgV + 1, TupleT, RestTs...>::init(_tuple);
			}
		};

		template<uint32_t ArgV, typename TupleT>
		struct InitArgsTupleHelper<ArgV, TupleT>
		{
			static void init(TupleT&)
			{}
		};
	} // namespace detail

	template<typename RetT, typename...ArgTs>
	MethodUtility<RetT(ArgTs...)>::MethodUtility(const String& _name)
		: m_name{ _name }
	{
		m_ret.class_name = detail::StaticClassNameUtility<RetT>::getClassName();
		m_ret.type = DecayedVariantTraits<RetT>::k_type;

		detail::InitArgsTupleHelper<0, decltype(m_args), ArgTs...>::init(m_args);
	}

	template<typename RetT, typename...ArgTs>
	MethodUtility<RetT(ArgTs...)>& MethodUtility<RetT(ArgTs...)>::retName(const String& _name)
	{
		m_ret.name = _name;
		return *this;
	}

	template<typename RetT, typename...ArgTs>
	MethodUtility<RetT(ArgTs...)>& MethodUtility<RetT(ArgTs...)>::retHint(const PropertyHint _hint)
	{
		m_ret.hint = _hint;
		return *this;
	}

	template<typename RetT, typename...ArgTs>
	MethodUtility<RetT(ArgTs...)>& MethodUtility<RetT(ArgTs...)>::retHint(const String& _hint)
	{
		m_ret.hint_string = _hint;
		return *this;
	}

	template<typename RetT, typename...ArgTs>
	MethodUtility<RetT(ArgTs...)>& MethodUtility<RetT(ArgTs...)>::retUsage(const uint32_t _usageId)
	{
		m_ret.usage = _usageId;
		return *this;
	}

	template<typename RetT, typename...ArgTs>
	template<uint32_t ArgV>
	MethodUtility<RetT(ArgTs...)>& MethodUtility<RetT(ArgTs...)>::argName(const String& _name)
	{
		std::get<ArgV>(m_args).name = _name;
		return *this;
	}

	template<typename RetT, typename...ArgTs>
	template<uint32_t ArgV>
	MethodUtility<RetT(ArgTs...)>& MethodUtility<RetT(ArgTs...)>::argHint(const PropertyHint _hint)
	{
		std::get<ArgV>(m_args).hint = _hint;
		return *this;
	}

	template<typename RetT, typename...ArgTs>
	template<uint32_t ArgV>
	MethodUtility<RetT(ArgTs...)>& MethodUtility<RetT(ArgTs...)>::argHint(const String& _hint)
	{
		std::get<ArgV>(m_args).hint_string = _hint;
		return *this;
	}

	template<typename RetT, typename...ArgTs>
	template<uint32_t ArgV>
	MethodUtility<RetT(ArgTs...)>& MethodUtility<RetT(ArgTs...)>::argUsage(const uint32_t _usageId)
	{
		std::get<ArgV>(m_args).usage = _usageId;
		return *this;
	}

	template<typename RetT, typename...ArgTs>
	MethodInfo MethodUtility<RetT(ArgTs...)>::get() const
	{
		return std::apply(
			[this](auto&&..._args) -> MethodInfo { 
				return MethodInfo(this->m_ret, this->m_name, _args...);
			},
			this->m_args);
	}
} // namespace detail