#pragma once

#include "bwn_core/utility/variantUtility.hpp"
#include "bwn_core/utility/visitorUtility.hpp"

#include <core/variant/dictionary.h>
#include <core/templates/vmap.h>
#include <core/variant/variant.h>

#include <map>
#include <unordered_map>
#include <vector>
#include <type_traits>
#include <utility>

namespace bwn
{
	template<typename ValueT>
	struct VariantConverter;

	template<typename VectorT>
	struct ArrayConverter
	{
		static Array createArray(const VectorT& _vector)
		{
			Array returnArray;
			returnArray.resize(static_cast<int>(_vector.size()));

			int currentIndex = 0;
			for (const auto& value : _vector)
			{
				using ValueType = std::remove_reference_t<std::remove_cv_t<decltype(value)>>;
				
				const Variant valueVariant = VariantConverter<ValueType>::toVariant(value);
				returnArray.set(currentIndex, valueVariant);

				++currentIndex;
			}

			return returnArray;
		}

		static bool parseArrayTry(const Array& _array, VectorT& o_vector)
		{
			const int arraySize = _array.size();
			o_vector.reserve(arraySize);
			for (int arrayItemIndex = 0; arrayItemIndex < arraySize; ++arrayItemIndex)
			{
				using ValueType = std::remove_reference_t<std::remove_cv_t<decltype(o_vector[0])>>;
				
				ValueType vectorItem;
				if (!VariantConverter<ValueType>::fromVariantTry(_array.get(arrayItemIndex), vectorItem))
				{
					o_vector.clear();
					return false;
				}

				o_vector.push_back(std::move(vectorItem));
			}

			return true;
		}

		static bool parseArray(const Array& _array, VectorT& o_vector)
		{
			const int arraySize = _array.size();
			o_vector.reserve(arraySize);
			for (int arrayItemIndex = 0; arrayItemIndex < arraySize; ++arrayItemIndex)
			{
				using ValueType = std::remove_reference_t<std::remove_cv_t<decltype(o_vector[0])>>;
				
				ValueType vectorItem;
				if (VariantConverter<ValueType>::fromVariant(_array.get(arrayItemIndex), vectorItem))
				{
					o_vector.push_back(std::move(vectorItem));
				}
			}

			return true;
		}

		template<typename VisitorT>
		static EVisitorResult parseArrayVisitor(const Array& _array, VisitorT&& _visitor)
		{
			const int arraySize = _array.size();
			for (int arrayItemIndex = 0; arrayItemIndex < arraySize; ++arrayItemIndex)
			{
				using ValueType = std::remove_reference_t<std::remove_cv_t<decltype(std::declval<VectorT>()[0])>>;
				
				ValueType vectorItem;
				if (VariantConverter<ValueType>::fromVariant(_array.get(arrayItemIndex), vectorItem))
				{
					if (callVisitor(_visitor, vectorItem) == EVisitorResult::k_break)
					{
						return EVisitorResult::k_break;
					}
				}
			}

			return EVisitorResult::k_continue;
		}
	};

	namespace detail
	{
		template<typename MapT, typename KeyT, typename ValueT>
		struct GeneralDictionaryConverter
		{
			static Dictionary createDictionary(const MapT& _map)
			{
				Dictionary returnDictionary;
				for (const auto& [key, value] : _map)
				{
					const Variant keyVariant = VariantConverter<KeyT>::toVariant(key);
					const Variant valueVariant = VariantConverter<ValueT>::toVariant(value);
					returnDictionary[keyVariant] = valueVariant;
				}

				return returnDictionary;
			}

			static bool parseDictionaryTry(const Dictionary& _dictionary, MapT& o_map)
			{
				const int dictSize = _dictionary.size();
				for (int dictIndex = 0; dictIndex < dictSize; ++dictIndex)
				{
					const Variant keyVariant = _dictionary.get_key_at_index(dictIndex);
					KeyT key;
					if (!VariantConverter<KeyT>::fromVariantTry(keyVariant, key))
					{
						o_map.clear();
						return false;
					}

					const Variant valueVariant = _dictionary.get_value_at_index(dictIndex);
					ValueT value;
					if (!VariantConverter<ValueT>::fromVariantTry(valueVariant, value))
					{
						o_map.clear();
						return false;
					}

					o_map[key] = std::move(value);
				}

				return true;
			}

			static bool parseDictionary(const Dictionary& _dictionary, MapT& o_map)
			{
				const int dictSize = _dictionary.size();
				for (int dictIndex = 0; dictIndex < dictSize; ++dictIndex)
				{
					const Variant keyVariant = _dictionary.get_key_at_index(dictIndex);
					KeyT key;
					if (!VariantConverter<KeyT>::fromVariantTry(keyVariant, key))
					{
						continue;
					}

					const Variant valueVariant = _dictionary.get_value_at_index(dictIndex);
					ValueT value;
					if (!VariantConverter<ValueT>::fromVariantTry(valueVariant, value))
					{
						continue;
					}

					o_map[key] = std::move(value);
				}

				return true;
			}

			template<typename VisitorT>
			static EVisitorResult parseDictionaryVisitor(const Dictionary& _dictionary, VisitorT&& _visitor)
			{
				const int dictSize = _dictionary.size();
				for (int dictIndex = 0; dictIndex < dictSize; ++dictIndex)
				{
					const Variant keyVariant = _dictionary.get_key_at_index(dictIndex);
					KeyT key;
					if (!VariantConverter<KeyT>::fromVariantTry(keyVariant, key))
					{
						continue;
					}

					const Variant valueVariant = _dictionary.get_value_at_index(dictIndex);
					ValueT value;
					if (!VariantConverter<ValueT>::fromVariantTry(valueVariant, value))
					{
						continue;
					}

					if (callVisitor(_visitor, keyVariant, valueVariant) == EVisitorResult::k_break)
					{
						return EVisitorResult::k_break;
					}
				}

				return EVisitorResult::k_continue;
			}
		};
	} // namespace detail

	template<typename StdMapT>
	struct DictionaryConverter
	{
		static Dictionary createDictionary(const StdMapT& _map)
		{
			return detail::GeneralDictionaryConverter<
				StdMapT,
				typename StdMapT::key_type,
				typename StdMapT::mapped_type>::createDictionary(_map);
		}

		static bool parseDictionaryTry(const Dictionary& _dictionary, StdMapT& o_map)
		{
			return detail::GeneralDictionaryConverter<
				StdMapT,
				typename StdMapT::key_type,
				typename StdMapT::mapped_type>::parseDictionaryTry(_dictionary, o_map);
		}

		static bool parseDictionary(const Dictionary& _dictionary, StdMapT& o_map)
		{
			return detail::GeneralDictionaryConverter<
				StdMapT,
				typename StdMapT::key_type,
				typename StdMapT::mapped_type>::parseDictionary(_dictionary, o_map);
		}

		template<typename VisitorT>
		static EVisitorResult parseDictionaryVisitor(const Dictionary& _dictionary, VisitorT&& _visitor)
		{
			return detail::GeneralDictionaryConverter<
				StdMapT,
				typename StdMapT::key_type,
				typename StdMapT::mapped_type>::parseDictionaryVisitor(_dictionary, std::forward<VisitorT>(_visitor));
		}
	};

	template<typename KeyT, typename ValueT>
	struct DictionaryConverter<VMap<KeyT, ValueT>>
	{
		static Dictionary createDictionary(const VMap<KeyT, ValueT>& _map)
		{
			Dictionary returnDictionary;
			using PairType = Pair<KeyT, ValueT>;
			const PairType* pairIt = _map.get_array();
			const PairType* const endIt = pairIt + _map.size();

			for (; pairIt != endIt; ++pairIt)
			{
				const Variant keyVariant = VariantConverter<KeyT>::toVariant(pairIt->first);
				const Variant valueVariant = VariantConverter<ValueT>::toVariant(pairIt->second);
				returnDictionary[keyVariant] = valueVariant;
			}

			return returnDictionary;
		}

		static bool parseDictionaryTry(const Dictionary& _dictionary, VMap<KeyT, ValueT>& o_map)
		{
			return detail::GeneralDictionaryConverter<VMap<KeyT, ValueT>, KeyT, ValueT>::parseDictionaryTry(_dictionary, o_map);
		}

		static bool parseDictionary(const Dictionary& _dictionary, VMap<KeyT, ValueT>& o_map)
		{
			return detail::GeneralDictionaryConverter<VMap<KeyT, ValueT>, KeyT, ValueT>::parseDictionary(_dictionary, o_map);
		}

		template<typename VisitorT>
		static EVisitorResult parseDictionaryVisitor(const Dictionary& _dictionary, VisitorT&& _visitor)
		{
			return detail::GeneralDictionaryConverter<VMap<KeyT, ValueT>, KeyT, ValueT>::parseDictionaryVisitor(
				_dictionary,
				std::forward<VisitorT>(_visitor));
		}
	};

	template<typename ValueT>
	struct VariantConverter
	{
		static constexpr Variant::Type k_variantType = VariantTraits<ValueT>::k_type;

		static Variant toVariant(const ValueT& _value)
		{
			return Variant(_value);
		}

		static typename VariantTraits<ValueT>::StorageType toVariantStorage(const ValueT& _value)
		{
			return typename VariantTraits<ValueT>::StorageType(_value);
		}

		static bool fromVariantTry(const Variant& _variant, ValueT& o_value)
		{
			if (!Variant::can_convert(_variant.get_type(), VariantTraits<ValueT>::k_type))
			{
				return false;
			}

			o_value = _variant;
			return true;
		}

		static bool fromVariant(const Variant& _variant, ValueT& o_value)
		{
			return fromVariantTry(_variant, o_value);
		}
	};

	namespace detail
	{
		template<typename VectorT, typename ValueT>
		struct VariantVectorConverter
		{
			static constexpr Variant::Type getVariantPackedItemType(const Variant::Type _arrayType)
			{
				switch (_arrayType)
				{
					case Variant::Type::PACKED_BYTE_ARRAY: return Variant::Type::INT;
					case Variant::Type::PACKED_INT32_ARRAY: return Variant::Type::INT;
					case Variant::Type::PACKED_INT64_ARRAY: return Variant::Type::INT;
					case Variant::Type::PACKED_FLOAT32_ARRAY: return Variant::Type::FLOAT;
					case Variant::Type::PACKED_FLOAT64_ARRAY: return Variant::Type::FLOAT;
					case Variant::Type::PACKED_STRING_ARRAY: return Variant::Type::STRING;
					case Variant::Type::PACKED_VECTOR2_ARRAY: return Variant::Type::VECTOR2;
					case Variant::Type::PACKED_VECTOR3_ARRAY: return Variant::Type::VECTOR3;
					case Variant::Type::PACKED_COLOR_ARRAY: return Variant::Type::COLOR;

					default:
						break;
				}

				return Variant::Type::NIL;
			}

			static constexpr Variant::Type getVariantPackedType(const Variant::Type _itemType)
			{
				switch (_itemType)
				{
					case Variant::Type::INT: return Variant::Type::PACKED_INT64_ARRAY;
					case Variant::Type::FLOAT: return Variant::Type::PACKED_FLOAT64_ARRAY;
					case Variant::Type::STRING: return Variant::Type::PACKED_STRING_ARRAY;
					case Variant::Type::VECTOR2: return Variant::Type::PACKED_VECTOR2_ARRAY;
					case Variant::Type::VECTOR3: return Variant::Type::PACKED_VECTOR3_ARRAY;
					case Variant::Type::COLOR: return Variant::Type::PACKED_COLOR_ARRAY;

					default:
						break;
				}

				return Variant::Type::NIL;
			}

			static constexpr Variant::Type getVariantContainerType()
			{
				constexpr Variant::Type k_valueType = VariantConverter<ValueT>::k_variantType;
				constexpr Variant::Type k_poolType = getVariantPackedType(k_valueType);

				return k_poolType == Variant::Type::NIL
					? Variant::Type::ARRAY
					: k_poolType;
			}

			template<typename VectorItemT>
			static void convertFromPacked(const Vector<VectorItemT>& _packedVector, VectorT& o_vector)
			{
				o_vector.reserve(_packedVector.size());
				for (const VectorItemT& item : _packedVector)
				{
					o_vector.emplace_back(item);
				}
			}

			static constexpr Variant::Type k_variantType = getVariantContainerType();

			static Variant toVariant(const VectorT& _vector)
			{
				static constexpr Variant::Type k_valueType = VariantConverter<ValueT>::k_variantType;
				static constexpr Variant::Type k_packedType = getVariantPackedType(k_valueType);
				if constexpr (k_packedType != Variant::Type::NIL)
				{
					using StorageType = typename GetVariantStorage<k_valueType>::StorageType;

					Vector<StorageType> returnPacked;
					returnPacked.resize(static_cast<int>(_vector.size()));
					int currentPoolIndex = 0;
					for (const ValueT& item : _vector)
					{
						returnPacked[currentPoolIndex] = VariantConverter<ValueT>::toVariantStorage(item);
						++currentPoolIndex;
					}
					return returnPacked;
				}
				else
				{
					return ArrayConverter<VectorT>::createArray(_vector);
				}
			}

			static bool fromVariantTry(const Variant& _variant, VectorT& o_vector)
			{
				static constexpr Variant::Type k_vectorItemType = DecayedVariantTraits<ValueT>::k_type;
			
				// First, it's better to simply check if this is some sort of a pool vector
				if (const Variant::Type poolItemType = getVariantPackedItemType(_variant.get_type()); poolItemType != Variant::Type::NIL)
				{
					if (!Variant::can_convert(poolItemType, k_vectorItemType))
					{
						return false;
					}

					switch (_variant.get_type())
					{
						case Variant::Type::PACKED_BYTE_ARRAY: convertFromPacked<uint8_t>(_variant, o_vector); return true;
						case Variant::Type::PACKED_INT32_ARRAY: convertFromPacked<int32_t>(_variant, o_vector); return true;
						case Variant::Type::PACKED_INT64_ARRAY: convertFromPacked<int64_t>(_variant, o_vector); return true;
						case Variant::Type::PACKED_FLOAT32_ARRAY: convertFromPacked<float>(_variant, o_vector); return true;
						case Variant::Type::PACKED_FLOAT64_ARRAY: convertFromPacked<double>(_variant, o_vector); return true;
						case Variant::Type::PACKED_STRING_ARRAY: convertFromPacked<String>(_variant, o_vector); return true;
						case Variant::Type::PACKED_VECTOR2_ARRAY: convertFromPacked<Vector2>(_variant, o_vector); return true;
						case Variant::Type::PACKED_VECTOR3_ARRAY: convertFromPacked<Vector3>(_variant, o_vector); return true;
						case Variant::Type::PACKED_COLOR_ARRAY: convertFromPacked<Color>(_variant, o_vector); return true;

						default: break;
					}

					// In theory we should not get here, but just in case.
					return false;
				}

				// The only viable type left is array.
				if (_variant.get_type() != Variant::Type::ARRAY)
				{
					return false;
				}

				const Array variantArray = _variant;
				return ArrayConverter<VectorT>::parseArrayTry(variantArray, o_vector);
			}

			static bool fromVariant(const Variant& _variant, std::vector<ValueT>& o_vector)
			{
				static constexpr Variant::Type k_vectorItemType = DecayedVariantTraits<ValueT>::k_type;
			
				// First, it's better to simply check if this is some sort of a pool vector
				if (const Variant::Type poolItemType = getVariantPackedItemType(_variant.get_type()); poolItemType != Variant::Type::NIL)
				{
					if (!Variant::can_convert(poolItemType, k_vectorItemType))
					{
						return false;
					}

					switch (_variant.get_type())
					{
						case Variant::Type::PACKED_BYTE_ARRAY: convertFromPacked<uint8_t>(_variant, o_vector); return true;
						case Variant::Type::PACKED_INT32_ARRAY: convertFromPacked<int32_t>(_variant, o_vector); return true;
						case Variant::Type::PACKED_INT64_ARRAY: convertFromPacked<int64_t>(_variant, o_vector); return true;
						case Variant::Type::PACKED_FLOAT32_ARRAY: convertFromPacked<float>(_variant, o_vector); return true;
						case Variant::Type::PACKED_FLOAT64_ARRAY: convertFromPacked<double>(_variant, o_vector); return true;
						case Variant::Type::PACKED_STRING_ARRAY: convertFromPacked<String>(_variant, o_vector); return true;
						case Variant::Type::PACKED_VECTOR2_ARRAY: convertFromPacked<Vector2>(_variant, o_vector); return true;
						case Variant::Type::PACKED_VECTOR3_ARRAY: convertFromPacked<Vector3>(_variant, o_vector); return true;
						case Variant::Type::PACKED_COLOR_ARRAY: convertFromPacked<Color>(_variant, o_vector); return true;

						default: break;
					}

					// In theory we should not get here, but just in case.
					return false;
				}

				// The only viable type left is array.
				if (_variant.get_type() != Variant::Type::ARRAY)
				{
					return false;
				}

				const Array variantArray = _variant;
				return ArrayConverter<VectorT>::parseArray(variantArray, o_vector);
			}
		};
	} // namespace detail

	template<typename ValueT, typename AllocT>
	struct VariantConverter<std::vector<ValueT, AllocT>>
	{
		static constexpr Variant::Type k_variantType = detail::VariantVectorConverter<std::vector<ValueT, AllocT>, ValueT>::k_variantType;

		static Variant toVariant(const std::vector<ValueT, AllocT>& _vector)
		{
			return detail::VariantVectorConverter<std::vector<ValueT, AllocT>, ValueT>::toVariant(_vector);
		}

		static bool fromVariantTry(const Variant& _variant, std::vector<ValueT, AllocT>& o_vector)
		{
			return detail::VariantVectorConverter<std::vector<ValueT, AllocT>, ValueT>::fromVariantTry(_variant, o_vector);
		}

		static bool fromVariant(const Variant& _variant, std::vector<ValueT, AllocT>& o_vector)
		{
			return detail::VariantVectorConverter<std::vector<ValueT, AllocT>, ValueT>::fromVariant(_variant, o_vector);
		}
	};

	template<typename KeyT, typename ValueT, typename CompareT>
	struct VariantConverter<std::map<KeyT, ValueT, CompareT>>
	{
		static constexpr Variant::Type k_variantType = Variant::Type::DICTIONARY;

		static Variant toVariant(const std::map<KeyT, ValueT, CompareT>& _map)
		{
			return DictionaryConverter<std::map<KeyT, ValueT, CompareT>>::createDictionary(_map);
		}

		static bool fromVariantTry(const Variant& _variant, std::map<KeyT, ValueT, CompareT>& o_map)
		{
			if (_variant.get_type() != Variant::Type::DICTIONARY)
			{
				return false;
			}

			return DictionaryConverter<std::map<KeyT, ValueT, CompareT>>::parseDictionaryTry(_variant, o_map);
		}

		static bool fromVariant(const Variant& _variant, std::map<KeyT, ValueT, CompareT>& o_map)
		{
			if (_variant.get_type() != Variant::Type::DICTIONARY)
			{
				return false;
			}

			return DictionaryConverter<std::map<KeyT, ValueT, CompareT>>::parseDictionary(_variant, o_map);
		}
	};

	template<typename KeyT, typename ValueT, typename HashT, typename PredT, typename AllocT>
	struct VariantConverter<std::unordered_map<KeyT, ValueT, HashT, PredT, AllocT>>
	{
		static constexpr Variant::Type k_variantType = Variant::Type::DICTIONARY;

		static Variant toVariant(const std::unordered_map<KeyT, ValueT, HashT, PredT, AllocT>& _map)
		{
			return DictionaryConverter<std::unordered_map<KeyT, ValueT, HashT, PredT, AllocT>>::createDictionary(_map);
		}

		static bool fromVariantTry(const Variant& _variant, std::unordered_map<KeyT, ValueT, HashT, PredT, AllocT>& o_map)
		{
			if (_variant.get_type() != Variant::Type::DICTIONARY)
			{
				return false;
			}

			return DictionaryConverter<std::unordered_map<KeyT, ValueT, HashT, PredT, AllocT>>::parseDictionaryTry(_variant, o_map);
		}

		static bool fromVariant(const Variant& _variant, std::unordered_map<KeyT, ValueT, HashT, PredT, AllocT>& o_map)
		{
			if (_variant.get_type() != Variant::Type::DICTIONARY)
			{
				return false;
			}
			
			return DictionaryConverter<std::unordered_map<KeyT, ValueT, HashT, PredT, AllocT>>::parseDictionary(_variant, o_map);
		}
	};

	template<typename KeyT, typename ValueT>
	struct VariantConverter<VMap<KeyT, ValueT>>
	{
		static constexpr Variant::Type k_variantType = Variant::Type::DICTIONARY;

		static Variant toVariant(const VMap<KeyT, ValueT>& _map)
		{
			return DictionaryConverter<VMap<KeyT, ValueT>>::createDictionary(_map);
		}

		static bool fromVariantTry(const Variant& _variant, VMap<KeyT, ValueT>& o_map)
		{
			if (_variant.get_type() != Variant::Type::DICTIONARY)
			{
				return false;
			}

			return DictionaryConverter<VMap<KeyT, ValueT>>::parseDictionaryTry(_variant, o_map);
		}

		static bool fromVariant(const Variant& _variant, VMap<KeyT, ValueT>& o_map)
		{
			if (_variant.get_type() != Variant::Type::DICTIONARY)
			{
				return false;
			}
			
			return DictionaryConverter<VMap<KeyT, ValueT>>::parseDictionary(_variant, o_map);
		}
	};

	template<typename ValueT>
	bool tryGetDictionaryValue(const Dictionary& _dictionary, const Variant& _key, ValueT& o_value)
	{
		return VariantConverter<ValueT>::fromVariantTry(_dictionary.get(_key, Variant()), o_value);
	}

} // namespace bwn