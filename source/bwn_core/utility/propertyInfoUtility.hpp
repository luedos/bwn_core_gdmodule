#pragma once

#include "bwn_core/utility/variantUtility.hpp"

#include <core/object/object.h>
#include <core/string/ustring.h>

namespace bwn
{
	template<typename PropertyT>
	class PropertyUtility
	{
		//
		// Construction and destruction.
		//
	public:
		PropertyUtility(const String& _name);

		//
		// Public interface.
		//
	public:
		// Sets hint of the property.
		PropertyUtility& hint(PropertyHint _hintEnum, const String& _hintString = String());
		// Sets usage flags of the property.
		PropertyUtility& usage(uint32_t _usageFlags);
		// Sets class name for the property (not sure exactly what this is)
		PropertyUtility& className(const String& _name);
		// Returns formated property info.
		PropertyInfo& get();

		//
		// Private members.
		//
	private:
		// Property info, which we initializing.
		PropertyInfo m_info;
	};
} // namespace bwn

template<typename PropertyT>
bwn::PropertyUtility<PropertyT>::PropertyUtility(const String& _name)
	: m_info(bwn::VariantTraits<PropertyT>::k_type, _name)
{}


template<typename PropertyT>
bwn::PropertyUtility<PropertyT>& bwn::PropertyUtility<PropertyT>::hint(PropertyHint _hintEnum, const String& _hintString)
{
	m_info.hint = _hintEnum;
	m_info.hint_string = _hintString;

	if (_hintEnum == PROPERTY_HINT_RESOURCE_TYPE)
	{
		m_info.class_name = _hintString;
	}

	return *this;
}

template<typename PropertyT>
bwn::PropertyUtility<PropertyT>& bwn::PropertyUtility<PropertyT>::usage(uint32_t _usageFlags)
{
	m_info.usage = _usageFlags;

	return *this;
}

template<typename PropertyT>
bwn::PropertyUtility<PropertyT>& bwn::PropertyUtility<PropertyT>::className(const String& _name)
{
	m_info.class_name = _name;

	return *this;
}

template<typename PropertyT>
PropertyInfo& bwn::PropertyUtility<PropertyT>::get()
{
	return m_info;
}
