#pragma once

#include "bwn_core/types/stringView.hpp"
#include "bwn_core/types/stringId.hpp"
#include "bwn_core/utility/visitorUtility.hpp"
#include "bwn_core/debug/debug.hpp"

#include <scene/main/node.h>

namespace bwn
{
	class StringId;
	
	// Searches for child with exact name.
	Node* findChildByExactName(const Node& _parent, const bwn::string_view& _name);
	Node* findChildByExactName(const Node& _parent, const StringId _name);

	template<typename TypeT>
	TypeT* findChildByExactNameAs(const Node& _parent, const bwn::string_view& _name)
	{
		return Object::cast_to<TypeT>(findChildByExactName(_parent, _name));
	}
	template<typename TypeT>
	TypeT* findChildByExactNameAs(const Node& _parent, const StringId _name)
	{
		return Object::cast_to<TypeT>(findChildByExactName(_parent, _name));
	}

	// Searches for child with partial name.
	Node* findChildByPartialName(const Node& _parent, const bwn::string_view& _partialName);

	// Searches for a child by specific path. Path should be start from a child (path should not have a name of a node provided as _parent).
	Node* findChildByExactPath(const Node&, const bwn::string_view& _path);
	// Searches for a child by path. Path should be start from a child (path should not have a name of a node provided as _parent).
	// Path should be exact except for the last node name, which could be partial.
	Node* findChildByPartialPath(const Node&, const bwn::string_view& _path);

	template<typename ClassT>
	size_t getChildrenCountOf(const Node& _parent)
	{
		size_t result = 0;
		const int totalCount = _parent.get_child_count();
		for (int childIndex = 0; childIndex < totalCount; ++childIndex)
		{
			result += _parent.get_child(childIndex)->is_class_ptr(ClassT::get_class_ptr_static());
		}

		return result;
	}

	namespace detail
	{
		template<typename VisitorT>
		EVisitorResult visitDescendantsRecursiveImpl(const Node& _parent, VisitorT&& _visitor)
		{
			const int childrenCount = _parent.get_child_count();
			for (int childIndex = 0; childIndex < childrenCount; ++childIndex)
			{
				Node& child = *_parent.get_child(childIndex);
				if (callVisitor(std::forward<VisitorT>(_visitor), child) == EVisitorResult::k_break)
				{
					return EVisitorResult::k_break;
				}

				if (visitDescendantsRecursiveImpl(child, std::forward<VisitorT>(_visitor)) == EVisitorResult::k_break)
				{
					return EVisitorResult::k_break;
				}
			}

			return EVisitorResult::k_continue;
		}

		template<typename VisitorT>
		EVisitorResult visitDescendantsDepthRecursiveImpl(const Node& _parent, const size_t _depth, VisitorT&& _visitor)
		{
			const int childrenCount = _parent.get_child_count();
			for (int childIndex = 0; childIndex < childrenCount; ++childIndex)
			{
				Node& child = *_parent.get_child(childIndex);
				if (callVisitor(std::forward<VisitorT>(_visitor), child) == EVisitorResult::k_break)
				{
					return EVisitorResult::k_break;
				}

				if (_depth > 0)
				{
					if (visitDescendantsDepthRecursiveImpl(child,_depth - 1,std::forward<VisitorT>(_visitor)) == EVisitorResult::k_break)
					{
						return EVisitorResult::k_break;
					}
				}
			}

			return EVisitorResult::k_continue;
		}

		template<typename VisitorT>
		EVisitorResult visitDescendantsDepthRangeRecursiveImpl(const Node& _parent, const size_t _minDepth, const size_t _maxDepth, VisitorT&& _visitor)
		{
			const int childrenCount = _parent.get_child_count();
			for (int childIndex = 0; childIndex < childrenCount; ++childIndex)
			{
				Node& child = *_parent.get_child(childIndex);
				if (_minDepth == 0)
				{
					if (callVisitor(std::forward<VisitorT>(_visitor), child) == EVisitorResult::k_break)
					{
						return EVisitorResult::k_break;
					}
				}

				if (_maxDepth > 0)
				{
					const size_t newMinDepth = _minDepth == 0
						? 0
						: _minDepth - 1;

					if (visitDescendantsDepthRangeRecursiveImpl(
						child,
						newMinDepth,
						_maxDepth - 1,
						std::forward<VisitorT>(_visitor)) == EVisitorResult::k_break)
					{
						return EVisitorResult::k_break;
					}
				}
			}

			return EVisitorResult::k_continue;
		}
	} // namespace detail

	template<typename ChildTypeT, typename VisitorT>
	EVisitorResult visitChildrenAs(const Node& _parent, VisitorT&& _visitor)
	{
		const int childrenCount = _parent.get_child_count();
		for (int childIndex = 0; childIndex < childrenCount; ++childIndex)
		{
			Node*const child = _parent.get_child(childIndex);
			
			ChildTypeT* castedChild;
			if constexpr (std::is_same_v<ChildTypeT, Node>)
			{
				castedChild = child;
			}
			else
			{
				castedChild = Object::cast_to<ChildTypeT>(child);
			}

			if (castedChild != nullptr)
			{
				if (bwn::callVisitor(std::forward<VisitorT>(_visitor), *castedChild) == EVisitorResult::k_break)
				{
					return EVisitorResult::k_break;
				}
			}
		}

		return EVisitorResult::k_continue;
	}

	template<typename VisitorT>
	EVisitorResult visitDescendants(const Node& _parent, VisitorT&& _visitor)
	{
		return detail::visitDescendantsRecursiveImpl(_parent, std::forward<VisitorT>(_visitor));
	}

	// Accepts depth as a parameter. If _depth is 0, visits only children.
	template<typename VisitorT>
	EVisitorResult visitDescendants(const Node& _parent, const size_t _depth, VisitorT&& _visitor)
	{
		return detail::visitDescendantsDepthRecursiveImpl(_parent, _depth, std::forward<VisitorT>(_visitor));
	}

	// Visits the parent node, and all it's descendants.
	template<typename NodeT, typename VisitorT>
	EVisitorResult visitTree(NodeT& _parent, VisitorT&& _visitor)
	{
		if (callVisitor(std::forward<VisitorT>(_visitor), _parent) == EVisitorResult::k_break)
		{
			return EVisitorResult::k_break;
		}

		return detail::visitDescendantsRecursiveImpl(_parent, std::forward<VisitorT>(_visitor));
	}

	// Visits the parent node, and all it's descendants with max depth. If depth is 0, visits only the root node.
	template<typename NodeT, typename VisitorT>
	EVisitorResult visitTree(NodeT& _parent, const size_t _depth, VisitorT&& _visitor)
	{
		const EVisitorResult parentResult = callVisitor(std::forward<VisitorT>(_visitor), _parent);
		if (parentResult == EVisitorResult::k_break)
		{
			return EVisitorResult::k_break;
		}

		if (_depth == 0)
		{
			return EVisitorResult::k_continue;
		}

		return detail::visitDescendantsDepthRecursiveImpl(_parent, _depth - 1, std::forward<VisitorT>(_visitor));
	}

	// Visits the parent node, and all it's descendants if they are further then min depth, but not further then max depth.
	// If min depth is 0, visits all node with the root one (min depth == 1 -> visits starting from children and so on)
	// If max depth is 0, visits only the root node (max depth == 1 -> visits only children ans so on).
	template<typename NodeT, typename VisitorT>
	EVisitorResult visitTree(NodeT& _parent, const size_t _minDepth, const size_t _maxDepth, VisitorT&& _visitor)
	{
		BWN_ASSERT_WARNING_COND(
			_minDepth <= _maxDepth,
			"Min depth of the search (provided: {}) should be smaller or same as max depth of the search (provided: {}), otherwise no nodes will be searched.",
			_minDepth,
			_maxDepth);

		if (_minDepth == 0)
		{
			const EVisitorResult parentResult = callVisitor(std::forward<VisitorT>(_visitor), _parent);
			if (parentResult == EVisitorResult::k_break)
			{
				return EVisitorResult::k_break;
			}
			if (_maxDepth == 0)
			{
				return EVisitorResult::k_continue;
			}
		}

		const size_t newMinDepth = _minDepth == 0
			? 0
			: _minDepth - 1;
		return detail::visitDescendantsDepthRangeRecursiveImpl(
			_parent,
			newMinDepth,
			_maxDepth - 1,
			std::forward<VisitorT>(_visitor));
	}

	// Searches for descendant with exact name.
	Node* findDescendantByExactName(const Node& _parent, const bwn::string_view& _name);
	// Searches for descendant with exact name and specific type.
	Node* findDescendantByExactName(const Node& _parent, const bwn::string_view& _name, const String& _typeHint);
	// Searches for descendant with partial name.
	Node* findDescendantByPartialName(const Node& _parent, const bwn::string_view& _partialName);
	// Searches for descendant with partial name and specific type.
	Node* findDescendantByPartialName(const Node& _parent, const bwn::string_view& _partialName, const String& _typeHint);

	// Searches for a descendant which is the final node in specified path.
	Node* findDescendantByExactPath(const Node& _node, const bwn::string_view& _path);
	// Searches for a descendant which is the final node in specified path and specific type.
	Node* findDescendantByExactPath(const Node& _node, const bwn::string_view& _path, const String& _typeHint);
	// Searches for a descendant which is the final node in specified path.
	// Path should be exact except for the last node name, which could be partial.
	Node* findDescendantByPartialPath(const Node& _node, const bwn::string_view& _path);
	// Searches for a descendant which is the final node in specified path and specific type.
	Node* findDescendantByPartialPath(const Node& _node, const bwn::string_view& _path, const String& _typeHint);

	// Checks that node is a part of the specific path.
	// Basically returns true if _highestNode is a last node in _pathToCheck, and that all the parents of _highestNode form the rest of the _pathToCheck.
	// _lowestNode is a stopping point for path checking. It must not be a part of the path.
	bool checkNodeEndsByExactPath(const bwn::string_view& _pathToCheck, const Node& _highestNode, const Node*const _lowestNode = nullptr);
	// Same as checkNodeExactPath, _highestNode shouldn't be with exact name of the first node in a path.
	bool checkNodeEndsByPartialPath(const bwn::string_view& _pathToCheck, const Node& _highestNode, const Node*const _lowestNode = nullptr);

	// Checks that somewhere in-between highest node and lowest node where is partial path to check (it's partial because it's end node could be partial name).
	bool checkNodeHasExactPath(const bwn::string_view& _pathToCheck, const Node& _highestNode, const Node*const _lowestNode = nullptr);
	// Checks that somewhere in-between highest node and lowest node where is partial path to check (it's partial because it's end node could be partial name).
	bool checkNodeHasPartialPath(const bwn::string_view& _pathToCheck, const Node& _highestNode, const Node*const _lowestNode = nullptr);

} // namespace bwn