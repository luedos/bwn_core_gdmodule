#pragma once

namespace bwn
{
	template<typename TypeT>
	TypeT rotl(TypeT _integral, uint32_t _shift)
	{
		const uint32_t integralBitSize = sizeof(_integral) * 8;
		const uint32_t normilizedShift = _shift % integralBitSize;

		const TypeT shiftedTail = _integral << _shift;
		const TypeT shiftedHead = _integral >> (integralBitSize - normilizedShift);

		return shiftedHead | shiftedTail;
	}

	template<typename TypeT>
	TypeT rotr(TypeT _integral, uint32_t _shift)
	{
		const uint32_t integralBitSize = sizeof(_integral) * 8;
		const uint32_t normilizedShift = _shift % integralBitSize;

		const TypeT shiftedTail = _integral >> _shift;
		const TypeT shiftedHead = _integral << (integralBitSize - normilizedShift);

		return shiftedHead | shiftedTail;
	}
} // namespace bwn