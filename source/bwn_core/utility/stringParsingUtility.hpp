#pragma once

#include <stdint.h>
#include <cctype>
#include <string_view>

namespace bwn
{

template<typename CharT>
bool asDecimalStrict(const std::basic_string_view<CharT> _string, int64_t& o_result)
{
    if (_string.empty())
    {
        return false;
    }

    const CharT firstCharacter = _string.front();
    const bool isNegative = firstCharacter == '-';    
    const bool hasSign = firstCharacter == '-' || firstCharacter == '+';

	// at least 2 if we have sign, and 1 otherwize
    if (_string.length() == hasSign)
    {
        return false;
    }

    static constexpr int64_t maxInt = (~static_cast<uint64_t>(0)) >> 1;
    static constexpr int64_t maxIntDiv = maxInt / 10;
    static constexpr int64_t maxAddition = maxInt - (maxIntDiv * 10);

    int64_t accumulator = 0;

	const CharT* charIt = _string.begin() + hasSign;
	const CharT*const endIt = _string.end();
    for (; charIt < endIt; ++charIt)
    {
        const CharT character = *charIt;

        if (!std::iswdigit(static_cast<wchar_t>(character)))
        {
            return false;
        }

        const int64_t addition = character - '0';

        if (accumulator > maxIntDiv || (accumulator == maxIntDiv && addition > maxAddition ) )
        {
            return false;
        }

        accumulator *= 10;
        accumulator += addition;
    }

    o_result = isNegative 
		? -static_cast<int64_t>(accumulator)
		: static_cast<int64_t>(accumulator);
    return true;
}

template<typename CharT>
bool asDecimalStrict(const std::basic_string_view<CharT> _string, uint64_t& o_result)
{
    if (_string.empty())
    {
        return false;
    }

    static constexpr uint64_t maxInt = ~static_cast<uint64_t>(0);
    static constexpr uint64_t maxIntDiv = maxInt / 10;
    static constexpr uint64_t maxAddition = maxInt - (maxIntDiv * 10);

    uint64_t accumulator = 0;

	const CharT* charIt = _string.begin();
	const CharT*const endIt = _string.end();
    for (; charIt < endIt; ++charIt)
    {
        const CharT character = *charIt;

        if (!std::iswdigit(static_cast<wchar_t>(character)))
        {
            return false;
        }

        const uint64_t addition = character - '0';

        if (accumulator > maxIntDiv || (accumulator == maxIntDiv && addition > maxAddition) )
        {
            return false;
        }

        accumulator *= 10;
        accumulator += addition;
    }

    o_result = accumulator;
    return true;
}

template<typename CharT>
bool asHexadecimalBodyStrict(const std::basic_string_view<CharT> _string, uint64_t& o_result)
{
    // we are parsing without header (0x)
    if (_string.empty())
    {
        return false;
    }

    static constexpr uint64_t maxInt = ~static_cast<uint64_t>(0);
    static constexpr uint64_t maxIntDiv = maxInt >> 4;

    uint64_t accumulator = 0;

	const CharT* charIt = _string.begin();
	const CharT*const endIt = _string.end();
    for (; charIt < endIt; ++charIt)
    {
        const CharT character = *charIt;

        if (!std::iswxdigit(static_cast<wchar_t>(character)))
        {
            return false;
        }

        // This is working only because we know that character could be only between [0-9A-Fa-f]
        constexpr CharT setCharacter[] { '0', 'A', 'a' };
        constexpr uint64_t setOffset[] { 0, 10, 10 };
        const int set = (character > '9') + (character > 'F');

        const uint64_t addition = character - setCharacter[set] + setOffset[set];

        if (accumulator > maxIntDiv)
        {
            return false;
        }

        accumulator <<= 4;
        accumulator += addition;
    }

    o_result = accumulator;
    return true;
}

template<typename CharT>
bool asHexadecimalStrict(const std::basic_string_view<CharT> _string, uint64_t& o_result)
{
	if (_string.length() < 3)
	{
		return false;
	}

	const CharT firstCharacter = _string[0];
	const CharT secondCharacter = _string[1];
	if (firstCharacter != '0' || (secondCharacter != 'x' && secondCharacter != 'X'))
	{
		return false;
	}

	return asHexadecimalBodyStrict(_string.substr(2), o_result);
}

template<typename CharT>
bool asOctodecimalBodyStrict(const std::basic_string_view<CharT> _string, uint64_t& o_result)
{
    // we are parsing without header (which is initial 0, like 01231)
    if (_string.empty())
    {
        return false;
    }

    static constexpr uint64_t maxInt = ~static_cast<uint64_t>(0);
    static constexpr uint64_t maxIntDiv = maxInt >> 3;

    uint64_t accumulator = 0;

	const CharT* charIt = _string.begin();
	const CharT*const endIt = _string.end();
    for (; charIt < endIt; ++charIt)
    {
        const CharT character = *charIt;

        if (character < '0' || character > '7')
        {
            return false;
        }

        const uint64_t addition = character - '0';

        if (accumulator > maxIntDiv)
        {
            return false;
        }

        accumulator <<= 3;
        accumulator += addition;
    }

    o_result = accumulator;
    return true;
}

template<typename CharT>
bool asOctodecimalStrict(const std::basic_string_view<CharT> _string, uint64_t& o_result)
{
	if (_string.length() < 2 || (_string.front() != '0'))
	{
		return false;
	}

	return asOctodecimalBodyStrict(_string.substr(1), o_result);
}

template<typename CharT>
bool asBinaryBodyStrict(const std::basic_string_view<CharT> _string, uint64_t& o_result)
{
    // we are parsing without header (0b)
    if (_string.empty())
    {
        return false;
    }

    static constexpr uint64_t maxInt = ~static_cast<uint64_t>(0);
    static constexpr uint64_t maxIntDiv = maxInt >> 1;

    uint64_t accumulator = 0;

	const CharT* charIt = _string.begin();
	const CharT*const endIt = _string.end();
    for (; charIt < endIt; ++charIt)
    {
        const CharT character = *charIt;

        if (character != '0' && character != '1')
        {
            return false;
        }

        const uint64_t addition = character - '0';

        if (accumulator > maxIntDiv)
        {
            return false;
        }

        accumulator <<= 1;
        accumulator += addition;
    }

    o_result = accumulator;
    return true;
}

template<typename CharT>
bool asBinaryStrict(const std::basic_string_view<CharT> _string, uint64_t& o_result)
{
	if (_string.length() < 3)
	{
		return false;
	}

	const CharT firstCharacter = _string[0];
	const CharT secondCharacter = _string[1];
	if (firstCharacter != '0' || (secondCharacter != 'b' && secondCharacter != 'B'))
	{
		return false;
	}

	return asBinaryBodyStrict(_string.substr(2), o_result);
}

template<typename CharT>
bool asDoubleStrict(const std::basic_string_view<CharT> _string, double& o_result)
{
    if (_string.empty())
    {
        return false;
    }

    const CharT firstCharacter = _string.front();
    const bool sign = firstCharacter == '-';

	const CharT*const mantissaStartIt = _string.begin() + (firstCharacter == '-' || firstCharacter == '+');
	const CharT*const endIt = _string.end();
	/*
	 * Count the number of digits in the mantissa (including the decimal
	 * point), and also locate the decimal point.
	 */

	int decimalPointIndex = -1;
	int mantissaSize = 0;
	
	const CharT* mantissaIt = mantissaStartIt;
	for (; mantissaIt < endIt; ++mantissaIt)
	{
		const CharT character = *mantissaIt;
		if (!std::iswdigit(static_cast<wchar_t>(character)))
		{
			// start of the exponent.
			if (character == 'e' || character == 'E')
			{
				break;
			}

			// If this is not a dicimal point, or our decimal point not the first, the float is invalid.
			const bool firstDecimalPoint = character == '.' && decimalPointIndex == -1;
			if (!firstDecimalPoint)
			{
				return false;
			}

			decimalPointIndex = mantissaSize;
		}

		++mantissaSize;
	}

	// If there were no digits in the mantissa, the value is invalid
	if (mantissaSize == 0)
	{
		return false;
	}

	// Just to track the start of the exponent. Could equal to _end.
	const CharT*const exponentStartIt = mantissaIt;

	/*
	 * Now suck up the digits in the mantissa. Use two integers to collect 9
	 * digits each (this is faster than using floating-point). If the mantissa
	 * has more than 18 digits, ignore the extras, since they can't affect the
	 * value anyway.
	 */
	if (decimalPointIndex == -1) 
	{
		decimalPointIndex = mantissaSize;
	} 
	else 
	{
		mantissaSize -= 1; /* One of the digits was the point. */
	}

	/* Exponent that derives from the fractional
	 * part. Under normal circumstances, it is
	 * the negative of the number of digits in F.
	 * However, if I is very long, the last digits
	 * of I get dropped (otherwise a long I with a
	 * large negative exponent could cause an
	 * unnecessary overflow on I alone). In this
	 * case, fracExp is incremented one for each
	 * dropped digit. */
	if (mantissaSize > 18)
	{
		mantissaSize = 18;
	}

	const int fractionalExponent = decimalPointIndex - mantissaSize;
	
	double fraction = [mantissaSize, mantissaStartIt]()
	{
		int localMantissaSize = mantissaSize;
		const CharT* charIt = mantissaStartIt;

		int32_t frac1 = 0;
		while (localMantissaSize > 9) 
		{
			const CharT character = *charIt;
			++charIt;

			if (character == '.')
			{
				continue;
			}

			frac1 = 10 * frac1 + (character - '0');
			--localMantissaSize;
		}

		int32_t frac2 = 0;
		while (localMantissaSize > 0)
		{
			const CharT character = *charIt;
			++charIt;

			if (character == '.')
			{
				continue;
			}

			frac2 = 10 * frac2 + (character - '0');
			--localMantissaSize;
		}

		return (1.0e9 * static_cast<double>(frac1)) + static_cast<double>(frac2);
	}();

	/*
	 * Skim off the exponent.
	 */

	int exponent = 0;
	bool exponentSign = false;
	// The only way we got here in first place is if we either didn't get exponent and all characters till _end was valid,
	// or if we have struck 'e' and exited the mantissa loop
	if (exponentStartIt != endIt)
	{
		// past the 'e'
		const CharT* exponentCharIt = exponentStartIt + 1;
		const CharT signCharacter = *exponentCharIt;
		
		if (signCharacter == '-' || signCharacter == '+')
		{
			exponentSign = signCharacter == '-';
			++exponentCharIt;
		}

		if (exponentCharIt >= endIt)
		{
			return false;
		}

		while (exponentCharIt < endIt)
		{
			const CharT character = *exponentCharIt;
			if (!std::iswdigit(static_cast<wchar_t>(character)))
			{
				return false;
			}

			exponent = exponent * 10 + (character - '0');
			++exponentCharIt;
		}
	}
	if (exponentSign)
	{
		exponent = fractionalExponent - exponent;
	}
	else
	{
		exponent = fractionalExponent + exponent;
	}

	/*
	 * Generate a floating-point number that represents the exponent. Do this
	 * by processing the exponent one bit at a time to combine many powers of
	 * 2 of 10. Then combine the exponent with the fraction.
	 */

	if (exponent < 0) 
	{
		exponentSign = true;
		exponent = -exponent;
	} 
	else 
	{
		exponentSign = false;
	}

	/* Largest possible base 10 exponent. Any
	 * exponent larger than this will already
	 * produce underflow or overflow, so there's
	 * no need to worry about additional digits. */
	static constexpr int k_maxExponent = 307;
	/* Table giving binary powers of 10. Entry
	 * is 10^2^i. Used to convert decimal
	 * exponents into floating-point numbers. */
	static const double k_powersOf10[]
	{
		10.,
		100.,
		1.0e4,
		1.0e8,
		1.0e16,
		1.0e32,
		1.0e64,
		1.0e128,
		1.0e256
	};

	if (exponent > k_maxExponent) 
	{
		return false;
	}

	double doubleExponent = 1.0;
	for (const double* powerIt = k_powersOf10; exponent != 0; exponent >>= 1, ++powerIt)
	{
		if (exponent & 1) 
		{
			doubleExponent *= *powerIt;
		}
	}
	if (exponentSign) 
	{
		fraction /= doubleExponent;
	} 
	else 
	{
		fraction *= doubleExponent;
	}

	o_result = sign 
		? -fraction
		: fraction;

	return true;
}

} // namespace bwn