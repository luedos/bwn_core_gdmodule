#include "precompiled_core.hpp"

#include "bwn_core/utility/variantUtility.hpp"

Variant bwn::convertVariant(const Variant& _input, const Variant::Type _type)
{
	if (_input.get_type() == _type)
	{
		return _input;
	}

	switch (_type)
	{
		case Variant::NIL: return Variant();
		case Variant::BOOL: return _input.operator bool();
		case Variant::INT: return _input.operator int64_t();
		case Variant::FLOAT: return _input.operator double();
		case Variant::STRING: return _input.operator String();
		case Variant::VECTOR2: return _input.operator Vector2();
		case Variant::VECTOR2I: return _input.operator Vector2i();
		case Variant::RECT2: return _input.operator Rect2();
		case Variant::RECT2I: return _input.operator Rect2i();
		case Variant::VECTOR3: return _input.operator Vector3();
		case Variant::VECTOR3I: return _input.operator Vector3i();
		case Variant::TRANSFORM2D: return _input.operator Transform2D();
		case Variant::VECTOR4: return _input.operator Vector4();
		case Variant::VECTOR4I: return _input.operator Vector4i();
		case Variant::PLANE: return _input.operator Plane();
		case Variant::QUATERNION: return _input.operator Quaternion();
		case Variant::AABB: return _input.operator AABB();
		case Variant::BASIS: return _input.operator Basis();
		case Variant::TRANSFORM3D: return _input.operator Transform3D();
		case Variant::PROJECTION: return _input.operator Projection();
		case Variant::COLOR: return _input.operator Color();
		case Variant::STRING_NAME: return _input.operator StringName();
		case Variant::NODE_PATH: return _input.operator NodePath();
		case Variant::RID: return _input.operator RID();
		case Variant::OBJECT: return _input.operator Object*();
		case Variant::CALLABLE: return _input.operator Callable();
		case Variant::SIGNAL: return _input.operator Signal();
		case Variant::DICTIONARY: return _input.operator Dictionary();
		case Variant::ARRAY: return _input.operator Array();
		case Variant::PACKED_BYTE_ARRAY: return _input.operator PackedByteArray();
		case Variant::PACKED_INT32_ARRAY: return _input.operator PackedInt32Array();
		case Variant::PACKED_INT64_ARRAY: return _input.operator PackedInt64Array();
		case Variant::PACKED_FLOAT32_ARRAY: return _input.operator PackedFloat32Array();
		case Variant::PACKED_FLOAT64_ARRAY: return _input.operator PackedFloat64Array();
		case Variant::PACKED_STRING_ARRAY: return _input.operator PackedStringArray();
		case Variant::PACKED_VECTOR2_ARRAY: return _input.operator PackedVector2Array();
		case Variant::PACKED_VECTOR3_ARRAY: return _input.operator PackedVector3Array();
		case Variant::PACKED_COLOR_ARRAY: return _input.operator PackedColorArray();

		case Variant::VARIANT_MAX:
		  break;
	}

	return Variant();
}