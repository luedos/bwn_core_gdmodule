#include "precompiled_core.hpp"

#include "bwn_core/utility/nodeUtility.hpp"
#include "bwn_core/types/stringId.hpp"

Node* bwn::findChildByExactName(const Node& _parent, const bwn::string_view& _name)
{
	const int childrenCount = _parent.get_child_count();
	for (int childIndex = 0; childIndex < childrenCount; ++childIndex)
	{
		Node& child = *_parent.get_child(childIndex);
		const String childName = child.get_name();
		const bwn::string_view childNameView = bwn::wrap_view(childName);
		if (childNameView == _name)
		{
			return &child;
		}
	}

	return nullptr;
}

Node* bwn::findChildByExactName(const Node& _parent, const StringId _name)
{
	const int childrenCount = _parent.get_child_count();
	for (int childIndex = 0; childIndex < childrenCount; ++childIndex)
	{
		Node& child = *_parent.get_child(childIndex);
		if (child.get_name() == _name)
		{
			return &child;
		}
	}

	return nullptr;
}

Node* bwn::findChildByPartialName(const Node& _parent, const bwn::string_view& _partialName)
{
	const int childrenCount = _parent.get_child_count();
	for (int childIndex = 0; childIndex < childrenCount; ++childIndex)
	{
		Node& child = *_parent.get_child(childIndex);
		const String childName = child.get_name();
		const bwn::string_view childNameView = bwn::wrap_view(childName);
		if (childNameView.find(_partialName) != bwn::string_view::npos)
		{
			return &child;
		}
	}

	return nullptr;
}

Node* bwn::findChildByExactPath(const Node& _node, const bwn::string_view& _path)
{
	const auto [childName, restPath] = splitPathOnBegin(_path);
	if (childName.empty())
	{
		return nullptr;
	}

	Node*const child = findChildByExactName(_node, childName);
	if (child == nullptr || restPath.empty())
	{
		return child;
	}

	return findChildByExactPath(*child, restPath);
}

Node* bwn::findChildByPartialPath(const Node& _node, const bwn::string_view& _path)
{
	const auto [childName, restPath] = splitPathOnBegin(_path);
	if (childName.empty())
	{
		return nullptr;
	}

	// If this is final path return
	Node*const child = restPath.empty()
		? findChildByPartialName(_node, childName)
		: findChildByExactName(_node, childName);

	if (child == nullptr || restPath.empty())
	{
		return child;
	}

	return findChildByPartialPath(*child, restPath);
}

Node* bwn::findDescendantByExactName(const Node& _parent, const bwn::string_view& _name)
{
	Node* result = nullptr;

	visitDescendants(
		_parent,
		[&result, &_name](Node& _descendant) -> EVisitorResult
		{
			const String descendantName = _descendant.get_name();
			const bwn::string_view descendantNameView = bwn::wrap_view(descendantName);
			if (descendantNameView == _name)
			{
				result = &_descendant;
				return EVisitorResult::k_break;
			}

			return EVisitorResult::k_continue;
		});

	return result;
}

Node* bwn::findDescendantByExactName(const Node& _parent, const bwn::string_view& _name, const String& _typeHint)
{
	Node* result = nullptr;

	visitDescendants(
		_parent,
		[&result, &_name, &_typeHint](Node& _descendant) -> EVisitorResult
		{
			if (!_typeHint.is_empty() && !_descendant.is_class(_typeHint))
			{
				return EVisitorResult::k_continue;
			}

			const String descendantName = _descendant.get_name();
			const bwn::string_view descendantNameView = bwn::wrap_view(descendantName);
			if (descendantNameView == _name)
			{
				result = &_descendant;
				return EVisitorResult::k_break;
			}

			return EVisitorResult::k_continue;
		});

	return result;
}

Node* bwn::findDescendantByPartialName(const Node& _parent, const bwn::string_view& _partialName)
{
	Node*result = nullptr;

	visitDescendants(
		_parent,
		[&result, &_partialName](Node& _descendant) -> EVisitorResult
		{
			const String descendantName = _descendant.get_name();
			const bwn::string_view descendantNameView = bwn::wrap_view(descendantName);
			if (descendantNameView.find(_partialName) != bwn::string_view::npos)
			{
				result = &_descendant;
				return EVisitorResult::k_break;
			}

			return EVisitorResult::k_continue;
		});

	return result;
}

Node* bwn::findDescendantByPartialName(const Node& _parent, const bwn::string_view& _partialName, const String& _typeHint)
{
	Node*result = nullptr;

	visitDescendants(
		_parent,
		[&result, &_partialName, &_typeHint](Node& _descendant) -> EVisitorResult
		{
			if (!_typeHint.is_empty() && !_descendant.is_class(_typeHint))
			{
				return EVisitorResult::k_continue;
			}

			const String descendantName = _descendant.get_name();
			const bwn::string_view descendantNameView = bwn::wrap_view(descendantName);
			if (descendantNameView.find(_partialName) != bwn::string_view::npos)
			{
				result = &_descendant;
				return EVisitorResult::k_break;
			}

			return EVisitorResult::k_continue;
		});

	return result;
}

Node* bwn::findDescendantByExactPath(const Node& _node, const bwn::string_view& _path)
{
	const auto& [restPath, topNodeView] = splitPathOnEnd(_path);
	if (restPath.empty())
	{
		return findDescendantByExactName(_node, topNodeView);
	}

	Node* result = nullptr;
	visitDescendants(
		_node,
		[&result, &_node, &_path](Node& _descendant) -> EVisitorResult
		{
			if (checkNodeEndsByExactPath(_path, _descendant, &_node))
			{
				result = &_descendant;
				return EVisitorResult::k_break;
			}

			return EVisitorResult::k_continue;
		});

	return result;
}

Node* bwn::findDescendantByExactPath(const Node& _node, const bwn::string_view& _path, const String& _typeHint)
{
	const auto& [restPath, topNodeView] = splitPathOnEnd(_path);
	if (restPath.empty())
	{
		return findDescendantByExactName(_node, topNodeView, _typeHint);
	}

	Node* result = nullptr;
	visitDescendants(
		_node,
		[&result, &_node, &_path, &_typeHint](Node& _descendant) -> EVisitorResult
		{
			if (!_typeHint.is_empty() && !_descendant.is_class(_typeHint))
			{
				return EVisitorResult::k_continue;
			}

			if (checkNodeEndsByExactPath(_path, _descendant, &_node))
			{
				result = &_descendant;
				return EVisitorResult::k_break;
			}

			return EVisitorResult::k_continue;
		});

	return result;
}

Node* bwn::findDescendantByPartialPath(const Node& _node, const bwn::string_view& _path)
{
	const auto& [restPath, topNodeView] = splitPathOnEnd(_path);
	if (restPath.empty())
	{
		return findDescendantByPartialName(_node, topNodeView);
	}

	Node*result = nullptr;
	visitDescendants(
		_node,
		[&result, &_node, &_path](Node& _descendant) -> EVisitorResult
		{
			if (checkNodeEndsByPartialPath(_path, _descendant, &_node))
			{
				result = &_descendant;
				return EVisitorResult::k_break;
			}

			return EVisitorResult::k_continue;
		});

	return result;
}

Node* bwn::findDescendantByPartialPath(const Node& _node, const bwn::string_view& _path, const String& _typeHint)
{
	const auto& [restPath, topNodeView] = splitPathOnEnd(_path);
	if (restPath.empty())
	{
		return findDescendantByPartialName(_node, topNodeView, _typeHint);
	}

	Node*result = nullptr;
	visitDescendants(
		_node,
		[&result, &_node, &_path, &_typeHint](Node& _descendant) -> EVisitorResult
		{
			if (!_typeHint.is_empty() && !_descendant.is_class(_typeHint))
			{
				return EVisitorResult::k_continue;
			}

			if (checkNodeEndsByPartialPath(_path, _descendant, &_node))
			{
				result = &_descendant;
				return EVisitorResult::k_break;
			}

			return EVisitorResult::k_continue;
		});

	return result;
}

bool bwn::checkNodeEndsByExactPath(const bwn::string_view& _pathToCheck, const Node& _highestNode, const Node*const _lowestNode)
{
	bwn::string_view currentPath = _pathToCheck;
	const Node* currentNode = &_highestNode;
	while (!currentPath.empty() && currentNode != _lowestNode)
	{
		const auto [restPath, startNode] = splitPathOnEnd(currentPath);

		const String nodeName = currentNode->get_name();
		const bwn::string_view nodeNameView = bwn::wrap_view(nodeName);

		if (nodeNameView != startNode)
		{
			return false;
		}

		currentNode = currentNode->get_parent();
		currentPath = restPath;
	}

	// If we run out of paths earlier then we ran out of nodes, then this is success, this is success.
	return currentPath.empty();
}

bool bwn::checkNodeEndsByPartialPath(const bwn::string_view& _pathToCheck, const Node& _highestNode, const Node*const _lowestNode)
{
	if (&_highestNode == _lowestNode)
	{
		// Even if we have no nodes to march, this is still could be a success, if path is also empty.
		return _pathToCheck.empty();
	}

	const auto [restPath, startNode] = splitPathOnEnd(_pathToCheck);
	if (startNode.empty())
	{
		return false;
	}

	{
		const String nodeName = _highestNode.get_name();
		const bwn::string_view nodeNameView = bwn::wrap_view(nodeName);

		if (nodeNameView.find(startNode) == bwn::string_view::npos)
		{
			return false;
		}
	}

	const Node*const parentNode = _highestNode.get_parent();
	if (parentNode == nullptr)
	{
		return restPath.empty();
	}
	return checkNodeEndsByExactPath(restPath, *parentNode, _lowestNode);
}

bool bwn::checkNodeHasExactPath(const bwn::string_view& _pathToCheck, const Node& _highestNode, const Node*const _lowestNode)
{
	if (&_highestNode == _lowestNode)
	{
		// Even if we have no nodes to march, this is still could be a success, if path is also empty.
		return _pathToCheck.empty();
	}

	const auto [restPath, startNode] = splitPathOnEnd(_pathToCheck);
	if (startNode.empty())
	{
		return false;
	}

	const Node* currentNode = &_highestNode;
	while (currentNode != _lowestNode)
	{
		if (checkNodeEndsByExactPath(restPath, *currentNode, _lowestNode))
		{
			return true;
		}

		currentNode = currentNode->get_parent();
	}

	return false;
}

bool bwn::checkNodeHasPartialPath(const bwn::string_view& _pathToCheck, const Node& _highestNode, const Node*const _lowestNode)
{
	if (&_highestNode == _lowestNode)
	{
		// Even if we have no nodes to march, this is still could be a success, if path is also empty.
		return _pathToCheck.empty();
	}

	const auto [restPath, startNode] = splitPathOnEnd(_pathToCheck);
	if (startNode.empty())
	{
		return false;
	}

	const Node* currentNode = &_highestNode;
	while (currentNode != _lowestNode)
	{
		const String nodeName = currentNode->get_name();
		const bwn::string_view nodeNameView = bwn::wrap_view(nodeName);

		const Node*const parentNode = currentNode->get_parent();

		if (nodeNameView.find(startNode) != bwn::string_view::npos)
		{
			if (restPath.empty())
			{
				return true;
			}

			if (parentNode == nullptr)
			{
				// Path hasn't ran out, but we don't have any parent.
				return false;
			}

			if (checkNodeEndsByExactPath(restPath, *parentNode, _lowestNode))
			{
				return true;
			}
		}

		currentNode = parentNode;
	}

	return false;
}