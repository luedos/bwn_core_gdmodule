#pragma once

#include "bwn_core/types/stringView.hpp"
#include "bwn_core/utility/templateAlgorithms.hpp"

#include <utf-algorithms/converter.hpp>
#include <utf-algorithms/algorithms.hpp>

#include <fmt/format.h>
#include <fmt/xchar.h>

//
// Utf magic..
//
namespace bwn
{
	// This is basically helper, to allow specific utf conversion on format algorithms, without copying strings all over the place.
	template<typename CharT>
	struct UtfWrapper
	{
		const CharT*const beg = nullptr;
		const CharT*const end = nullptr;
	};

	template<typename CharT>
	constexpr UtfWrapper<CharT> wrap_utf(const std::basic_string_view<CharT>& _view)
	{
		return UtfWrapper<CharT>{ _view.data(), _view.data() + _view.size() };
	}

	template<typename CharT>
	constexpr UtfWrapper<CharT> wrap_utf(const CharT*const _string)
	{
		return wrap_utf(std::basic_string_view<CharT>(_string));
	}

	template<typename CharT>
	constexpr UtfWrapper<CharT> wrap_utf(const CharT*const _string, const size_t _size)
	{
		return UtfWrapper<CharT>{ _string, _string + _size };
	}

} // namespace bwn

template<typename OutputUtfCharT, typename InputUtfCharT> 
struct fmt::formatter<bwn::UtfWrapper<InputUtfCharT>, OutputUtfCharT> 
{
	template<typename ParseContextT>
	constexpr auto parse (ParseContextT& _ctx) 
	{
		// No format specifiers
		return _ctx.begin();
	}

	template <typename FormatContextT>
	auto format(const bwn::UtfWrapper<InputUtfCharT>& _utfString, FormatContextT& _ctx)
	{
		auto outIt = _ctx.out();
		auto unchangedOutIt = outIt;

		using Converter = utf::Converter<OutputUtfCharT, InputUtfCharT>;
		const InputUtfCharT* tempInputIt = _utfString.beg;
		const bool success = Converter::template covertStringUnsafe<decltype(outIt), const InputUtfCharT*, false>(outIt, tempInputIt, _utfString.end);

		return success
			? outIt
			: unchangedOutIt;
	}
};

namespace bwn::detail
{
	template<typename> struct ArrayOpeningStrLiteral;
	template<> struct ArrayOpeningStrLiteral<char>{ static constexpr char k_value[] = "["; };
	template<> struct ArrayOpeningStrLiteral<char32_t>{ static constexpr char32_t k_value[] = U"["; };

	template<typename> struct ArrayClosingStrLiteral;
	template<> struct ArrayClosingStrLiteral<char>{ static constexpr char k_value[] = "]"; };
	template<> struct ArrayClosingStrLiteral<char32_t>{ static constexpr char32_t k_value[] = U"]"; };

	template<typename> struct FormatTagStrLiteral;
	template<> struct FormatTagStrLiteral<char>{ static constexpr char k_value[] = "{}"; };
	template<> struct FormatTagStrLiteral<char32_t>{ static constexpr char32_t k_value[] = U"{}"; };

	template<typename> struct ArrayDividerStrLiteral;
	template<> struct ArrayDividerStrLiteral<char>{ static constexpr char k_value[] = ", "; };
	template<> struct ArrayDividerStrLiteral<char32_t>{ static constexpr char32_t k_value[] = U", "; };

} // namespace bwn::detail

//
// Native helpers.
//
namespace bwn
{

	// This functions is nothing special, but I wanted this one be as wrapper, 
	// so if in the future I found better way to format directly into String,
	// I will simply replace this function.
	template<typename...ArgsTs>
	_FORCE_INLINE_ String format(fmt::basic_string_view<char32_t> _str, ArgsTs&&..._args)
	{
		return String(fmt::format(_str, std::forward<ArgsTs>(_args)...).c_str());
	}

	template<typename...ArgsTs>
	_FORCE_INLINE_ String format(fmt::basic_string_view<char> _str, ArgsTs&&..._args)
	{
		return String(fmt::format(_str, std::forward<ArgsTs>(_args)...).c_str());
	}

	_FORCE_INLINE_ String format(fmt::basic_string_view<char32_t> _str)
	{
		return String(_str.data(), _str.size() );
	}

	_FORCE_INLINE_ String format(fmt::basic_string_view<char> _str)
	{
		return String(_str.data(), _str.size());
	}

} // namespace bwn

namespace bwn
{
	namespace detail
	{
		template<typename StringT>
		struct ToStringView : public std::false_type
		{};

		template<>
		struct ToStringView<String> : public std::true_type
		{
			using CharType = char32_t;

			static std::basic_string_view<char32_t> get(const String& _string)
			{
				return std::basic_string_view<char32_t>(_string.ptr(), _string.length());
			}
		};

		template<typename CharT>
		struct ToStringView<std::basic_string_view<CharT>> : public std::true_type
		{
			using CharType = CharT;

			constexpr static std::basic_string_view<CharT> get(const std::basic_string_view<CharT>& _string)
			{
				return _string;
			}
		};

		template<typename CharT>
		struct ToStringView<std::basic_string<CharT>>
		{
			using CharType = CharT;

			static std::basic_string_view<CharT> get(const std::basic_string<CharT>& _string)
			{
				return std::basic_string_view<CharT>(_string);
			}
		};

	} // namespace detail

	template<typename...ArgTs>
	String concat(const ArgTs &..._args)
	{
		static_assert((detail::ToStringView<ArgTs>::value && ...),
		              "Not all string could be converted to the string view via ToStringView helper.");
		using CharType = typename detail::ToStringView<variadic_template_first_t<ArgTs...>>::CharType;
		using Algorithms = utf::Algorithms<CharType>;
		const std::array<std::basic_string_view<CharType>, sizeof...(_args)> strings = {detail::ToStringView<ArgTs>::get(_args)...};

		int totalSize = 0;
		for (const std::basic_string_view<CharType>& view: strings)
		{
			const CharType* begin = view.begin();
			totalSize += static_cast<int>(Algorithms::distance(begin, view.end()));
		}

		String ret;
		ret.resize(totalSize + 1);

		CharType* currentPos = ret.ptrw();
		for (const std::basic_string_view<CharType>& view: strings)
		{
			using Converter = utf::Converter<char32_t, CharType>;
			const CharType* inputBegin = view.begin();
			if (!Converter::template covertStringUnsafe<char32_t*, const CharType*, false>(currentPos, inputBegin, view.end()))
			{
				ret.clear();
				return ret;
			}
		}

		if (totalSize > 0)
		{
			// Basically the last string position.
			*currentPos = 0;
		}
	}

} // namespace bwn

// This is more or less just a copy of godot algorithms, which for some reason exist only in mono module.

#if defined(__GNUC__)
	#define _PRINTF_FORMAT_ATTRIBUTE_1_0 __attribute__((format(printf, 1, 0)))
	#define _PRINTF_FORMAT_ATTRIBUTE_1_2 __attribute__((format(printf, 1, 2)))
#else
	#define _PRINTF_FORMAT_ATTRIBUTE_1_0
	#define _PRINTF_FORMAT_ATTRIBUTE_1_2
#endif

String str_format(const char *_format, ...) _PRINTF_FORMAT_ATTRIBUTE_1_2;
String str_format(const char *_format, va_list _list) _PRINTF_FORMAT_ATTRIBUTE_1_0;
char *str_format_new(const char *_format, ...) _PRINTF_FORMAT_ATTRIBUTE_1_2;
char *str_format_new(const char *_format, va_list _list) _PRINTF_FORMAT_ATTRIBUTE_1_0;