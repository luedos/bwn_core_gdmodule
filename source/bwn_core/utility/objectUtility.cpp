#include "precompiled_core.hpp"

#include "bwn_core/utility/objectUtility.hpp"

bwn::UniqueRef<Object> bwn::cloneByProperties(Object& _object, const bool p_deepCopy)
{
	UniqueRef<Object> clonedObject = UniqueRef<Object>(ClassDB::instantiate(_object.get_class_name()));
	BWN_TRAP_COND(clonedObject.valid(), "Failed to clone object by class '{}'.", _object.get_class_name());

	List<PropertyInfo> propertiesInfo;
	_object.get_property_list(&propertiesInfo);

	for (const PropertyInfo& info : propertiesInfo)
	{
		if (!(info.usage & PROPERTY_USAGE_STORAGE))
		{
			continue;
		}

		const Variant value = _object.get(info.name);
		switch (value.get_type())
		{
			case Variant::Type::DICTIONARY:
			case Variant::Type::ARRAY:
			case Variant::Type::PACKED_BYTE_ARRAY:
			case Variant::Type::PACKED_COLOR_ARRAY:
			case Variant::Type::PACKED_INT32_ARRAY:
			case Variant::Type::PACKED_INT64_ARRAY:
			case Variant::Type::PACKED_FLOAT32_ARRAY:
			case Variant::Type::PACKED_FLOAT64_ARRAY:
			case Variant::Type::PACKED_STRING_ARRAY:
			case Variant::Type::PACKED_VECTOR2_ARRAY:
			case Variant::Type::PACKED_VECTOR3_ARRAY: {
				clonedObject->set(info.name, value.duplicate(p_deepCopy));
			} break;

			case Variant::Type::OBJECT:
			{
				if (!(info.usage & PROPERTY_USAGE_NEVER_DUPLICATE) && (p_deepCopy || info.usage & PROPERTY_USAGE_ALWAYS_DUPLICATE))
				{
					Ref<Resource> subresource = value;
					if (subresource.is_valid())
					{
						clonedObject->set(info.name, subresource->duplicate(p_deepCopy));
					}
				}
				else
				{
					clonedObject->set(info.name, value);
				}
			}
			break;

			default: {
				clonedObject->set(info.name, value);
			}
		}
	}

	return clonedObject;
}

bwn::UniqueRef<Object> bwn::cloneByProperties(Object& _object, const StringName& _ignoreClassUpTo, const bool p_deepCopy)
{
	UniqueRef<Object> clonedObject = UniqueRef<Object>(ClassDB::instantiate(_object.get_class_name()));
	BWN_TRAP_COND(clonedObject.valid(), "Failed to clone object by class '{}'.", _object.get_class_name());

	std::vector<StringName> classIgnoreList;
	if (_ignoreClassUpTo != StringName())
	{
		StringName currentIgnoreClass;
		while (true)
		{
			classIgnoreList.push_back(currentIgnoreClass);
			if (currentIgnoreClass == SNAME("Object"))
			{
				break;
			}

			currentIgnoreClass = ClassDB::get_parent_class(currentIgnoreClass);
		}
	}

	List<PropertyInfo> propertiesInfo;
	_object.get_property_list(&propertiesInfo);

	for (const PropertyInfo& info : propertiesInfo)
	{
		if (!(info.usage & PROPERTY_USAGE_STORAGE))
		{
			continue;
		}

		if (std::find(classIgnoreList.begin(), classIgnoreList.end(), info.class_name) != classIgnoreList.end())
		{
			continue;
		}

		const Variant value = _object.get(info.name);
		switch (value.get_type())
		{
			case Variant::Type::DICTIONARY:
			case Variant::Type::ARRAY:
			case Variant::Type::PACKED_BYTE_ARRAY:
			case Variant::Type::PACKED_COLOR_ARRAY:
			case Variant::Type::PACKED_INT32_ARRAY:
			case Variant::Type::PACKED_INT64_ARRAY:
			case Variant::Type::PACKED_FLOAT32_ARRAY:
			case Variant::Type::PACKED_FLOAT64_ARRAY:
			case Variant::Type::PACKED_STRING_ARRAY:
			case Variant::Type::PACKED_VECTOR2_ARRAY:
			case Variant::Type::PACKED_VECTOR3_ARRAY:
				clonedObject->set(info.name, value.duplicate(p_deepCopy));
				break;

			case Variant::Type::OBJECT:
			{
				if (!(info.usage & PROPERTY_USAGE_NEVER_DUPLICATE) && (p_deepCopy || info.usage & PROPERTY_USAGE_ALWAYS_DUPLICATE))
				{
					Ref<Resource> subresource = value;
					if (subresource.is_valid())
					{
						clonedObject->set(info.name, subresource->duplicate(p_deepCopy));
					}
				}
				else
				{
					clonedObject->set(info.name, value);
				}
			}
			break;

			default:
				clonedObject->set(info.name, value);
				break;
		}
	}

	return clonedObject;
}