#pragma once

#include "bwn_core/utility/variantUtility.hpp"
#include "bwn_core/utility/propertyInfoUtility.hpp"
#include "bwn_core/utility/methodInfoUtility.hpp"
#include "bwn_core/utility/templateAlgorithms.hpp"
#include "bwn_core/types/uniqueRef.hpp"

#include <core/object/object.h>
#include <core/object/class_db.h>

#include <type_traits>

namespace bwn
{
	template<typename TypeT>
	inline void addSignal(const MethodInfo& _method)
	{
		ClassDB::add_signal(TypeT::get_class_static(), _method);
	}

	template<typename TypeT>
	inline void addSignal(const String& _methodName)
	{
		ClassDB::add_signal(TypeT::get_class_static(), MethodUtility<void()>(_methodName).get());
	}
	
	template<typename TypeT, typename MethodT>
	inline void addSignal(const String& _methodName, const String& _argName0)
	{
		ClassDB::add_signal(
			TypeT::get_class_static(),
			MethodUtility<MethodT>(_methodName)
				.template argName<0>(_argName0)
				.get());
	}

	template<typename TypeT, typename MethodT>
	inline void addSignal(const String& _methodName, const String& _argName0, const String& _argName1)
	{
		ClassDB::add_signal(
			TypeT::get_class_static(),
			MethodUtility<MethodT>(_methodName)
				.template argName<0>(_argName0)
				.template argName<1>(_argName1)
				.get());
	}

	template<typename TypeT, typename MethodT>
	inline void addSignal(const String& _methodName, const String& _argName0, const String& _argName1, const String& _argName2)
	{
		ClassDB::add_signal(
			TypeT::get_class_static(),
			MethodUtility<MethodT>(_methodName)
				.template argName<0>(_argName0)
				.template argName<1>(_argName1)
				.template argName<2>(_argName2)
				.get());
	}

	template<typename MethodT>
	_FORCE_INLINE_ MethodBind* bindMethod(
	#if defined(DEBUG_METHODS_ENABLED)
		const MethodDefinition& _definition,
	#else
		const char* _definition,
	#endif 
		MethodT _method)
	{
		return ClassDB::bind_method<decltype(_definition), MethodT>(_definition, std::forward<MethodT>(_method));
	}

#define BWN_BIND_PROPERTY(m_nameString, ...) bindProperty(m_nameString, "set_" m_nameString, "get_" m_nameString, __VA_ARGS__)

	template<typename SetterT, typename GetterT>
	_FORCE_INLINE_ void bindProperty(
		const char*const _propertyName,
		const char*const _setterName,
		const char*const _getterName,
		SetterT _setter,
		GetterT _getter,
		const PropertyHint _hintEnum = PROPERTY_HINT_NONE,
		const String& _hintString = String(),
		const uint32_t _usageFlags = PROPERTY_USAGE_DEFAULT)
	{
		static_assert(std::is_member_function_pointer<GetterT>::value, "Property getter should be a member function pointer.");
		static_assert(std::is_member_function_pointer<SetterT>::value, "Property setter should be a member function pointer.");
		
		using GetterClassType = std::remove_const_t<bwn::callable_class_type_t<GetterT>>;
		using SetterClassType = std::remove_const_t<bwn::callable_class_type_t<SetterT>>;
		
		static_assert(std::is_same<GetterClassType, SetterClassType>::value, "Getter and setter of the property should be from the same class.");

		using PropertyType =
			typename std::remove_cv<
				typename std::remove_reference<
					typename std::invoke_result<
						GetterT, 
						const GetterClassType*
					>::type
				>::type
			>::type;

		static_assert(std::is_invocable<SetterT, SetterClassType*, const PropertyType>::value, "Incorrect setter, which can't be invoked with type returned from getter.");

		bwn::bindMethod(D_METHOD(_setterName), _setter);
		bwn::bindMethod(D_METHOD(_getterName), _getter);

		ClassDB::add_property(
			GetterClassType::get_class_static(), 
			PropertyUtility<PropertyType>(_propertyName).hint(_hintEnum, _hintString).usage(_usageFlags).get(), 
			_scs_create(_setterName),
			_scs_create(_getterName));
	}

	UniqueRef<Object> cloneByProperties(Object& _object, const bool p_deepCopy = false);
	UniqueRef<Object> cloneByProperties(Object& _object, const StringName& _ignoreClassUpTo, const bool p_deepCopy = false);

	template<typename ObjectT>
	UniqueRef<ObjectT> cloneByProperties(ObjectT& _object, const bool p_deepCopy = false)
	{
		return bwn::objectCast<ObjectT>(cloneByProperties(static_cast<Object&>(_object, p_deepCopy)));
	}

	template<typename ObjectT>
	UniqueRef<ObjectT> cloneByProperties(ObjectT& _object, const StringName& _ignoreClassUpTo, const bool p_deepCopy = false)
	{
		return bwn::objectCast<ObjectT>(cloneByProperties(static_cast<Object&>(_object), _ignoreClassUpTo, p_deepCopy));
	}

} // namespace bwn