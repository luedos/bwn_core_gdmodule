#pragma once

#include <type_traits>

namespace bwn
{
	enum class EVisitorResult
	{
		k_continue,
		k_break
	};

	template<typename FunctorT, typename...ArgsTs>
	_FORCE_INLINE_ EVisitorResult callVisitor(FunctorT&& _functor, ArgsTs&&..._args)
	{
		using ResultType = typename std::invoke_result<FunctorT, ArgsTs...>::type;
		if constexpr (std::is_convertible<ResultType, EVisitorResult>::value)
		{
			return _functor(std::forward<ArgsTs>(_args)...);
		}
		else
		{
			_functor(std::forward<ArgsTs>(_args)...);
			return EVisitorResult::k_continue;
		}
	}
} // namespace bwn