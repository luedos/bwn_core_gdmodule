#pragma once 

#include <utility>

namespace bwn
{
	template<typename>
	struct TupleForeach;

	template<std::size_t...IndexVs>
	struct TupleForeach<std::index_sequence<IndexVs...>>
	{
		//
		// Public interface.
		//
	public:
		template<typename CallableT, typename...TupleTs>
		static bool call(CallableT&& _func, TupleTs&&..._tuples)
		{
			bool success = true;
			((success && (success = callIndex<CallableT, IndexVs, TupleTs...>(std::forward<CallableT>(_func), std::forward<TupleTs>(_tuples)...))), ...);
			return success;
		}

		//
		// Private methods.
		//
	private:
		template<typename CallableT, std::size_t IndexV, typename...TupleTs>
		static bool callIndex(CallableT&& _func, TupleTs&&..._tuples)
		{
			return _func(std::get<IndexV>(_tuples)...);
		}
	};

	template<typename, typename>
	struct is_tuple_convertable : public std::false_type
	{};

	template<typename...FromArgTs, typename...ToArgTs>
	struct is_tuple_convertable<std::tuple<FromArgTs...>, std::tuple<ToArgTs...>>
	{
		static_assert(sizeof...(FromArgTs) == sizeof...(ToArgTs), "Then comparing to tuples, their sizes should be equal.");
		static constexpr bool value = (std::is_convertible_v<FromArgTs, ToArgTs> && ...);
	};

	template<>
	struct is_tuple_convertable<std::tuple<>, std::tuple<>> : public std::true_type
	{};

	template<typename FromTupleT, typename ToTupleT>
	using is_tuple_convertable_v = typename is_tuple_convertable<FromTupleT, ToTupleT>::value;

	template<typename FirstT, typename...>
	struct variadic_template_first
	{
		using type = FirstT;
	};

	template<typename...TypeTs>
	using variadic_template_first_t = typename variadic_template_first<TypeTs...>::type;

	template<typename FirstT, typename...RestTs>
	struct variadic_template_last
	{
		using type = typename variadic_template_last<RestTs...>::type;
	};

	template<typename SingleT>
	struct variadic_template_last<SingleT>
	{
		using type = SingleT;
	};

	template<typename...TypeTs>
	using variadic_template_last_t = typename variadic_template_last<TypeTs...>::type;

	template<size_t k_typeIndex, typename FirstT, typename...RestTs>
	struct variadic_template_nth
	{
		static_assert(k_typeIndex <= sizeof...(RestTs), "Incorrect index for variadic type range.");
		using type = typename variadic_template_nth<k_typeIndex - 1, RestTs...>::type;
	};

	template<typename FirstT, typename...RestTs>
	struct variadic_template_nth<0, FirstT, RestTs...>
	{
		using type = FirstT;
	};

	template<size_t k_typeIndex, typename...TypeTs>
	using variadic_template_nth_t = typename variadic_template_nth<k_typeIndex, TypeTs...>::type;

	template<typename...TypeTs>
	struct variadic_template_pack
	{
		static constexpr size_t size_value = sizeof...(TypeTs);
		using first_type = variadic_template_first_t<TypeTs...>;
		using last_type = variadic_template_last_t<TypeTs...>;

		template<size_t k_typeIndex>
		using get_type = variadic_template_nth_t<k_typeIndex, TypeTs...>;
	};

	template<typename>
	struct unpack_variadic;

	template<typename...TypeTs, template<typename...> class VariadicTypeT>
	struct unpack_variadic<VariadicTypeT<TypeTs...>>
	{
		using type = variadic_template_pack<TypeTs...>;
	};

	template<typename TypeT>
	using unpack_variadic_t = typename unpack_variadic<TypeT>::type;

	template<typename VariadicArrayT, template <typename...> class PacketT>
	struct pack_variadic;

	template<typename...TypeTs, template <typename...> class PacketT>
	struct pack_variadic<variadic_template_pack<TypeTs...>, PacketT>
	{
		using type = PacketT<TypeTs...>;
	};

	template<typename VariadicArrayT, template <typename...> class PacketT>
	using pack_variadic_t = typename pack_variadic<VariadicArrayT, PacketT>::type;

	template<typename CallableT, typename=void>
	struct callable_info;

	template<typename ReturnT, typename...ArgTs>
	struct callable_info<ReturnT(ArgTs...), void>
	{
		using return_type = ReturnT;
		using arguments_type = variadic_template_pack<ArgTs...>;
		using signature_type = ReturnT(ArgTs...);
	};

	template<typename ReturnT, typename...ArgTs>
	struct callable_info<ReturnT(*)(ArgTs...), void>
	{
		using return_type = ReturnT;
		using arguments_type = variadic_template_pack<ArgTs...>;
		using signature_type = ReturnT(ArgTs...);
	};

	template<typename ClassT, typename ReturnT, typename...ArgTs>
	struct callable_info<ReturnT(ClassT::*)(ArgTs...), void>
	{
		using return_type = ReturnT;
		using arguments_type = variadic_template_pack<ArgTs...>;
		using signature_type = ReturnT(ArgTs...);
		using class_type = ClassT;
	};

	template<typename ClassT, typename ReturnT, typename...ArgTs>
	struct callable_info<ReturnT(ClassT::*)(ArgTs...) const, void>
	{
		using return_type = ReturnT;
		using arguments_type = variadic_template_pack<ArgTs...>;
		using signature_type = ReturnT(ArgTs...);
		using class_type = const ClassT;
	};

	template<typename FunctorT>
	struct callable_info<FunctorT, std::void_t<typename callable_info<decltype(&FunctorT::operator())>::signature_type>>
	{
		using return_type = typename callable_info<decltype(&FunctorT::operator())>::return_type;
		using arguments_type = typename callable_info<decltype(&FunctorT::operator())>::arguments_type;
		using signature_type = typename callable_info<decltype(&FunctorT::operator())>::signature_type;
		using class_type = typename callable_info<decltype(&FunctorT::operator())>::class_type;
	};

	template<typename CallableT>
	using callable_return_type_t = typename callable_info<CallableT>::return_type;

	template<typename CallableT>
	using callable_arguments_type_t = typename callable_info<CallableT>::arguments_type;

	template<typename CallableT>
	using callable_signature_type_t = typename callable_info<CallableT>::signature_type;

	template<typename CallableT>
	using callable_class_type_t = typename callable_info<CallableT>::class_type;

	template<typename FunctorT, typename VariadicArrayT>
	struct is_invocable_packed : public std::false_type
	{};

	template<typename FunctorT, typename...ArgTs>
	struct is_invocable_packed<FunctorT, variadic_template_pack<ArgTs...>> : public std::is_invocable<FunctorT, ArgTs...>
	{};

	template<typename FunctorT, typename VariadicArrayT>
	static constexpr bool is_invocable_packed_v = is_invocable_packed<FunctorT, VariadicArrayT>::value;

	template<typename ValueTypeT, ValueTypeT ForcedValueV>
	struct ForceCompileTime
	{
		static constexpr ValueTypeT k_value = ForcedValueV;
	};

	template<typename ToTypeT, typename FromTypeT>
	inline ToTypeT union_cast(const FromTypeT& _obj)
	{
	    // static_assert(sizeof(ToTypeT) == sizeof(FromTypeT), "The sizes of two casted objects must be equal, otherwise this could be even more ub then it already is!");
	    union
	    {
	        ToTypeT castedObject;
	        FromTypeT originalObject;
	    } conversion;

	    conversion.originalObject = _obj;
	    return conversion.castedObject;
	}

} // namespace bwn