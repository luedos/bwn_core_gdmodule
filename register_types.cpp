#include "precompiled_core.hpp"

#include "register_types.h"

#include "bwn_core/results/systemResult.hpp"
#include "bwn_core/results/systemFacility.hpp"
#include "bwn_core/debug/assertManager.hpp"

void initialize_bwn_core_module(const ModuleInitializationLevel _level)
{
	if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_CORE)
	{
	#if defined(DEBUG_ENABLED)
		bwn::AssertManager::createSingleton();
	#endif // DEBUG_ENABLED

		bwn::SystemFacility::createSingleton();
	}
	else if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_SCENE)
	{
		// Results
		GDREGISTER_CLASS(bwn::SystemResultObject)
		GDREGISTER_CLASS(bwn::SystemFacility)
	}
}

void uninitialize_bwn_core_module(const ModuleInitializationLevel _level)
{
	if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_CORE)
	{
		bwn::SystemFacility::destroySingleton();

	#if defined(DEBUG_ENABLED)
		bwn::AssertManager::destroySingleton();
	#endif // DEBUG_ENABLED
	}
}