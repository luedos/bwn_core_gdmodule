## Core functionality extension for the godot engine

This library introduce bunch of small additional classes/structs and functionality for the better coding in godot cpp modules.

This modules is based on cmake build system implementation and not the scons one.

Other dependencies for this library include:
- fmtlib
- bwn_utf_algorithms_lib
- messagebox-x11-lib