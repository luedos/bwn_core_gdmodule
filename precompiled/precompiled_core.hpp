#pragma once

//
// Std includes.
//
#include <string>
#include <string_view>
#include <vector>
#include <map>
#include <unordered_map>
#include <memory>
#include <optional>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <algorithm>
#include <type_traits>
#include <utility>
#include <cstdlib>
#include <cwctype>
#include <limits>

//
// Godot includes.
//
#include <core/object/object.h>
#include <core/object/ref_counted.h>
#include <core/object/class_db.h>

//
// Basic project includes.
//
#include "bwn_core/utility/formatUtility.hpp"
#include "bwn_core/utility/godotFormating.hpp"
#include "bwn_core/types/uniqueRef.hpp"

#include "bwn_core/debug/debug.hpp"
#include "bwn_core/debug/debugUtility.hpp"